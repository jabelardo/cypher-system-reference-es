Horror
######

Although it’s very likely a subset of the modern genre, horror as a
genre gets special treatment. Unlike the other genres, horror doesn’t
necessarily suggest a setting. Any setting can be horrific. Horror is
more of a style. An approach. A mood.

You could easily have horror in other times and settings, but for our
purposes, we’ll deal with a default setting in the modern day. The PCs
are probably normal people, not secret agents or special investigators
(although being a part of a secret agency that deals with monsters in
the shadows could make for a fine horror game).

Suggested types and additional equipment for a horror setting are the
same as in a modern setting.

Consent
=======

Horror games allow us to explore some pretty dark topics from the safety
of our own game tables. But before you do that, make sure everyone
around your table is okay with that. Find out what your players will
find “good uncomfortable,” which is something that makes us squirm in
our seats in a great horror movie, and “bad uncomfortable,” which is
something that actually makes a player feel nauseated, unsafe, or
offended. Being scared can be fun, but being sickened isn’t.

Consider the age and maturity of everyone in the game, perhaps in terms
of the movie rating system. Tell the players what you think the game
you’re running would be rated. If everyone’s okay with an R rating, then
fine. You can have a spooky game that’s on the level of a kids’ movie
rated G—more like *Scooby-Doo* than *Saw*, in other words. A PG rating
might be right for a game that’s more creepy than horrific, with ghosts
and spooky noises but not axe-wielding maniacs.

The different ratings suggest different kinds of content for your game.
Finding a dead body is horrible, but watching someone get decapitated is
something else entirely. Getting chased around by an alien that wants to
eat you is one thing, but having it gestate and burst out of your own
intestines is another. You need to know where the line is for everyone
participating, and you need to know it right from the beginning.

For more information and advice on safe ways to address consent issues
in your game, read the free *Consent in Gaming* PDF at
https://myMCG.info/consent

Basic Creatures and NPCs for a Horror Game
==========================================

Businessperson:
    level 2

Cat:
    level 1, Speed defense as level 3

Clerk:
    level 2

Dog:
    level 2, perception as level 3

Dog, vicious:
    level 3, attacks and perception as level 4

Groundskeeper/caretaker:
    level 2; health 8

Man in Black:
    level 4; carries weird weapons, including those with long
    range

Rat:
    level 1

Tarantula:
    level 1

Horror Artifacts
================

Most of the time, a horror artifact will be something really weird—an
ancient tome of forbidden necromancy, an alien device that humans can
barely understand, and so forth. They are often unique items rather than
one of a type. Horror artifacts should probably come with a risk, such
as a built-in cost, a drawback, or something else that makes using them
another way to heighten the tension of the game. Several examples are
below.

.. sqlquery::
   SELECT DISTINCT
       name,
       CASE
           WHEN level_dice_count <> 0 AND level_bonus <> 0
               THEN level_dice_count || 'd' || level_dice_size ||
                    ' + ' || level_bonus
           WHEN level_dice_count <> 0 AND level_bonus = 0
               THEN level_dice_count || 'd' || level_dice_size
           WHEN level_dice_count = 0 AND level_bonus <> 0
               THEN level_bonus || ''
       END as level,
       form,
       effect,
       CASE
           WHEN depletion_target == 0
               THEN '—'
           WHEN depletion_target == 1
               THEN '1 in 1d' || depletion_dice_size
           ELSE '1-' || depletion_target || ' in 1d' || depletion_dice_size
       END as depletion,
       depletion_notes,
       notes
   FROM artifact_templates
   WHERE source_name = 'CSRD Horror'
   ORDER BY name

   {% for row in cursor %}
   {{row.name}}
   {{row.name|length * "-"}}

   Level:
       {{row.level}}

   Form:
       {{row.form|indent}}

   Effect:
       {{row.effect|html2rst|indent}}

   Depletion:
       {{row.depletion}} {{row.depletion_notes}}

   {{row.notes|html2rst}}
   {% endfor %}

Optional Rule: Shock
====================

When the PCs encounter something shocking, many times the most realistic
response is to scream, stand in abject horror, or run. That might not be
the smartest thing to do in the situation, but it’s genuine. What would
your accountant do if they saw an axe-wielding maniac coming at them?
Let’s face it, unless they truly steeled themselves with all their will,
they’d probably scream and run.

When a PC encounters something horrific, utterly disgusting, dreadful,
impossible, or otherwise shocking, call for an Intellect defense roll
based on the level of the creature involved, or simply an appropriate
level as decided by the GM (see the :ref:`horror:Shock Levels` table). Failure might
mean that for one round, the player loses control of the character, and
the GM decides what the PC does next. This usually means that the
character runs, screams, gibbers, stares slack-jawed, or just does
nothing. However, GMs should welcome player input into this situation.
The point is to portray that when we’re shocked, we don’t always react
in the best way, the smartest way, or even the way we want to. Fear is a
powerful thing.

Alternatively, failure on the Intellect defense roll might mean that the
character suffers Intellect damage equal to the level of the defense
task. This indicates an overall toll that numerous shocks and horrors
can have on a person. You might have a situation where a character
literally dies of fright.

Shock Levels
------------

+----------------------------------------------------+----------------+
| Event                                              | Level          |
+====================================================+================+
| Something unexpected darts or jumps out            | 1              |
+----------------------------------------------------+----------------+
| Something suddenly moves just out of the corner of | 2              |
| the eye                                            |                |
+----------------------------------------------------+----------------+
| A sudden loud noise (like a scream)                | 2              |
+----------------------------------------------------+----------------+
| Unexpectedly seeing a corpse                       | 2              |
+----------------------------------------------------+----------------+
| Watching someone die                               | 3              |
+----------------------------------------------------+----------------+
| Seeing something impossible (like an inanimate     | 4              |
| object sliding across the floor)                   |                |
+----------------------------------------------------+----------------+
| Watching a friend die                              | 5              |
+----------------------------------------------------+----------------+
| Seeing a monstrous creature                        | Creature level |
+----------------------------------------------------+----------------+
| Witnessing something supernatural (like a spell)   | 5              |
+----------------------------------------------------+----------------+
| Seeing something mind-bending (like an impossible, | 8              |
| multidimensional demigod coalescing out of thin    |                |
| air)                                               |                |
+----------------------------------------------------+----------------+

Optional Rule: Horror Mode
==========================

For horror games, GMs can implement a rule called Horror Mode. The idea
is to create a feeling of escalating dread and menace by changing one
die roll mechanic. In the game, things begin as normal. The PCs interact
with each other and the NPCs, investigate, research, travel, and so on.
But when they enter the haunted house, the serial killer gets close, the
elder things beneath the earth awaken, or whatever horrific situation
planned by the GM begins, things change. At this time, the GM announces
that the game has gone into Horror Mode.

This is a key for the players (not the characters) to recognize that
things are getting bad. It’s the RPG equivalent of spooky music
beginning to play in a horror film. While in Horror Mode, the rules for
GM intrusions governed by die rolls change. Normally this happens only
on a roll of 1, but when Horror Mode starts, it becomes a roll of 1 or
2. And then it escalates. As time passes, GM intrusions happen on a roll
of 1 to 3, then a roll of 1 to 4, and so on. This potentially means that
a die roll in Horror Mode can indicate success in a task and still
trigger a GM intrusion.

As the intrusion range changes with each escalation, the GM should
announce this to the players. The feeling of rising tension should be
dramatic and overt.

+------------------------+--------------------------------------------+
| Activity               | Intrusion Range Increases by 1             |
+========================+============================================+
| Exploring a large area | Every time a new intrusion is indicated by |
|                        | a die roll                                 |
+------------------------+--------------------------------------------+
| Exploring              | Every ten minutes or every time a new      |
|                        | intrusion is indicated by a die roll       |
+------------------------+--------------------------------------------+
| Combat                 | Each round                                 |
+------------------------+--------------------------------------------+

For example, while the PCs are exploring a dark swamp (a large area),
the game goes into Horror Mode and intrusions are indicated on a 1 or 2.
During this exploration, one of the players rolls a 2. Not only is there
an intrusion, but now the range escalates to 1, 2, or 3. The character
is almost dragged into a spot of quicksand-like muck. Then the PCs find
an old abandoned house in the middle of the swamp. They enter, and now
the escalation rate goes up if they roll a 1, 2, or 3, or every ten
minutes that passes in the game. They explore the house for twenty
minutes (escalating intrusions to 1 to 5), and during the investigation
of the kitchen, someone rolls a 3, triggering an intrusion. A cabinet
opens mysteriously and a strangely carved clay pot falls, striking the
character. This also escalates the intrusion rate, so they now occur on
a roll of 1 to 6. When the PCs reach the attic, they encounter the
dreaded swamp slayer, a half man, half beast that thrives on blood. It
attacks, and now the range goes up during each round of combat. After
four rounds of fighting, intrusions happen on a roll of 1 to 10—half the
time. Things are getting dicey, and they’re only going to get worse.

When the GM announces that Horror Mode has ended, the GM intrusion rate
goes back to normal, happening only on a roll of 1 or when the GM awards
XP.

(Horror Mode is a very “meta” rule. It gives players knowledge that
their characters don’t have. This is similar to how the viewers of a
horror movie or readers of a horror story often know more than the
characters on the screen or page. It heightens the tension. Players can
express the start of Horror Mode by having their characters talk about
goosebumps or a feeling of being watched, but this is not necessary.)

Using GM Intrusions in Horror Mode
----------------------------------

With the GM intrusions coming fast and furious toward the end of Horror
Mode, it’s easy to run out of ideas. In combat, intrusions might just
mean that the monster or villain gets a surprise extra attack or
inflicts more damage. Perhaps a PC is thrown to the ground or nearer to
the edge of a cliff. If the characters are running away, one might trip
and fall. If the PCs are exploring, a bookcase topples, potentially
hitting someone. Think of all the similar moments you’ve seen in horror
films.

Sometimes, if the GM prefers, the GM intrusion can simply be something
frightening, like a moan or a whisper. These aren’t dangerous to the
PCs, but they escalate the tension and indicate that something bad is
getting closer.

In fact, while in Horror Mode, GMs should mostly refrain from doing
anything bad, ominous, or dangerous unless it’s an intrusion (either
from a die roll or through the awarding of XP). In a horror game, GM
intrusions are an indication that things are bad and getting worse, and
whenever possible, the GM should allow the Horror Mode escalation to
drive the action. This makes the GM more of a slave to the dice than in
other Cypher System situations, but that’s okay.

Consider this example. The PCs have tracked something that is probably
committing a series of horrific murders to an old factory. They enter
the building to explore. The GM knows where the creature is hiding in
the factory, but decides that it doesn’t become aware of the characters
until an intrusion is indicated. The only clue the PCs have is a
mysterious noise off in the darkness. The creature doesn’t move toward
them until another GM intrusion occurs. Now they hear something dragging
across the factory floor, coming closer. But it’s not until a third
intrusion occurs that the creature lunges out from behind an old machine
at the PC who rolled the die.

In some ways, the status quo doesn’t change until an intrusion happens.
This could be seen as limiting the GM and the need for pacing, but
remember that the GM can still have an intrusion occur anytime they
desire, in addition to waiting for the low die rolls.

(GMs may want to limit the number of intrusions to no more than one per
round, no matter what the dice indicate, but that should be based on the
situation.)

Optional Rule: Madness
======================

Having characters descend into madness is an interesting facet of some
kinds of horror and can make long-term horror campaigns more
interesting. The easiest way to portray blows to a character’s sanity is
through Intellect damage. When PCs encounter something shocking, as
described above, they always take Intellect damage. If they would
normally move one step down the damage track due to the damage, they
instead immediately regain points (equal to 1d6 + their tier) in their
Intellect Pools but lose 1 point from their maximums in that Pool.
Characters whose Intellect Pools reach 0 go insane. They lose their
current descriptor and adopt the Mad descriptor, regain 1d6 + tier
points to their Intellect Pools, and gain +1 to their Intellect Edge. If
they ever reach a permanent Intellect Pool maximum of 0 again, they go
stark raving mad and are no longer playable.

Intellect Edge offers an interesting means to portray a character who is
knowledgeable (and perhaps even powerful in terms of mental abilities)
yet mentally fragile. A character with a low Intellect Pool but a high
Intellect Edge can perform Intellect actions well (since Edge is very
helpful) but is still vulnerable to Intellect damage (where Edge is of
no help).

Since Cypher System games are meant to be story based, players should
recognize that the degrading sanity of their character is part of the
story. A player who feels that their character is going mad can talk to
the GM, and the two of them can work out the means to portray
that—perhaps by using the Mad descriptor, permanently trading up to 4
points from their Intellect Pool to gain +1 to their Intellect Edge, or
anything else that seems appropriate. Mental disorders, manias,
psychopathy, schizophrenia, or simple phobias can be added to a
character’s traits, but they don’t need to be quantified in game
statistics or die rolls. They’re simply part of the character.

Inabilities in personal interaction or any area requiring focus might be
appropriate, perhaps allowing the PC to gain training in weird lore or
forbidden knowledge. Or maybe the opposite is true—as the character’s
mind slowly slips away, they become oddly compelled or can obsessively
focus on a single task for indefinite periods, and thus they gain
training in that topic or skill. These kinds of changes could be
balanced with inabilities, such as being unable to remember important
details.

As another way to represent madness, the GM could hinder Intellect-based
tasks that would be considered routine, such as “remembering your
friends and family” or “caring what happens to your best friend” or
“stopping yourself from injecting a mysterious substance into your
veins.” These routine tasks normally have a difficulty of 0, but for a
PC who has lost their mind, they might have a difficulty of 1, 2, or
even higher. Now the character must make rolls to do even those simple
things.

