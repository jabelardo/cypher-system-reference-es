Genres
######

The Cypher System can be used to play in many settings. This chapter
provides additional information and rules for fantasy, modern, science
fiction, horror, romance, superheroes, post-apocalyptic, fairy tale, and
historical genres.

.. toctree::
   :maxdepth: 2

   fantasy
   modern
   science-fiction
   horror
   romance
   superheroes
   post-apocalyptic
   fairy-tale
   historical
