Creatures
#########

This chapter describes many common and uncommon creatures that the
characters might meet—and fight—in a Cypher System game and gives their
stats. The variety of creatures that populate the possible settings and
genres is so great that this chapter only scratches the surface. It
does, however, provide examples of kinds of inhabitants—bestial and
civilized, living and undead, organic and inorganic—so that you can
easily extrapolate and create your own.

Understanding the Listings
==========================

Every creature is presented by name, followed by a standard template
that includes the following categories.

Level:
    Like the difficulty of a task, each creature and NPC has a level
    attached to it. You use the level to determine the target number a PC
    must reach to attack or defend against the opponent. In each entry, the
    difficulty number for the creature or NPC is listed in parentheses after
    its level. As shown on the following table, the target number is three
    times the level.

    ===== =============
    Level Target Number
    ===== =============
    1     3
    2     6
    3     9
    4     12
    5     15
    6     18
    7     21
    8     24
    9     27
    10    30
    ===== =============

Description:
    Following the name of the creature or NPC is a general
    description of its appearance, nature, intelligence, or background.

Motive:
    This entry is a way to help the GM understand what a creature or
    NPC wants. Every creature or person wants something, even if it’s just
    to be left alone.

Environment:
    This entry describes whether the creature tends to be
    solitary or travel in groups and what kind of terrain it inhabits (such
    as “They travel in packs through dry wastes and temperate lowlands”).

Health:
    A creature’s target number is usually also its health, which is
    the amount of damage it can sustain before it is dead or incapacitated.
    For easy reference, the entries always list a creature’s health, even
    when it’s the normal amount for a creature of its level.

Damage Inflicted:
    Generally, when creatures hit in combat, they inflict
    their level in damage regardless of the form of attack. Some inflict
    more or less or have a special modifier to damage. Intelligent NPCs
    often use weapons, but this is more a flavor issue than a mechanical
    one. In other words, it doesn’t matter if a level 3 foe uses a sword or
    claws—it deals the same damage if it hits. The entries always specify
    the amount of damage inflicted, even if it’s the normal amount for a
    creature of its level.

Armor:
    This is the creature’s Armor value. Sometimes the number
    represents physical armor, and other times it represents natural
    protection. This entry doesn’t appear in the game stats if a creature
    has no Armor.

Movement:
    Movement determines how far the creature can move in a single
    turn. Creatures have movements of immediate, short, long, or very long,
    which equate to the ranges of the same name. Most PCs have an effective
    movement of short, so if they are chasing (or being chased by) a
    creature with immediate movement, their Speed tasks are eased; if the
    creature’s movement is long or greater, the PCs’ Speed tasks are
    hindered.

Modifications:
    Use these default numbers when a creature’s information
    says to use a different target number. For example, a level 4 creature
    might say “defends as level 5,” which means PCs attacking it must roll a
    target number of 15 (for difficulty 5) instead of 12 (for difficulty 4).
    In special circumstances, some creatures have other modifications, but
    these are almost always specific to their level.

Combat:
    This entry gives advice on using the creature in combat, such as
    “This creature uses ambushes and hit-and-run tactics.” At the end of the
    combat listing, you’ll also find any special abilities, such as
    immunities, poisons, and healing skills. GMs should be logical about a
    creature’s reaction to a particular action or attack by a PC. For
    example, a mechanical creation is immune to normal diseases, a character
    can’t poison a being of energy (at least, not with a conventional
    poison), and so on.

Interaction:
    This entry gives advice on using the creature in
    interactions, such as “These creatures are willing to talk but respond
    poorly to threats,” or “This creature is an animal and acts like an
    animal.”

Use:
    This entry gives the GM suggestions for how to use the creature in
    a game session. It might provide general notes or specific adventure
    ideas.

Loot:
    This entry indicates what the PCs might gain if they take items
    from their fallen foes (or trade with or trick them). It doesn’t appear
    in the game stats if the creature has no loot.

GM Intrusion:
    This optional entry in the stats suggests a way to use GM
    intrusion in an encounter with the creature. It’s just one possible idea
    of many, and the GM is encouraged to come up with their own uses of the
    game mechanic.

Basic Creatures
===============

Unlike many creatures in this chapter, normal animals are simple and
understandable enough to be encapsulated by just their level and maybe
one or two other stats.

Bear, black:
    level 3, attacks as level 4

Bear, grizzly:
    level 5; health 20; Armor 1

Dog:
    level 2, perception as level 3

Dog, guard:
    level 3, attacks and perception as level 4

Hawk:
    level 2; flies a long distance each round

Horse:
    level 3; moves a long distance each round

Rat:
    level 1

Rattlesnake:
    level 2; bite inflicts 3 points of Speed damage (ignores Armor)

List of Creatures
=================

This chapter includes details about the following creatures:
:ref:`creatures:Abomination 5 (15)`,
:ref:`creatures:Chimera 6 (18)`,
:ref:`creatures:Chronophage 4 (12)`,
:ref:`creatures:Deep One 4 (12)`,
:ref:`creatures:Deinonychus 3 (9)`,
:ref:`creatures:Demigod 9 (27)`,
:ref:`creatures:Demon 5 (15)`,
:ref:`creatures:Devil 4 (12)`,
:ref:`creatures:Djinni 7 (21)`,
:ref:`creatures:Dragon 7 (21)`,
:ref:`creatures:Fire Elemental 4 (12)`,
:ref:`creatures:Earth Elemental 5 (15)`,
:ref:`creatures:Enthraller 6 (18)`,
:ref:`creatures:Fallen Angel 5 (15)`,
:ref:`creatures:Fusion Hound 3 (9)`,
:ref:`creatures:Ghost 4 (12)`,
:ref:`creatures:Ghoul 4 (12)`,
:ref:`creatures:Giant 7 (21)`,
:ref:`creatures:Giant Rat 3 (9)`,
:ref:`creatures:Giant Snake 4 (12)`,
:ref:`creatures:Giant Spider 3 (9)`,
:ref:`creatures:Goblin 1 (3)`,
:ref:`creatures:Golem 6 (18)`,
:ref:`creatures:Grey 4 (12)`,
:ref:`creatures:Kaiju 10 (30)`,
:ref:`creatures:Killer Clown 5 (15)`,
:ref:`creatures:Killing White Light 5 (15)`,
:ref:`creatures:Mechanical Soldier 4 (12)`,
:ref:`creatures:Mi-Go 5 (15)`,
:ref:`creatures:Mokuren 3 (9)`,
:ref:`creatures:Nuppeppo 2 (6)`,
:ref:`creatures:Ogre 4 (12)`,
:ref:`creatures:Orc 2 (6)`,
:ref:`creatures:Prince(ss) of Summer 5 (15)`,
:ref:`creatures:Puppet Tree 6 (18)`,
:ref:`creatures:Ravage Bear 4 (12)`,
:ref:`creatures:Replicant 5 (15)`,
:ref:`creatures:Shadow Elf 4 (12)`,
:ref:`creatures:Skeleton 2 (6)`,
:ref:`creatures:Statue, Animate 7 (21)`,
:ref:`creatures:Tyrannosaurus Rex 7 (21)`,
:ref:`creatures:Vampire 6 (18)`,
:ref:`creatures:Vampire, Transitional 3 (9)`,
:ref:`creatures:Vat Reject 3 (9)`,
:ref:`creatures:Wardroid 6 (18)`,
:ref:`creatures:Werewolf 4 (12)`,
:ref:`creatures:Witch 5 (15)`,
:ref:`creatures:Xenoparasite 6 (18)`,
:ref:`creatures:Zombie 3 (9)`.

The :doc:`genres` chapter also includes suggestions
for creatures appropriate to various genres.

Abomination 5 (15)
------------------

An abomination is a hideous bestial humanoid covered with thickened
plates of scarlet flesh. Their eyes shine with the stagnant glow of
toxic waste dumps. Standing at least 7 feet (2 m) tall, abominations are
drawn to movement. Always famished, they consume living prey in great
tearing bites.

Motive:
    Hungers for flesh

Environment:
    Almost anywhere, hunting alone or in pairs

Health:
    22

Damage Inflicted:
    6 points

Armor:
    2

Movement:
    Short

Modifications:
    Might defense as level 6; sees through deception as level
    3

Combat:
    Abominations use scavenged weapons to attack prey at range, but
    probably switch to biting targets within immediate range. Targets
    damaged by a bite must also succeed on a Might defense task or descend
    one step on the damage track as the abomination tears off a big piece of
    flesh and gulps it down. Those who survive an attack must succeed on a
    Might defense task a day later when they come down with flu-like
    symptoms. Those who fail begin the process of transforming into a fresh
    abomination.

    Abominations regain 2 points of health per round and have +5 Armor
    against damage inflicted by energy (radiation, X-rays, gamma rays, and
    so on).

Interaction:
    Most abominations can speak and have vague memories of the
    people they were before transforming. However, those memories,
    motivations, and hopes are usually submerged in a hunger that can never
    be sated.

Use:
    Abominations hunt ravaged wastelands and bombed-out spacecraft
    hulks, lurk in basements where mad scientists have conducted illicit
    experiments, and haunt the dreams of children who’ve gotten in over
    their heads.

GM intrusion:
    The abomination isn’t dead; it stands up on the following
    round at full health.

Chimera 6 (18)
--------------

Chimeras are unsettling hybrids that combine the features of many
different animals, often arranged in odd formations. The fusion of
animal forms is the only thing that unifies these creatures—otherwise,
different chimeras often look very different from each other. They
include combinations of goat and lion, lizard and bat, dragon and
spider, dinosaur and giant insect. A few even display human features,
such as an improbably located face or hands instead of claws. Some
chimeras can fly. Others slither across the ground.

A chimera typically has a dominant form to which other animal parts are
grafted. The base form must be large enough to support the weight of the
extra heads, so lions, bears, and horses are popular as the base form.

Chimeras kill even when not hungry and throw their victims’ remains
around a wide area in a wild rage. When not feeding or tormenting prey,
a chimera that can fly takes to the air, beating its enormous leather
wings to scour the landscape for new prey.

Motive:
    Hungers for human flesh

Environment:
    Anywhere, usually alone

Health:
    21

Damage Inflicted:
    4 points

Movement:
    Short while on the ground; long while flying (if it can fly)

Modifications:
    Speed defense rolls as level 5 due to size

Combat:
    All chimeras have a number of ways to kill. The exact methods
    vary, but most can bite, sting, and gore (three attacks) as a single
    action, either attacking the same opponent or attacking different foes
    within immediate range of each other. A chimera’s sting carries a
    powerful toxin, and a stung target must succeed on a Might defense roll
    or take 4 additional points of damage. Chimeras with spikes can project
    them at up to three targets within long range as a single action.

Interaction:
    Chimeras are a lot like wild animals with rabies. They’re
    confused and violent, and they behave erratically. Savage, ferocious
    beasts, they hate all other creatures and seize any opportunity to kill.

Use:
    While exploring an island, the PCs find carcasses that have been
    torn apart, the pieces scattered in all directions. A chimera lairs
    nearby, and if the characters draw attention to themselves, it hunts
    them down, too.

GM intrusion:
    The chimera grabs a character it bites and flies off with
    the victim

Chronophage 4 (12)
------------------

These segmented, 6-foot (2 m) long creatures look partly like larvae
that have grown gargantuan and vicious. They appear in places where time
moves more slowly or more quickly than normal, where balls and liquids
flow upslope, or where a time traveler has visited.

Motive:
    Hungers for the flesh of those who create, or were created by,
    time anomalies

Environment:
    Clutches of four to eight fade into existence within long
    range of space-time fractures in almost any location.

Health:
    18

Damage Inflicted:
    5 points

Armor:
    1

Movement:
    Short; can phase into the dimension of time (and disappear) as
    a move. On its next action, it can phase back into the world up to 300
    feet (90 m) from where it disappeared (as an action).

Modifications:
    Perception as level 5

Combat:
    A chronophage attacks with its crushing mandibles.

    A chronophage can phase back and forth between its home dimension, and
    it uses this ability to great effect when hunting prey. For instance, it
    can close on prey otherwise protected by barriers or features of the
    landscape. It can also use the ability to draw a victim’s attention and
    then launch a surprise attack from behind after it has effectively
    teleported. However, it is an action for the creature to shift its phase
    between the dimension of time and normal reality.

Interaction:
    Chronophages are unswerving in their drive to find prey.
    Once one marks its target, only killing the creature can sway it from
    the prey.

Use:
    When the PCs happen upon a location where the rules of space-time
    are loose and malleable, or if the PCs trigger a cypher or other device
    that interferes with time’s regular flow, a clutch of chronophages may
    soon come calling.

Loot:
    The skin of a chronophage can be salvaged to create a silvery
    cloak that reflects its surroundings, but the reflection is one hour
    behind the present.

GM intrusion:
    If a chronophage’s prey fails its Speed defense roll, the
    attack ignores Armor, and the prey must make an Intellect defense roll
    (difficulty 4) or be phased into the chronophage’s home dimension of
    time. Victims automatically phase back into reality on their next turn
    but are displaced by 100 feet (30 m) straight up or to the closest open
    space. This usually results in a fall that potentially deals 10 points
    of damage, knocks victims prone, and dazes them, hindering all actions
    for a round.

Deep One 4 (12)
---------------

Some deep ones dwell in coastal regions on land, usually in isolated
villages where they might attempt to pass for human. They are able to
breathe both air and water. Most, however, thrive in the ocean depths,
in ancient underwater cities like “Cyclopean and many-columned
Y’ha-nthlei.” Deep ones sometimes breed with insane humans to produce
squamous offspring that eventually develop fully into deep ones well
after maturity (or even middle age).

Motive:
    Hungers for flesh

Environment:
    Anywhere near a large body of salt water

Health:
    15

Damage Inflicted:
    5 points

Armor:
    2

Movement:
    Short on land; long in the water

Modifications:
    Swims as level 6; perception as level 3

Combat:
    Deep ones attack with tooth and claw most often, although
    occasionally one might use a weapon. They usually give no quarter, nor
    ask for it. Their skin is subject to drying, and they take 1 extra point
    of damage (ignores Armor) from any attack that deals fire or heat
    damage. Because of this weakness, deep ones sometimes retreat from fire
    and fire attacks.

Interaction:
    Deep ones are a strange mix of utter alienness and the
    vestiges of lost humanity. They are foul and degenerate creatures by
    human standards, however. Many still retain the ability to speak human
    languages, but all speak their own slurred, unearthly tongue.

    Deep ones spend a great deal of time in the sincere adoration of their
    gods, Mother Hydra, Father Dagon, and Cthulhu. Their religion demands
    frequent blood sacrifices.

Use:
    The PCs wander into a small coastal village where everyone seems
    standoffish and oddly distant. A few people appear to be sickly and
    malformed, perhaps from mutation or birth defects. Some of the villagers
    have squamous skin because they are transforming into deep ones. And, of
    course, true deep ones hide within the community as well.

Loot:
    A few deep ones will have a cypher.

GM intrusion:
    The deep one produces a net and throws it over the
    character. The only physical action the victim can take is to try to get
    free, as either a Might-based or a Speed-based action.

Deinonychus 3 (9)
-----------------

Popularly known as the velociraptor, the dinosaur genus called
deinonychus doesn’t care if its prey gets the proper terminology sorted.
Meat tastes like meat. The “terrible claw” these carnivores are named
after refers to their massive, sickle-shaped claws, which are unsheathed
from their hind legs when attacking prey.

Deinonychus are pack hunters, which means they work together as a unit,
each taking on different roles to scare, flush, and direct even
intelligent prey into the claws of an ambush.

Motive:
    Hungers for flesh

Environment:
    Wherever they can hunt food, in packs of three to seven

Health:
    15

Damage Inflicted:
    4 points

Armor:
    1

Movement:
    Short

Modifications:
    Perception as level 5; attacks and Speed defense as level
    4 due to quickness; overcoming obstacles and figuring out tricks as
    level 4

Combat:
    When a deinonychus bites its prey, the victim takes damage and
    must make a Might defense roll. On a failure, the deinonychus holds the
    victim in place with its jaws while it slices them to ribbons with its
    terrible claws, automatically inflicting 6 points of damage each round
    in which they fail a Might-based task to break free (not attempting to
    break free counts as a failed attempt). For a human-sized or smaller
    victim held in the jaws, all other tasks are hindered by two steps.

Interaction:
    Vicious, cunning, and a little too smart to be classified
    as simple predators, these creatures are unlikely to negotiate, give
    quarter, or back off from a fight even if contact could be made.

Use:
    Some fool decided to build a Cretaceous-themed zoo. The only
    question is:
    How long before the dinosaurs get loose and take over the
    local mall?

GM intrusion:
    The fleeing deinonychus was actually leading the character
    over a cliff, into a deadfall trap, or into an ambush with more
    deinonychus.

Demigod 9 (27)
--------------

Lesser gods, divine children of gods and mortals, and other beings
bequeathed with partly divine power are called demigods. Their
capacities so radically exceed those of regular people that they have
transcended humanity. Demigods are so physically and mentally powerful
that it’s difficult for them to hide their semi-divine appearance to
mortal creatures—not that most would make the effort in the first place.

Motive:
    Ineffable

Environment:
    Anywhere other divine entities exist (or once existed)

Health:
    99

Damage Inflicted:
    12 points

Armor:
    5

Movement:
    Short; long when flying

Combat:
    Demigods can attack foes up to half a mile (1 km) away with
    bolts of divine energy (usually in the form of lightning). A demigod can
    dial up the level of destruction if it wishes, so that instead of
    affecting only one target, a bolt deals 9 points of damage to all
    targets within short range of the primary target. Targets caught in the
    conflagration who succeed on a Speed defense roll still suffer 5 points
    of damage.

    Demigods are just as scary in hand-to-hand combat and can attack all
    targets within immediate range as an action. They can also call on a
    variety of other abilities that seem like magic to lesser foes and mimic
    the effect of any cypher of level 5 or lower.

    A demigod doesn’t need to alter reality to heal itself, as it
    automatically regains 2 points of health per round.

Interaction:
    For all their power, demigods share most human traits and
    weaknesses. This means it’s possible to negotiate with one, though the
    consequences for angering a demigod in the process are dire.

Use:
    A demigod was banned from the higher realm of their birth for
    unknown reasons. Now they seek to show their worth by undertaking a
    great quest in the mortal world, and they are looking to assemble a
    group of mortal comrades (sycophants?) to aid them.

Loot:
    Demigods might carry an artifact related to some aspect of their
    domain (such as wind, messages, or death), if they have one, and 1d6
    cyphers.

GM intrusion:
    The divine nature of the demigod allows it to act out of
    turn, take control of an object (such as an artifact or a cypher) that
    the PC is about to use against it, and either deactivate the object or
    turn it against the character.

Demon 5 (15)
------------

Demons are formless spirits of the dead tortured in nether realms until
all that was good or caring in them was burned away, forging a being of
spite and hate.

A demon remembers only fragments of its former life—every good memory is
cauterized, and every slight, misfortune, snub, and pain is amplified,
motivating the creature to tempt others into the same state.

Having no flesh to call its own, a demon is a shadowed, ephemeral horror
able to possess others. A demon can cause great harm in a short time by
forcing its host to lie, steal, and harm loved ones.

Motive:
    Hungers for others’ pain and fear

Environment:
    Anywhere

Health:
    25

Damage Inflicted:
    6 points

Movement:
    Short; immediate while flying in immaterial form

Modifications:
    All stealth tasks as level 7 in immaterial form;
    deception tasks as level 6

Combat:
    The immaterial touch of a demon either inflicts 5 points of
    damage from rot, or allows the demon to attempt to possess the target.
    The target of an attempted possession must make an Intellect defense
    roll or become possessed, whereupon the demon’s immaterial form
    disappears into the target.

    The first round in which a character is possessed, they can act
    normally. In the second and all subsequent rounds, the possessing
    demon can control the actions of the host, but the
    character can attempt an Intellect defense roll to resist each
    suggested action. Successful resistance means that the character does
    nothing for one round. In other rounds, the character can act as they
    choose. A possessing demon’s actions are limited to attempts to
    control its host and leaving the host.

    A possessed target is allowed an Intellect defense roll to eject the
    demon once per day, barring any exorcism attempts. The defense roll is
    hindered by one additional step each day of possession after the first
    seven days. An ejected or cast-out demon is powerless for one or more
    days.

    A demon not possessing another creature is immaterial and can pass
    through solid objects whose level is lower than its own. While the demon
    is immaterial, it takes only 1 point of damage from mundane attacks, but
    it takes full damage from magical, energy, and psychic attacks. While it
    possesses another creature, the demon is immune to most attacks (though
    not so the host; killing the host will eject the demon).

Interaction:
    A demon allows a possessed host to act normally, as long as
    it doesn’t reveal the demon’s presence. If its presence is known, the
    demon might negotiate, but only after a tirade of lies and obscenity,
    and the demon likely betrays any deal reached.

Use:
    An ally of the PCs has begun acting differently, and not for the
    good.

GM intrusion:
    The character who attempts an exorcism of a possessed
    target is successful, but the demon moves directly from the former
    victim into the exorcist. The new host can make an Intellect-based roll
    to eject the demon, but only after the first round of possession.

Devil 4 (12)
------------

Devils are manifest evil. As “native fauna” of various tortuous nether
realms, devils come in many forms, though most are iterations on a theme
that includes a humanoid shape, large batwings, bestial faces, and
twisting horns. Most stink of brimstone and sport tails that end in a
fork. Devils fill the ranks of hellish armies, guard evil vaults, and
appear at the magical summons of warlocks and sorcerers who are not
afraid for the sanctity of their own souls.

Motive:
    Collect souls

Environment:
    Anywhere in various nether realms; sometimes called by
    mortal magic

Health:
    12

Damage Inflicted:
    5 points

Armor:
    3

Movement:
    Short when walking or flying

Modifications:
    All tasks related to deception as level 7

Combat:
    When possible, a devil attacks with surprise. If successful, it
    unfurls two great wings and claws at the ends of its fingers. It leaps
    into the air, flies up to a short distance toward the nearest foe, and
    attacks that creature as a single action.

    Some devils carry tridents. The weapon inflicts 5 points of damage,
    and the target must either move to a position within an immediate
    distance chosen by the devil or take 2 additional points of damage
    from being impaled (a total of 7 points of damage). Impaled foes
    automatically take 5 points of damage each round until they use an
    action to pull
    themselves free.

Interaction:
    Evil, cruel, and malevolent, devils are more than happy to
    talk, especially to those already caught and being readied for torture.
    Devils serve yet more powerful devils out of fear. If they find someone
    or something they fear more, they readily betray their master and become
    obsequious and cringing, though further betrayal is always on the table.

Use:
    A spate of violent murders grips a city in fear—a devil has escaped
    into the world of mortals without a leash. It spends nights hunting
    anyone it spots from its perches atop the city’s holy places.

GM intrusion:
    A devil anticipates the character’s melee attack and
    brings its wing down “just so” on the attacker’s weapon. If the
    character fails a Speed defense roll, the weapon breaks. Either way, the
    attack fails to hit the devil.

Djinni 7 (21)
-------------

Djinn inhabit unseen dimensions beyond the visible universe. Just like
normal creatures, djinn are individuals, and they can be good, evil, or
unconcerned about the fates and doings of others.

Motive:
    Unpredictable

Environment:
    Almost anywhere

Health:
    35

Damage Inflicted:
    9 points

Movement:
    Short; long when flying

Modifications:
    Knowledge of Arabian history as level 8

Combat:
    With a touch, a djinni can warp a victim’s flesh, inflicting
    damage. Djinn can also use an action to send out a magitech “EMP burst”
    that renders all artifacts, machines, and lesser magic devices within
    short range inoperable for one minute. (If the item is part of a
    character’s equipment, they can prevent this outcome by succeeding on a
    Speed defense roll.) Instead of disabling all devices in range, a djinni
    can instead take control of one item within range for one minute, if
    applicable.

    A djinni can transform into a being of smoke and flame as its action.
    While in this form, it has +10 to Armor but can’t attack foes. It gains
    the ability to fly a long distance each round and retains the ability to
    communicate normally. The first time each day that a djinni returns to
    physical form after having become smoke, it regains 25 points of health.

    Some djinn have the ability to grant wishes, and a few are beholden to
    do so thanks to an ancient, unexplained agreement with other djinn.
    Those who grant wishes twist them against the asker, especially if a
    wish is poorly worded or there are multiple ways to interpret it. The
    level of the effect granted is no greater than level 7, as determined by
    the GM, who can modify the effect of the wish accordingly. (The larger
    the wish, the more likely the GM will limit its effect.)

Interaction:
    When a djinni interacts with characters, it’s narcissistic,
    certain in its own immense power, and unlikely to let slights pass. That
    said, low-tier characters could negotiate with one peacefully because
    even djinn have needs and desires.

Use:
    Agents of a foreign power retrieved a magic lamp from an ancient
    Arabian ruin. The PCs’ job is to determine whether there is reason for
    alarm.

Loot:
    Most djinn carry a couple of cyphers, and some have a magic
    artifact useful in combat.

GM intrusion:
    When the character is touched by a djinni, instead of
    taking damage, the character is turned to smoke and fire and sent
    whirling off in a random direction. They lose their next turn and return
    to normal almost 300 feet (90 m) from where they started.

Dragon 7 (21)
-------------

Dragons are exceptionally territorial, vain, and greedy. Apex predators,
dragons must eat large meals on a regular basis. They prefer virgins,
though they will settle for whoever, or whatever—such as horses or wild
pigs—is available in a pinch. They love games of all sorts, especially
when they get to consume the loser. Drawn to wealth and magic, dragons
accumulate hoards of golden treasure. A dragon’s hoard is not only an
end in itself, but part of a never-ending contest between dragons of a
certain age to see which one can accumulate the largest trove.

Motive:
    Self-aggrandizement, hungers for flesh, treasure collection

Environment:
    Dragons thrive where wilderness meets the civilized
    frontier.

Health:
    45

Damage Inflicted:
    10 points

Armor:
    3

Movement:
    Short; long while flying

Modifications:
    Perception and riddles as level 8; Speed defense as level
    6 due to size

Combat:
    A dragon can bite one target or claw two opponents in immediate
    range as a single action. When bitten, targets are also immobilized
    until they succeed on a Might defense roll to break free (or the dragon
    drops them).

    Most dragons have one or more additional magical abilities they can
    bring to bear in combat, including the following.

    Captivate:
        A dragon with this ability can psychically mesmerize a
        nondragon target in immediate range who fails an Intellect defense roll.
        A captivated target does the dragon’s verbal bidding for one or more
        hours. Each time the target is confronted by a third party about its
        mental condition, the target is allowed another Intellect defense roll
        to break the effect.

    Change Shape:
        A dragon with this ability can take the form of a human
        or similar humanoid as its action, or return to its regular shape. When
        so changed, the dragon’s disguise is nearly impenetrable without special
        knowledge. As a human, the dragon is a level 5 creature.

    Fiery Breath:
        A dragon can breathe a stream of fire up to long range,
        doing 7 points of damage to all targets within immediate range of each
        other. Targets who succeed on a Speed defense roll to avoid the full
        effect of the fire still take 3 points of damage. This ability cannot be
        used in consecutive rounds.

Interaction:
    Like the many hues of dragon scales, dragon personalities
    run the gamut from beastly thug to refined connoisseur. Some dragons lie
    with every smoky breath, others consider the least bit of dishonesty a
    personal failing, and most fall somewhere in between. All of them can be
    flattered and even charmed by someone with courtly manners and grace.

Use:
    A dragon confronts the PCs, challenging them to a riddle game. If
    the characters win, they get a cypher. If the dragon wins, the PCs owe
    it a favor to be specified later . . . unless the dragon is hungry now.

Loot:
    A dragon’s hoard might contain 2d6 cyphers, hard currency
    equivalent to 1d6 exorbitant items, and possibly a few artifacts (but a
    hoard is usually well guarded).

GM intrusion:
    The dragon breathes fire while the character is caught in
    its mouth, which automatically inflicts maximum fire damage on them.

Fire Elemental 4 (12)
---------------------

Searing flame in a vaguely humanoid shape, a fire elemental exists only
to burn that which is not already ash. They sometimes spin into being
where great conflagrations burn.

Motive:
    Burn

Environment:
    Anywhere fires can burn

Health:
    24

Damage Inflicted:
    4 to 7 points; see Combat

Movement:
    Short

Modifications:
    See Combat for escalating attack level modification.

Combat:
    A fire elemental attacks with a flaming limb. The more the
    elemental burns foes, the more powerful it grows. Its power increases
    according to the number of successful attacks (that dealt fire damage)
    it made on another creature during the previous minute.

    0 successful attacks:
        deals 4 points of damage; attacks as level 4

    1 successful attack:
        deals 5 points of damage; attacks as level 5

    3 successful attacks:
        deals 6 points of damage; attacks as level 6

    4+ successful attacks:
        deals 7 points of damage; attacks as level 7

    If a fire elemental hasn’t burned a foe within the last minute, its
    combat stats drop back to its level 4 baseline.

    A fire elemental is immune to fire attacks but vulnerable to cold; every
    time it takes 1 point of cold damage, it takes 1 additional point of
    damage.

Interaction:
    Fire elementals are barely sapient and usually respond only
    to those who know spells able to command them. However, there’s a chance
    (about 10%) that a fire elemental commanded to accomplish a particular
    task breaks free and instead burns whatever’s around until it exhausts
    all possible fuel sources.

Use:
    A rash of fires leads some people to suspect that an arsonist is on
    the loose, but the truth is worse.

GM intrusion:
    A character hit by the fire elemental’s attack catches on
    fire and takes 3 points of damage each round until they use an action
    patting, rolling, or smothering the flames.

Earth Elemental 5 (15)
----------------------

An excavation, a meteor fall, a still-shuddering earthquake—all these
events can summon an earth elemental to take shape and expand the
destruction further.

Motive:
    Crumble and break, reduce things to earth

Environment:
    Anywhere solid or earthen

Health:
    30

Damage Inflicted:
    6 points

Armor:
    3

Movement:
    Immediate; short when burrowing

Combat:
    Earth elementals batter foes with heavy fists. They can also
    create earthquakes (no more than once every other round) that affect the
    ground within short range. Creatures standing in the area fall to the
    ground and take 5 points of damage on a failed Might defense roll.

    An earth elemental is vulnerable to water. Any damage it takes while
    standing in or being doused in water ignores its Armor.

Interaction:
    Although brooding and slow to respond if encountered as
    immobile stone, earth elementals are intelligent. The ones that are
    summoned with a spell have about a 5% chance of breaking the geas and
    turning on their summoner.

Use:
    Oddly articulated monoliths were discovered high in the mountains
    around a shrine containing an ancient treasure. A merchant wants someone
    to investigate the monoliths in case they represent a trap. In fact, the
    monoliths are inactive earth elementals.

GM intrusion:
    A character within range of the earth elemental’s
    earthquake attack must succeed on a Speed defense roll or be covered in
    an avalanche from a collapsing structure or cliff face.

Enthraller 6 (18)
-----------------

Hundreds of thousands of years ago, enthraller ancestors psychically
dominated a group of interstellar spacefarers who had the misfortune to
land on the enthraller homeworld. Leapfrogging technological prowess by
mentally commandeering the know-how of every new species they
encountered using their stolen space vessel, the aliens fashioned the
Enthraller Dominion, which stretches across vast swaths of space,
cemented by the psychic control.

Individual enthrallers are scary, but enthraller overlords are even more
powerful thanks to technological aids. These include cranial circlets
that give a single enthraller governor the ability to dominate a small
city, solar-system-sized ring relays that boost their control across
interstellar distances, and more.

Recently, a newly contacted species of aliens developed the
technological means to resist the mental influence of the enthrallers.
Now war bubbles across the Enthraller Dominion. Sometimes individual
enthrallers, stripped of their technological enhancements as a
consequence of this war, flee into virgin space, looking for new
soldiers to dominate.

Motive:
    Domination of other creatures

Environment:
    Almost anywhere, alone or in groups of three

Health:
    18

Damage Inflicted:
    4 points; see Combat

Armor:
    1

Movement:
    Short

Modifications:
    Speed defense as level 4; perception and ability to
    detect falsehoods as level 8

Combat:
    An enthraller usually relies on dominated minions to make
    physical attacks on its behalf. An enthraller can
    make a psychic attack on a creature within short range. On a failed
    Intellect defense roll, the target acts as the enthraller mentally
    commands on its next action. If the same target is affected by this
    dominating attack a second time within a minute, the enthraller’s
    mental control lasts for one minute.

    Alternatively, as its action, an enthraller can emit a psychic burst
    that can target up to three creatures in short range. On a failed
    Intellect defense roll, a victim suffers 4 points of Intellect damage
    (ignores Armor) and is unable to
    take actions on their subsequent turn. If the victim is attacked while
    so stunned, their defense rolls are hindered by two steps.

    The enthraller’s attack is a form of mental feeding. If it moves a PC
    down the damage track, the creature regains 4 points of health.

Interaction:
    An enthraller can communicate telepathically with
    characters within short range. It tries to mentally dominate whoever it
    runs across and will negotiate only with characters who are strong
    enough to harm it. Even if an enthraller makes a deal, it eventually
    reneges if it senses any advantage for doing so because it implicitly
    believes that other creatures are cattle.

Use:
    A spacecraft (or perhaps an escape pod) crash lands. Inside, a hurt
    enthraller lies in suspended animation. Investigators are unlikely to
    realize the enthraller’s nature beforehand, but they certainly learn if
    they wake the alien.

Loot:
    Enthrallers wear light armor suited for their forms. They might
    have one or two cyphers and, rarely, an artifact that boosts their
    already-fearsome mental capabilities.

GM intrusion:
    The enthraller’s intrusion into the character’s mind stirs
    up forgotten memories. The character must deal with the contents of
    these memories and perhaps why they were repressed.

Fallen Angel 5 (15)
-------------------

Angels are normally associated with virtue and service to higher moral
beings. But just like people, sometimes angels are tempted into impure
acts. Those who stray too far over the line may fall from higher realms
and be forced to walk the Earth in penance. This experience drives most
fallen angels insane.

Fallen angel abilities wax and wane according to the position of the
sun. During the day, a fallen angel seems almost sane (and is less
dangerous), but at night, it is volatile and threatening to everyone.

Motive:
    Revenge (but on whom and for what isn’t clear, even to the
    fallen angel)

Environment:
    Anywhere, sometimes living alone in the wilderness, other
    times walking the hard streets of large cities

Health:
    25

Damage Inflicted:
    6 points by day, 8 points at night

Armor:
    2

Movement:
    Short; long when flying

Modifications:
    At night, perceptions and attacks as level 7

Combat:
    At night, a fallen angel can attack other creatures by
    projecting a long-range beam of burning light. Against foes within
    immediate range, the fallen angel manifests burning wings. A fallen
    angel can choose to make its attacks ignore Armor, but for each attack
    so modified, it loses 4 points of health.

    On the rare occasion that a fallen angel is within immediate range of
    another of its kind, both regain 1 point of health per round.

    By day, a fallen angel cannot project
    long-range attacks and has no visible wings with which to make melee
    attacks, though it may carry a melee weapon.

Interaction:
    By day, fallen angels are not automatically hostile, and
    they can be negotiated and reasoned with. They can seem truly angelic,
    though they are often confused and forgetful of their origin. But when
    night descends, fallen angels lose control of their faculties as they
    swell with rage and power. Unless a character directs a fallen angel
    toward another creature on which it can vent its wrath, the character
    becomes the object of the fury.

Use:
    A star slips down from the sky and lands in the country. The next
    day, travelers come upon a farm in the area and find everyone dead and
    burned. A trail of scorched earth leads up into the hills.

Loot:
    Fallen angels collect cyphers and usually have a few.

GM intrusion:
    A fallen angel’s successful attack causes the
    character’s cypher to detonate (if a grenade) or otherwise activate in
    a
    less-than-ideal fashion.

Fusion Hound 3 (9)
------------------

In radiation-scoured wastelands, either creatures adapt to the deadly
energies of their environment, or they die. Fusion hounds are mutant
canines able to absorb unbelievable amounts of radiation and thrive on
it. They roam in packs, killing and devouring everything they come upon.

A fusion hound’s entire head appears to be a blast of flame, and gouts
of dangerous radiation flare from its body.

Motive:
    Hungers for flesh

Environment:
    Packs of three to eight can be found almost anywhere.

Health:
    10

Damage Inflicted:
    5 points

Armor:
    1

Movement:
    Long

Modifications:
    Speed defense as level 4; stealth and climbing as level 2

Combat:
    Fusion hounds move very fast and use that speed to their
    advantage in combat. A hound can move a long distance and still attack
    as a single action. It can also use its action to run about in random
    patterns, hindering attacks against it by two steps.

    A fusion hound’s head is completely haloed in a seething mass of
    radioactive energy, so unlike traditional canines, it has no bite
    attack. Instead, it pounces on prey with its clawed forelimbs, which
    causes a burst of radiation to flare from its body, burning whatever it
    touches.

    Anyone within close distance of a fusion hound for more than one round
    suffers 1 point of damage in each round after the first.

Interaction:
    Fusion hounds are animals. Creatures immune to radiation
    sometimes train the hounds to become guardians or hunting dogs, but such
    creatures are rare.

Use:
    An NPC delivering something the characters need never made it to
    the rendezvous. If they backtrack to where the NPC should have come
    from, the PCs are attacked by a pack of fusion hounds on the road.
    Clearly, the courier was attacked by the pack as well, and the
    characters must discover if the NPC is dead or merely injured, and where
    the package now lies.

GM intrusion:
    The hound flares with energy and the character must
    succeed on a Might defense task or go blind for ten minutes.

Ghost 4 (12)
------------

Sounds with no apparent origin, such as the tap of footsteps on the
stair, knocking behind the walls, crying from empty rooms, and haunting
music, might be signs of a ghost. If the sound is accompanied by a
sudden temperature drop and the breath of living creatures begins to
steam, it’s a certainty.

Ghosts are the spectral remnants of humans, which persist either as
fragments of memory or as full-fledged spirits. Though their appearance
varies between individuals, many appear somewhat translucent, washed
out, or physically warped from their time spent as a phantom.

Motive:
    Unpredictable (but often seeking to complete unfinished
    business)

Environment:
    Almost anywhere

Health:
    12

Damage Inflicted:
    5 points

Movement:
    Short

Modifications:
    Stealth as level 7; tasks related to frightening others
    as level 6

Combat:
    A ghost doesn’t take damage from mundane physical sources, but
    it takes half damage from spells and attacks that direct energy, and
    full damage from weapons designed to affect spirits, psychic attacks,
    and similar attacks.

    A ghost’s touch inflicts freezing damage. Some ghosts can kill victims
    with fear. A ghost with this ability can attack all creatures within
    short range with a psychic display so horrible that targets who fail an
    Intellect defense roll take 4 points of Intellect damage (ignores Armor)
    and become terrified, freezing in place. In each subsequent round, a
    terrified victim can attempt an Intellect-based task to push away the
    fright. Each failed attempt moves the victim one step down the damage
    track. Not attempting to clear one’s mind of fear counts as a failed
    attempt. Those killed by fear are marked by expressions of horror and
    hair that has turned white.

    A ghost can move through solid objects of up to level 7 at will,
    although it can choose to pick up and manipulate objects if it focuses
    on them. Ghosts can also go into a state of apparent
    non-existence for hours or days at a time.

Interaction:
    Some ghosts are talkative, some don’t know they’re dead,
    some want help for a task
    they failed to accomplish in life, and some only rage against the
    living and want to bring those who yet breathe into the same colorless
    existence they endure.

Use:
    A ghost (that at first appears fully human) wants help in
    eradicating a guild of ghost hunters that has targeted it and a few
    others haunting an abandoned structure. The ghost promises to tell
    secrets of the afterlife to any who accept its strange offer.

Loot:
    A ghost usually doesn’t carry objects, though some might have a
    keepsake (like an amulet showing the face of a loved one) or an
    artifact.

GM intrusion:
    The character must succeed on an Intellect defense roll or
    be possessed by the ghost until they succeed on an Intellect-based task
    to push it out. While possessed, the character acts just like the ghost
    did when it was alive.

Ghoul 4 (12)
------------

Ghouls spend almost as much time beneath the ground as corpses do, but
ghouls are very much alive. Their bodies are hairless and so
porcelain-smooth that their faces are sometimes mistaken for masks,
albeit gore-smeared masks. Ghouls come to the surface at night to gather
humanoid remains or steal those recently interred from their graves,
though many prefer to eat from still-living victims.

Most ghouls are orgiastic eaters of human flesh, but a rare few ghoul
populations are more refined. These wear clothes, have language and
sophisticated customs, live in grand subterranean cities of their own
design, and fight with milk-white blades of bone. These civilized ghouls
claim to hold dominion over the remains of all humans, according to
ancient custom, even if they only sometimes assert that privilege. They
eat the dead in order to absorb residual memories left in the corpses.

Motive:
    Hunger for dead flesh; knowledge (in certain rare cases)

Environment:
    Anywhere above ground at night, usually in groups of three
    or more, or in subterranean lairs

Health:
    12

Damage Inflicted:
    5 points

Movement:
    Short

Modifications:
    Two areas of knowledge as level 5

Combat:
    Ghoul saliva contains a paralytic agent. Ghoul bites (and
    weapons used by ghouls) inflict damage and, on a failed Might defense
    roll, render the target paralyzed for one minute. A paralyzed target can
    attempt a Might-based task each round to regain mobility, but for the
    next minute, attacks, defenses, and movement tasks are hindered.

    Ghouls can see in the dark. They’re blind in full daylight, but
    civilized ghouls who travel to the surface carry lenses that cover their
    eyes, allowing them to see without penalty in full sunlight.

Interaction:
    Common ghouls can’t be negotiated with, though a rare
    civilized ghoul is an excellent linguist. These latter are willing to
    deal in return for the body of someone who was knowledgeable or who kept
    valuable secrets in life.

Use:
    If a PC needs a piece of information not otherwise obtainable, a
    trip down into a ghoul city might be worthwhile, for the creatures are
    rumored to keep lightless libraries below the earth that store knowledge
    once known by humans.

Loot:
    If the PCs defeat a group of civilized ghouls, they might find a
    cypher and a few sets of black goggles that allow the wearer to look
    directly at the sun and see it as a pale circle.

GM intrusion:
    The ghoul spits in the character’s eye, directly
    introducing the paralytic into the victim’s bloodstream. The victim’s
    Might defense roll to avoid becoming paralyzed is hindered.

Giant 7 (21)
------------

Violent storms, earthquakes, typhoons, and other natural disasters draw
giants. Standing 20 to 30 feet (6 to 9 m) tall, giants delight in
rampaging through the middle of such calamities, creating even more
destruction. Some giants grow so powerful that they can trigger natural
disasters on their own.

Motive:
    Destruction

Environment:
    Underground, deserts, mountaintops, and similar desolate
    areas

Health:
    40

Damage Inflicted:
    9 points

Armor:
    1

Movement:
    Short

Modifications:
    Speed defense as level 5 due to size; breaks and throws
    objects as level 8; sees through deceptions and tricks as level 3

Combat:
    Giants smash foes with their fists, possibly catching up to
    three human-sized targets with the same attack if all the targets are in
    immediate range of each other.

    If a giant attacks a single target, they can choose to do regular damage
    or to grab hold of the victim, dealing 4 points of damage instead. On
    their turn, the victim can attempt a Might defense roll to struggle out
    of the grip, a Speed defense roll to slip out, or an Intellect-based
    task to distract the giant. If the victim fails, the giant throws the
    victim as high and as far as they can on their next turn. Damage on
    impact varies, depending on the environment, but a victim takes an
    average of 10 points of ambient damage.

    A few giants can generate storms, tidal waves, earthquakes, and similar
    phenomena that can lash an area up to 1,000 feet (300 m) across for up
    to a minute, inflicting 3 points of damage each round to all creatures
    and objects not protected by shelter designed to withstand a storm
    (though few shelters protect against an earthquake).

Interaction:
    Most giants are not very bright. When a giant is rampaging,
    someone could attempt to distract them by singing, juggling, or doing
    some other trick, which some giants will pause to watch for at least one
    or two rounds.

Use:
    A giant came down out of the mountains and laid waste to half the
    nearby village. Survivors will pay someone to venture into the giant’s
    mountain lair and destroy the creature.

Loot:
    Individual giants carry little, but giant lairs may contain
    currency equivalent to 1d6 expensive items, 1d6 cyphers, and a couple of
    artifacts.

GM intrusion:
    The giant’s blow sprains one of the character’s limbs,
    making it useless for ten minutes.

Giant Rat 3 (9)
---------------

Giant rats are as large as big attack dogs, just as vicious, and more
wily. Some giant rats are the lone matriarchs of a pack of ordinary
level 1 rats, and others are just one of several making up a colony of
oversized rodents. Like their smaller cousins, giant rats are known for
harboring virulent disease.

Motive:
    Defense, reproduction

Environment:
    Anywhere in ruins or sewers, in groups of one to seven

Health:
    18

Damage Inflicted:
    4 points

Movement:
    Short; long when jumping

Modifications:
    Perception as level 4; tasks related to overcoming
    obstacles and puzzles as level 5

Combat:
    Victims damaged by a giant rat’s diseased teeth and claws take 4
    points of damage and, on a failed Might defense roll, are infected with
    a level 5 disease. Within twelve hours, the victim’s lymph glands swell,
    creating visible buboes. Every twelve hours thereafter, the victim must
    succeed on a Might defense roll or take 5 points of ambient damage.

Interaction:
    Giant rats stubbornly pursue prey, but they flee if that
    prey proves to be too strong.

Use:
    A contact of the PCs dies of plague before they can deliver an
    important message. The PCs will have to backtrack the contact’s
    movements to discover what they wanted to say, which leads to a giant
    rat colony.

GM intrusion:
    A swarm of twelve ordinary rats—each level 1, but acting
    like a level 3 swarm—is summoned by the high-pitched squeaking of a
    giant rat.

Giant Snake 4 (12)
------------------

Those about to stumble into the presence of a giant snake at least 50
feet (15 m) long are warned by the skin it shed and discarded and by the
cracked, slippery bones of digested victims.

Motive:
    Hungers for flesh

Environment:
    Anywhere a giant snake can lurk, including jungles, sewers,
    caves, and spacecraft access tubes

Health:
    18

Damage Inflicted:
    5 points or more; see Combat

Armor:
    2

Movement:
    Short

Modifications:
    Perception and stealth as level 6; Speed defense as level
    3 due to size

Combat:
    A giant snake bites foes, preferably from ambush, hindering the
    target’s Speed defense by two steps. If it succeeds, the snake’s bite
    deals 8 points of damage for that attack. On a failed Might defense
    roll, a bite also inflicts 3 points of Speed damage (ignores Armor). A
    giant snake may coil around a sleeping, stunned, or debilitated victim.
    Caught victims automatically take 5 points of crushing damage each round
    until they break free.

    Giant snakes lose their perception and stealth modifications in cold
    climates and when attacked with abilities that reduce the temperature.
    Thus, the creatures retreat from cold.

Interaction:
    A giant snake is a predator that regards other creatures as
    food, though it ignores them when it is already busy digesting a meal.

Use:
    Characters note something amiss as they glimpse lambent eyes
    peering from the darkness, glaring as if seeking to pin victims in place
    with cold terror.

Loot:
    A giant snake’s droppings or gullet might hold a few cyphers and
    possibly an artifact that the creature could not digest.

GM intrusion:
    The snake’s venom affects the character more strongly.
    Instead of merely inflicting Speed damage, it also paralyzes the
    character for one minute, though after a couple of rounds, the victim
    can make another Might defense roll to throw off the effects of the
    poison early.

Giant Spider 3 (9)
------------------

Giant spiders result most commonly from radioactive accidents, magic, or
genetic manipulation. Whatever their origin, they’re terrifying hunters
large enough to predate people. The creatures range from the size of a
large dog to the size of a large horse.

Motive:
    Hungers for blood

Environment:
    Anywhere webs can be spun in the dark

Health:
    12

Damage Inflicted:
    3 points

Movement:
    Short; long when traveling on their webs

Modifications:
    Perception as level 5; Speed defense as level 4 due to
    quickness

Combat:
    A giant spider’s envenomed fangs inflict 3 points of damage,
    plus 3 points of Speed damage (ignores Armor) if a victim fails a Might
    defense roll. Debilitated victims are not killed but instead cocooned
    and hung for later dining. Giant spider webs (level 4) can hold victims
    immobile and unable to take actions until they manage to break free.

    Giant spiders lose their perception and Speed defense modifications in
    bright light and thus often retreat from intense illumination.

Interaction:
    Most giant spiders are simple predators and react
    accordingly.

Use:
    Giant spider webs can infest unlit alleys, dungeon corridors, dark
    forests, and darkened hallways of decommissioned genetic labs.

Loot:
    Cocooned corpses of previous victims hanging in a giant spider’s
    web sometimes contain all manner of valuables, including cyphers.

GM intrusion:
    Giant spider eggs hatch, and a level 3 swarm of tiny
    spiders attacks the character.

Goblin 1 (3)
------------

Goblins are wicked, grasping, and perversely resourceful. Usually no
larger than children, they can seem like pesky rabble, but that illusion
hides something altogether more cunning. Tribe members work together to
accomplish their goals of murder, kidnapping, and theft.

Motive:
    Greed and theft

Environment:
    Tunnels and caves, usually in groups of ten or more

Health:
    3

Damage Inflicted:
    2 points

Movement:
    Short

Modifications:
    Tasks related to perception, stealth, and setting traps
    as level 5

Combat:
    Goblins attack from the shadows with ambushes and hit-and-run
    tactics. When they have surprise, they attack as level 4 creatures and
    deal 2 additional points of damage, and they attempt to draw larger prey
    into level 5 traps they’ve previously set. They often flee in the face
    of real danger.

Interaction:
    Goblins are lying tricksters but can be cowed into
    cooperating for short periods.

Use:
    Thieves and murderers, goblins are foes to all, even rival goblin
    tribes.

Loot:
    Aside from weapons, each goblin carries a personal stash,
    including bones, shiny rocks, sticks, and other bits of worthless trash,
    plus currency equivalent to an inexpensive item.

GM intrusion:
    The goblin poisoned its knife. If struck, the character
    must make a Might defense roll or immediately move one step down the
    damage track.

Golem 6 (18)
------------

Animate creatures of stone created by magic for a specific purpose,
golems usually serve as guardians. However, they may also serve as
soldiers, couriers, and banner-bearers. Golems that have accomplished
their task may spend years without moving, like statuary posed in
unexpected places—stained, eroded, and forlorn. But if disturbed, a
golem rumbles back to movement and attempts to restart the last task
assigned to it by its maker.

Motive:
    Seeks to fulfill the commands of its creator

Environment:
    Anywhere that needs a sturdy magical guardian

Health:
    30

Damage Inflicted:
    8 points

Armor:
    5

Movement:
    Short

Modifications:
    Intellect defense as level 2; Speed defense as level 4
    due to slowness

Combat:
    Skilled with large two-handed weapons, golems inflict 2
    additional points of damage (total of 8 points) when using them. Golems
    cannot be stunned or dazed. They are immune to most poisons and disease,
    and 2 of their 5 points of Armor protect against ambient damage
    (environmental damage, heat, cold, falling, and so on).

    On the other hand, golems are activated by light, even light as dim as a
    candle. In complete darkness, a golem is blind and suffers penalties to
    attack and defend normally. A golem subject to complete darkness may
    choose to freeze in place like a statue. When one does so, its Armor
    increases to 10 (and Armor against ambient damage increases to 5), but
    it can take no actions, including purely mental actions. Unless
    something can damage the golem through its Armor, it remains frozen
    indefinitely or until light returns.

    Even if a golem is completely destroyed, the rubble of its form slowly
    reassembles over the course of three days, unless that rubble is ground
    to the finest gravel and spread widely.

Interaction:
    Most golems can’t speak. Those that can are mournful, and a
    few have become cruel in their isolation, but at heart, all are lonely.
    Many are also tired of their stone existence, in which they can move but
    not really feel, and they wish for some sort of final end.

Use:
    Powerful sorcerers sometimes create golems and press them into
    service with yet more spells. These golems prove to be tough bodyguards,
    but sometimes the futility of such service overcomes a golem and it
    turns on the sorcerer, breaking free of the binding spells in its rage
    over being denied the peace of death.

GM intrusion:
    The character hit by the golem is also grabbed and
    headbutted for 6 additional points of damage. The victim must break or
    slip free, or else they remain in the golem’s grip.

Grey 4 (12)
-----------

Greys are enigmatic creatures born of alien stars (or dimensions) who
have learned to move across the vast distances that bridge neighboring
star systems. The creatures descend through the atmosphere under the
cover of night to abduct specimens for study and return the victims
later after a thorough examination. Returned abductees are usually
befuddled and confused, and they retain little memory of what happened
to them. Victims of the greys’ examination frequently sport strange
marks on their flesh, oddly shaped wounds, gaps where teeth used to be,
and strange or unknown metal lodged somewhere under the skin.

A grey stands 3 feet (1 m) tall. It has a narrow body with skinny limbs
and a large, bulbous head. Two large black eyes, almond shaped, dominate
a face that has only a suggestion of a nose and a narrow mouth. Greys
wear skintight uniforms, carry numerous instruments to study their
environments, and keep a weapon or two for protection.

Motive:
    Knowledge

Environment:
    Greys land their spacecraft in remote areas, where they
    have minimal risk of discovery.

Health:
    12

Damage Inflicted:
    6 points

Armor:
    1

Movement:
    Short

Modifications:
    All tasks related to knowledge as level 6; Speed defense
    as level 5 due to size and quickness

Combat:
    A grey carries a powerful ray emitter that can burn holes
    through solid steel. The grey can use the emitter to attack targets
    within long range. Against dangerous opponents, a grey can use an action
    to activate a personal shield that encapsulates it in a bubble of force.
    The shield gives it +3 to Armor, but while the shield is active, the
    grey can’t fire its ray emitter.

    Greys are scientists, but cautious ones. Leaving a trail of corpses as
    evidence of their existence isn’t their preferred mode of operation. For
    this reason, one grey in every group has a memory eraser. When this grey
    activates the device, each target other than a grey within short range
    must succeed on an Intellect defense roll or become stunned for one
    minute, taking no action (unless attacked, which snaps the victim out of
    the condition). When the effect wears off naturally, the target has no
    recollection of encountering little grey creatures.

Interaction:
    Greys are curious about the places they visit but reluctant
    to move or act in the open. Secretive and mysterious, they prefer to
    observe creatures from afar and, on occasion, pick them up for closer
    inspection. Someone who offers a grey true knowledge might be treated as
    an equal rather than a lab animal.

Use:
    The PCs are called to investigate a series of disappearances of
    animals and people. One by one, the abductees return, usually in odd
    places, and always bearing physical markings that suggest they were
    subjected to invasive procedures. To protect others from a similar fate,
    the PCs must catch the abductors in the act.

Loot:
    A grey has one or two cyphers and might have a memory
    eraser that works as described under Combat (depletion roll
    of 1–2 on a 1d10).

GM intrusion:
    A grey’s ray emitter suffers a terrible mishap and
    explodes. The device kills the grey and destroys its body completely.
    For the next day, creatures that come within a short distance of where
    the grey died take 4 points of ambient damage from the psychic radiation
    each round they remain there.

Kaiju 10 (30)
-------------

Kaiju come in a variety of shapes, but all share one difficult-to-ignore
quality: mind-blowing size. Appearances of these colossal creatures are
rare events that usually don’t last for more than a few days. In that
sense, they’re akin to hundred-year storms and at least as destructive.
When they emerge, they’re attracted by artificial structures, the more
densely situated and elaborate the better, which they set to smashing
with a vengeance. It’s hard to judge the size of things so far outside
normal scale, but good estimates put most kaiju at over 300 feet (90 m)
in height.

Kaiju rely primarily on their strength and mass, but many have some
additional trick or ability that sets them apart from their kin, which
usually translates into even more devastation.

The other quality all kaiju share is the talent of hiding after a
rampage by diving into a nearby sea or burrowing deep into the earth.
Sometimes the same kaiju will appear again days, months, years, or
decades later, attacking the same location or someplace entirely new.

Motive:
    Destruction

Environment:
    Usually near communities containing many high structures

Health:
    140

Damage Inflicted:
    18 points

Armor:
    5

Movement:
    Short

Modifications:
    Speed defense as level 8 due to size

Combat:
    A kaiju can punch, kick, or deliver a tail or tentacle lash at
    something within long range. Damage is inflicted on the target and
    everything within short range of the target, and even those that succeed
    on a Speed defense roll take 7 points of damage.

    Kaiju heal quickly, usually at a rate of 2 points per round.

    Kaiju are rare and devastating enough that most are dubbed with a unique
    identifier by survivors. The entry for each creature below notes only
    where it varies from the base creature described above.

    Rampagion:
        This kaiju has been estimated to be almost 1,000 feet (300
        m) high. Once per day, it can make a charging trample attack, dealing
        its damage in a line 300 feet (90 m) wide and 2 miles (3 km) long.
        Rampagion has 10 Armor and deals 20 points of damage with a physical
        attack (or 8 points if a victim makes a successful Speed defense roll).

    Suneko:
        This kaiju’s body, which resembles a cross between a lion and
        a lizard, is so hot that its skin glows like red coals, its mane like
        the sun’s corona, and its eyes like beaming searchlights. Suneko
        automatically deals 10 points of damage to everyone within immediate
        range. The creature can emit twin rays of plasma from its eyes in a
        focused beam that can reach as far as the horizon, which from Suneko’s
        height above the ground is about 22 miles (35 km). When it makes its
        eyebeam attack, it stops emitting killing heat in immediate range for
        about one minute.

Interaction:
    Most PCs can’t directly interact with a kaiju unless they
    have some special device or association allowing them to get the
    attention of one of the massive creatures. Doing so could give the
    characters a chance to trick or lure the beast, or maybe even persuade
    one kaiju to fight another.

Use:
    After seeing the devastation caused by a kaiju, the PCs might
    decide (or be asked) to find a way to stop a projected future appearance
    by the same creature.

GM intrusion:
    The character gains the direct attention of the kaiju. If the kaiju
    attacks the character, They are awarded 5 XP, only 1 of which they
    have to give to a friend.

Killer Clown 5 (15)
-------------------

A clown—whether it’s a doll or what seems to be a person wearing clown
makeup—could be entirely benign. But if you see one sitting alone in a
dark room, lying under your bed, or gazing up at you through the sewer
grate in the street, it might be a killer clown. Killer clowns might be
evil spirits possessing someone or an insane person living out a
homicidal fantasy. Either way, they’re as dangerous as anything you’ll
ever likely meet. If you see a clown, run. Because it might be a killer.

Motive:
    Homicide

Environment:
    Almost anywhere

Health:
    25

Damage Inflicted:
    5 points

Movement:
    Short

Modifications:
    Detecting falsehoods, deception, and persuasion as level
    7

Combat:
    A killer clown attempts to deceive its victim into believing
    that the clown is a friend. In fact, the clown is setting up an ambush
    where the victim can be strangled to death in private. When a killer
    clown successfully attacks, it inflicts 5 points of damage and locks its
    hands around the victim’s neck. In each round that the victim does not
    escape, it suffers 5 points of damage from being strangled.

    Some killer clowns know tricks that border on the supernatural. Such a
    clown may do one of the following as its action during combat.

    == ====================================================================
    d6 Clown Trick
    1  Reveal a secret that one character is keeping from
       one or more of their allies.
    2  Poke target in the eyes as a level 6 attack, blinding
       target for one minute.
    3  Activate a trapdoor beneath victim that drops them 20 feet (6 m)
       into a cellar or basement.
    4  Disappear into secret door or hatch and reappear somewhere
       hidden within short range.
    5  Jab target in the throat as a level 6 attack; resulting coughing fit
       causes target to lose next action.
    6  Down an elixir or energy drink that heals the killer clown of all
       damage sustained.
    == ====================================================================

Interaction:
    A killer clown is all jokes, magic tricks, and juggling,
    until it decides it’s time to strike.

Use:
    The creepy circus that just pulled into town is guarded by a killer
    clown, as late-night investigators soon learn.

Loot:
    A killer clown might have one or two cyphers in the form of a joy
    buzzer, cards, and cheap trinkets.

GM intrusion:
    The clown snatches a weapon, cypher, or other object from
    the character’s hand as a level 6 attack, and if successful, immediately
    uses it on the character.

Killing White Light 5 (15)
--------------------------

A killing white light isn’t a subtle hunter. At a distance, the creature
is an eye-watering point of brilliance. When it closes in, it is nothing
less than blinding, though its emanation isn’t warm. Despite the blazing
intensity, a killing white light is as cold as starlight on a December
night, sapping heat and life from living things caught in its radiance.

By day, a killing white light is usually inactive. During this period,
the creature hibernates in darkened areas, as if unwilling or unable to
compete against the sun.

Motive:
    Eliminate organic life

Environment:
    Almost anywhere dark

Health:
    15

Damage Inflicted:
    5 points

Armor:
    1

Movement:
    Short when flying

Combat:
    An active (glowing) killing white light can attack one target
    within immediate range each round with a pulse of its brilliant nimbus.
    A character who fails a Speed defense roll against the attack takes
    damage and experiences a cooling numbness. A victim killed by the
    creature is rendered into so much blowing ash, though their clothing and
    equipment are unharmed.

    As it attacks, a killing white light emits a blinding nimbus of
    illumination that affects all creatures within short range. Targets in
    the area must succeed on a Might defense roll each round or be blinded
    for one round. A character in the area can avert their eyes when
    fighting a killing white light to avoid being blinded, but attacks and
    defenses are hindered for those who do so.

    A killing white light is vulnerable to strong sources of light other
    than its own. If exposed to daylight or caught in a high-intensity beam
    of light (such as a spotlight), the killing white light falters and
    takes no action for one round, after which it can act normally. However,
    if the competing light persists for more than three or four rounds, the
    creature usually retreats to a darkened place of safety.

Interaction:
    A killing white light is too alien for interaction and may
    not be intelligent in a way humans can understand.

Use:
    An inactive killing white light (which looks something like an
    albino lump of volcanic glass) is sometimes mistaken for a cypher whose
    properties can’t quite be identified—until the creature becomes active,
    at which point its true nature is revealed.

GM intrusion:
    Normally resistant to interaction, a killing white light
    uses its blazing nimbus to burn an alien glyph of uncertain meaning in
    the character’s flesh before the creature fades like a light bulb
    switched off.

Mechanical Soldier 4 (12)
-------------------------

Clockwork automatons powered by steam, these mechanical men patrol about
and guard locations of importance to their makers. Lanky and awkward in
their movements, these quasi-humanoid automatons stand almost 8 feet (2
m) tall. In their three-fingered hands, they wield a variety of weapons.

A few people have wondered if a gear-driven soldier could ever truly
attain sentience. Most scoff at the suggestion, but is that a gleam in
the glass lens of its eye?

Motive:
    Incomprehensible

Environment:
    Anywhere, usually in groups of three to eight

Health:
    15

Damage Inflicted:
    4 points

Armor:
    3

Movement:
    Short

Modifications:
    Perception as level 5; leaps, runs, and balances as level
    3

Combat:
    Mechanical soldiers attack in groups using well-organized
    tactics. Although they can speak, they transmit information to one
    another silently and instantly within a 100-mile (160 km) range via
    wireless radio transmissions.

    Soldiers armed with advanced weaponry typically carry rifle-like guns
    that can fire multiple rapid shots without reloading. The soldiers fire
    at up to three targets (all next to one another) at once. For each
    target after the first, defense rolls are eased.

    In addition, one in four soldiers carries a back-mounted device that
    hurls bombs at long range with deadly accuracy. They explode in
    immediate range for
    4 points of damage. Each device holds 1d6
    such bombs.

    A mechanical soldier that has lost its original weaponry scavenges
    whatever is available.

    Certain frequencies of sound confuse these clockwork soldiers, hindering
    all their actions by two steps, and other frequencies prevent them from
    acting at all for 1d6 + 1 rounds.

Interaction:
    On their own, mechanical
    soldiers act on prior orders. Otherwise, they listen to and obey their
    creator—and only their creator.

Use:
    An enterprising bandit has captured and repurposed a number of
    mechanical soldiers, probably using sound. These soldiers remember
    nothing of their former duties and work for their new master as
    high-tech brigands and pirates. The bandit has no idea how to repair
    them if they are damaged, much less make new soldiers.

Loot:
    A determined scientist might scavenge the body of one of these
    automatons to find a cypher.

GM intrusion:
    The destroyed soldier explodes in a gout of flame, black
    smoke, and steam, inflicting 6 points of damage to all within immediate
    range.

Mi-Go 5 (15)
------------

These extraterrestrial creatures are known as the Fungi from Yuggoth
or the Abominable Ones. They are a bizarre amalgam of insect and
fungal entity, with many limbs and wings that can carry them aloft.
They sometimes enslave humans to work for them in strange factories,
mines, or other
labor-intensive capacities.

Motive:
    Knowledge and power

Environment:
    Usually cold or temperate hills or mountains

Health:
    19

Damage Inflicted:
    5 points

Armor:
    1

Movement:
    Short; long when flying

Modifications:
    All knowledge tasks as level 6

Combat:
    Mi-go defend themselves with pincers and claws but are more
    likely to use technological devices as weapons. Assume that a mi-go has
    one of the following abilities from a device:

    Project a blast of electricity at long range that inflicts 6 points of
    damage

    Emit poison gas in a cloud that fills to short range and inflicts 4
    points of Intellect damage if the victim fails a Might defense roll (the
    mi-go is immune)

    Project a holographic image of itself to one side that hinders attacks
    aimed at the real mi-go by two steps

    Project a sonic field that provides +2 to Armor

    Mi-go have access to other devices as well, including translators,
    cylinders that can preserve a human’s brain without its body,
    sophisticated tools, collars that control the actions of their wearers,
    and weird vehicles. Mi-go suffer no damage from cold and do not need to
    breathe.

Interaction:
    Although very few mi-go speak human languages, peaceful
    interaction with these creatures is not impossible. It’s just very
    difficult (level 7), as they see most humans as little more than
    animals.

Use:
    The characters are attacked by mi-go intent on capturing and
    enslaving them. If caught, the PCs are sent to scavenge through
    primordial ruins for disturbing technological relics.

Loot:
    Mi-go always have 1d6 cyphers as well as many curious objects that
    have no obvious human function.

GM intrusion:
    Fungal spores from the mi-go’s body overcome the
    character, who must succeed at a Might defense roll or lose their next
    turn. The character faces this risk each round they are within immediate
    distance of the creature.

Mokuren 3 (9)
-------------

Mokuren are usually no larger than a cat, but they possess the ability
to swell until they’re the size of a bus (if only briefly). That
ability, combined with their flashy pyrokinetic tails, make these
creatures a particular favorite with children, at least in stories and
picture books. Given that mokuren can “burrow” into paintings and other
two-dimensional art, it’s possible that some mokuren images are more
than simple representations.

Motive:
    Play

Environment:
    Almost anywhere, usually as static images on walls or in
    storybooks

Health:
    9

Damage Inflicted:
    3 points, unless enlarged; see Combat

Movement:
    Short; long if flying

Modifications:
    Defends as level 5 due to size, unless enlarged; see
    Combat

Combat:
    A mokuren exists in three states:
    as an image, as a cat-sized
    creature, and as a bus-sized behemoth.

    As an image, a mokuren can’t be harmed. Even if the image is defaced,
    the mokuren merely “burrows” away and reappears like graffiti on a new
    flat space within a few miles.

    Alternatively, it could emerge from the image and become a physical
    cat-sized creature as a move. In this form, a mokuren can attack with
    its claws or bite. It can also direct a stream of fire from its glowing
    tail at a target within long range. (When a mokuren flies, it’s by using
    its tail to create a jet that rockets it skyward.)

    Finally, it can make an enlarged attack, in which it swells to the size
    of a bus and swipes at, bites, or lands on a target as part of the same
    action. When enlarged, the mokuren gains +5 to Armor and makes and
    defends against all attacks as a level 7 creature. On a hit, the
    enlarged mokuren deals 7 points of damage. However, a mokuren can remain
    enlarged for a total of only four rounds during any twenty-four-hour
    period, so it uses this ability sparingly or only when enraged.

Interaction:
    To see an active mokuren is considered good luck, unless
    you manage to get on the wrong side of one. Then an offering of sweets
    must be made to the offended creature. A mokuren can’t talk, but it can
    understand the languages where it lives about as well as a trained
    courser or hound can.

Use:
    A mokuren can lead characters into unexplored areas, helping them
    find places they may have overlooked or skipped. It can also lead PCs
    into danger, but it usually does so only to bring aid (the characters)
    to someone else in trouble.

GM intrusion:
    The character hit by the mokuren doesn’t take damage.
    Instead, they must succeed on a Might defense roll or be pulled into the
    nearest wall, floor, or book with the creature, becoming a
    two-dimensional image. In this state, the victim is in stasis until the
    mokuren pulls them free, another creature “pries” them loose, or a day
    passes and the effect ends naturally.

Nuppeppo 2 (6)
--------------

Nuppeppos are animated lumps of human flesh that walk on vaguely defined
limbs. They smell of decay and death. They’re spotted in graveyards,
battlefields, coroner’s offices, and other places where the dead are
kept or interred. When witnessed in other places, nuppeppos seem to
wander streets aimlessly, sometimes alone, sometimes in groups, and
sometimes following a living person who’d rather be left alone.

Information about these creatures is scarce. They might be the
unintended consequence of a reanimation attempt, one that’s able to
catalyze its animation in similarly dead tissue to form more nuppeppos.
On the other hand, they could be particularly gruesome spirits of the
dead.

A nuppeppo sometimes follows a living individual around like a silent,
smelly pet that shows no affection. No one knows why.

(*If a nuppeppo begins to follow a character, interaction tasks by that
character and their allies are hindered. Most other creatures are put
off by a lump of animate human flesh hanging around nearby.*)

Motive:
    Wander, graze on dead flesh

Environment:
    Near places of death at night, alone or in groups of up to
    eight

Health:
    12

Damage Inflicted:
    4 points

Armor:
    1

Movement:
    Short

Combat:
    A nuppeppo can smash a foe with one of its lumpy limbs. If a
    nuppeppo is touched or struck in melee, the attacker’s weapon (or hand)
    becomes stuck to the nuppeppo and can be pulled free only with a
    difficulty 5 Might roll.

    A victim of a nuppeppo’s attack (or someone who touches a nuppeppo)
    begins to decay at a rate of 1 point of Speed damage (ignores Armor) per
    round, starting in the round following contact. To stop the spread of
    the decay, the victim can cut off the layer of affected flesh, which
    deals 4 points of damage (ignores Armor).

Interaction:
    If approached, a nuppeppo turns to “face” its interlocutor,
    but it doesn’t respond to questions or orders. However, it may begin to
    follow its interlocutor from that point forward unless physically
    prevented—at which point the nuppeppo becomes violent.

Use:
    The PCs open a grave, a coffin, or a sealed research lab, and
    several nuppeppos spill out. Unless stopped, the creatures attempt to
    “adopt” their discoverers.

GM intrusion:
    The character who allowed the nuppeppo to follow them
    around like a pet (or who has been unable to prevent it) wakes to find
    that the creature has settled upon them in the night and is using its
    touch-decay abilities to feed. In fact, the character might already be
    incapacitated by the time they wake.

Ogre 4 (12)
-----------

A bestial brute, the ogre is a sadistic, 8-foot (2 m) tall,
cannibalistic fiend that preys upon other creatures in the woods,
mountains, or other wilderness areas. This often pits them against
sylvan beings like elves and fey. Ogres dwelling in more civilized lands
are also the enemy of humans, but these ogres usually come no closer to
civilization than its very fringes.

Ogres typically dress in ragged, piecemeal clothing or nothing at all.

Motive:
    Hungers for flesh, sadistic

Environment:
    Anywhere, usually alone or (rarely) in a band of three or
    four

Health:
    20

Damage Inflicted:
    8 points

Armor:
    1

Movement:
    Short

Modifications:
    Feats of raw strength as level 6; Intellect defense and
    seeing through deception as level 3; Speed defense as level 3 due to
    size

Combat:
    Ogres usually use clubs or large, two-handed weapons with great
    power. Since they are accustomed to fighting smaller creatures, they are
    adept at using their size and strength to their advantage. If an ogre
    strikes a foe smaller than itself, either the victim is knocked back up
    to 5 feet (1.5 m), or it is dazed, which hinders its next action.

    Ogres can also swing their huge weapons in wide arcs, attacking all foes
    within close range. Defending against this attack is hindered and the
    attack inflicts 5 points of damage.

    Ogres rarely flee from a fight, and only a foe of overwhelming power can
    force them to surrender.

Interaction:
    Ogres are stupid and cruel. They speak whatever language is
    most common in the area in which they live, but their vocabulary is
    extremely limited. They don’t like conversation, even with their own
    kind. Reasoning with them is difficult at best, but sometimes they can
    be fooled.

Use:
    A solitary ogre is an excellent encounter for a group of first-tier
    characters. A number of ogres, particularly well-equipped and
    well-trained warriors, make excellent troops or guards in the service of
    a powerful master. Evil wizards and warlords like to enslave ogres and
    place them at the forefront of their armies. In these cases, the ogres
    are typically bribed, ensorcelled, or intimidated by great force.

Loot:
    Some ogres hoard gold or other valuables in their lairs, but they
    rarely have use for magic or cyphers.

GM intrusion:
    The ogre’s mighty blow (whether it strikes a foe or not)
    hits the ground or the wall, causing major structural damage and a
    possible collapse, cave-in, or landslide. It might also expose a hidden
    underground cave or chamber.

Orc 2 (6)
---------

Born into squalor and fear, the orc species is composed of miserable,
misbegotten humanoids that seem destined to serve as fodder for more
powerful evil overlords. When left to their own devices, these loathsome
creatures turn on each other, the strongest oppressing the next weakest
(and so on down the line) with cruel barbs, gruesome jokes, and physical
beatings. When these creatures have no masters to hate, they hate
themselves.

No two orcs look exactly alike, but all have a mean, ugly, and shambolic
facade. Never clean and often spattered with the remains of recent
meals, orcs have a mouthful of sharp, broken teeth that can develop into
true fangs. Adults range in height from no larger than a human child to
massive specimens larger than a strapping man. Whether big or small,
nearly all orcs have stooped backs and crooked legs. The hue of their
skin is hard to ascertain, because they are covered by the sediment of
years, not to mention the iron armor every orc constantly wears from the
moment it’s able to lift a weapon.

Motive:
    Make others more miserable than itself

Environment:
    Anywhere near, on, or under mountains, usually in groups of
    four to six, or in tribes dozens to hundreds strong

Health:
    7

Damage Inflicted:
    4 points

Armor:
    2

Movement:
    Short

Modifications:
    Speed defense as level 3 when carrying a shield; pleasant
    interactions as level 1

Combat:
    Most orcs have bows able to target foes within long range. Some
    carry a shield and wield a medium axe, sword, or mace that inflicts 4
    points of damage. Other orcs (usually those that are larger than their
    fellows) dispense with shields and wield heavy two-handed mauls and
    hammers that inflict 6 points of damage.

    Orcs live short, brutish lives. The few that survive for years do so
    because of some special advantage; they’re sneakier, stronger, tougher,
    or meaner than average. These have the following modifications,
    respectively:

    * Stealth tasks as level 5
    * Deal 2 additional points of damage with melee weapons
    * +10 health
    * Tasks related to trickery and deceit as level 5

Interaction:
    An orc would stab its own mother if it thought doing so
    would give it another hour of life in a desperate situation. That said,
    most orcs have been conditioned, through beatings and torture, to fear
    the evil master they serve (if any). Characters attempting to negotiate
    with an orc through intimidation find that short-term success is
    followed by medium-term betrayal.

Use:
    A band of orcs fires on the PCs from the edge of the forest.
    However, these orcs are crafty, and characters who rush directly into
    combat might fall victim to a hidden pit trap or other prepared ambush.

Loot:
    Orcs carry a lot of garbage. Amid this dross, a band of orcs might
    have currency equivalent to a moderately priced item among them.

GM intrusion:
    With a scream of savage glee, five more orcs rush to join
    the fight.

Prince(ss) of Summer 5 (15)
---------------------------

Fey nobility are as numberless as cottonwood seeds on the June breeze.
But that doesn’t mean each isn’t unique, with a quirky personality and a
specific role to play in the mysterious Court of Summer. Demonstrating
life, vigor, predation, growth, and competition, the princesses and
princes of summer are beings of warmth and generosity, usually. But
catch them during the change of the season, and they can be deadly
adversaries just as easily. Fey nobles dress in costly diaphanous and
flowing garments, and often wear some sign of their noble lineage, such
as a circlet or diadem.

Motive:
    Unpredictable; defend fey territory and prerogatives

Environment:
    Almost any wilderness region alone or commanding a small
    group of lesser faerie creatures

Health:
    22

Damage Inflicted:
    5 points

Armor:
    2

Movement:
    Short; short when gliding on the wind

Modifications:
    Tasks related to deception, disguise, courtly manners,
    and positive interactions as level 7

Combat:
    Most fey princesses and princes are armed with an elegant sword
    and possibly a bow carved of silverwood. Also, each knows one or more
    faerie spells. Faerie spells include the following.

    Brilliant Smile:
        Target must succeed on an Intellect defense task or
        do the fey creature’s will for up to one minute.

    Golden Mead:
        Allies who drink from the fey’s flask gain an asset to
        all defense tasks for ten hours.

    Night’s Reward:
        Target suffers 5 points of Intellect damage (ignores
        Armor) and must make an Intellect defense roll or fall asleep for up to
        one minute.

    Summer Confidence:
        Selected targets in short range have an asset on
        tasks related to resisting fear and acting boldly.

    Thorns:
        Target suffers 5 points of Speed damage (ignores Armor) and
        must succeed on a Might defense task or lose their next turn entangled
        in rapidly grown thorny vines.

    Princes and princesses of summer regain 2 points of health per round
    while their health is above 0 unless they’ve been damaged with a
    silvered or cold iron weapon.

Interaction:
    Most fey are willing to talk, and those of the Summer Court
    are especially eager to make deals. However, people who bargain with fey
    nobles should take care to avoid being tricked.

Use:
    The characters find a fey noble wounded and in need of aid.

Loot:
    In addition to fine clothing, fine equipment, and a considerable
    sum of currency, a prince or princess of summer might carry a few
    cyphers and even a faerie artifact.

GM intrusion:
    The character is blinded for up to one minute by a shaft
    of brilliant sunlight unless they succeed on a Might defense task.

Puppet Tree 6 (18)
------------------

A puppet tree is a 25-foot (8 m) tall, spiky, orange and blue tree
surrounded by a large area of red reeds that tremble and wave enticingly
even when no wind is present. Humanoid figures are often gathered around
it, but these rotted, overgrown corpses are the tree’s victims, dead but
serving as fleshy puppets to the tree’s will.

Victims drained of knowledge and life are used as lures to draw in yet
more victims, at least until the bodies rot away. When not used as
lures, the corpse puppets are sent to scout nearby areas.

(Corpse puppet: level 2; struck targets must also succeed on a Might
defense task or be grabbed until they can escape; all tasks attempted by
the grabbed target are hindered; free-roaming puppets remain animate for
one day)

Motive:
    Hungers for fresh bodies

Environment:
    On hilltops, isolated from other plant life

Health:
    33

Damage Inflicted:
    8 points

Armor:
    3

Movement:
    None

Modifications:
    Speed defense as level 5 due to size and immobility;
    deception and disguise (puppeteering corpses to act in a lifelike
    manner) as level 7

Combat:
    Some of the red reeds surrounding a puppet tree end in a hard,
    sharp crystal spike. When a living creature comes within short
    range of the tree, the reeds rise behind the target and try to skewer
    them through the head or neck with the spike. If a target is killed by
    these attacks, the puppet tree controls the body as a corpse puppet,
    using it to enact its plans. Over time these humanoids rot and are
    overgrown by the biology of the plant, losing utility for the tree.
    Most trees have about five corpse puppets active, which can be
    simultaneously animated to attack foes.

    A puppet tree is vulnerable to fire. All fire attacks against the tree
    inflict 2 additional points of damage and ignore Armor. The puppet tree
    will always attempt to stop a fire, or target the source of flame during
    combat.

    A corpse puppet can be detached and sent roaming; however, it retains
    only about a day’s worth of animation, after which it collapses and
    molders like a normal corpse. Sometimes, however, a sapling puppet tree
    blooms from the remains.

Interaction:
    Puppet trees are highly intelligent, but malevolent. Even
    if communication can be opened via telepathy or some other means, the
    tree will always attempt to double-cross the PCs.

Use:
    The PCs spy a group of “people” having a picnic under a
    strange-looking tree in the middle of nowhere.

Loot:
    Possessions of former victims can be found in the red reeds,
    usually including a moderate amount of currency and various bits of
    gear. Devices of victims (if any) are collected by the corpse puppets
    and cobbled together into a strange machine, its purpose inexplicable.

GM intrusion:
    Two corpse puppets, unseen in the red reeds, rise and
    seize a character in an attempt to hold them still for a crystal spike
    attack. The character must make a difficulty 4 Speed or Might task to
    shake free.

Ravage Bear 4 (12)
------------------

A ravage bear is a hideous predator that hunts entirely by sense of
smell. It is blind and nearly deaf, but it still tracks and senses prey
easily. It is very protective of its young, and if hungry, it is
extremely dangerous. Otherwise, it gives most creatures a wide berth.

Motive:
    Hungers for flesh

Environment:
    Alone or in pairs (usually with a few cubs) in wooded,
    rocky, or mountainous areas, typically in cold or temperate climes

Health:
    20

Damage Inflicted:
    7 points

Armor:
    1

Movement:
    Long

Modifications:
    Makes Might defense rolls as level 6; runs, climbs, and
    jumps as level 7

Combat:
    A ravage bear grabs foes with its powerful arms, holds them
    fast, and then squeezes and tears at them until they are dead. It can
    hold only one creature at a time. While a ravage bear is holding a
    creature, it can attack only the held creature. In each round that a
    held creature does not escape, it suffers 4 points of damage in addition
    to damage from attacks made against it.

    A ravage bear can move very quickly in short sprints. In combat, it can
    go into an insane fury and will fight to the death. If it takes 10 or
    more points of damage, its defenses are hindered, but its attacks are
    eased.

    Ravage bears are immune to visual effects, such as illusions. However,
    olfactory effects can confuse and “blind” them temporarily.

Interaction:
    Ravage bears are animals and act like animals.

Use:
    Ravage bears are likely chance encounters in the wilderness for
    unlucky travelers.

GM intrusion:
    In its rage, the ravage bear makes an extra attack that
    does 2 additional points of damage.

Replicant 5 (15)
----------------

Virtually identical to adult humans, these biosculpted androids are
stronger, faster, and potentially smarter. However, because they are
manufactured beings with grafted memories, replicants rarely feel true
human emotion, be that love, sadness, or empathy, though those who live
long enough to lay down their own memories can develop the capacity to
do so.

However, few replicants gain the opportunity because they are created
for a purpose, which could be to serve as police or guards, as soldiers
in a distant war, or as impostors shaped to blend in with people so they
can explore on behalf of an alien intelligence or a bootstrapped AI. In
most of these cases, these purposes lead to a relatively short span of
existence, which usually ends when the replicant chooses to detonate
itself rather than be captured.

Motive:
    Go unnoticed; stamp out (or replace) any who learn of their
    existence

Environment:
    Anywhere

Health:
    18

Damage Inflicted:
    6 points

Movement:
    Short

Modifications:
    Tasks related to pleasant social interaction,
    understanding human social norms, and deception as level 2

Combat:
    Replicants blend in and prefer not to enter combat. Since
    destruction is not usually their principal goal, they avoid
    confrontation. If, however, something threatens their mission, they
    defend themselves to the best of their ability. Replicants might use
    weaponry but are adept in using their limbs to batter foes into
    submission.

    A replicant poses the greatest danger when its physical form begins to
    fail through violence or natural degradation (many seem to have a
    natural “life” span of just a few years). When reduced to 0 points of
    health, the replicant explodes, inflicting 10 points of damage to
    everything in long range.

Interaction:
    Replicants are designed to look human and, at least during
    a casual interaction, pass as human. But extended conversation trips up
    a replicant more often than not. Eventually, a replicant gets something
    wrong and says inappropriate things or exhibits strange mannerisms.

Use:
    A contact of one of the characters is secretly a replicant. It has
    survived longer than expected, and its connection to whatever created it
    has weakened enough that it has gained some independence and made strong
    emotional connections to the PC. It knows its time is running out and
    may turn to the character for help.

GM intrusion:
    The character struck by the replicant is smashed into the
    wall so hard that the surrounding structure begins to collapse on them.

Shadow Elf 4 (12)
-----------------

Elves who faded from the surface to escape the justice of their fey
cousins for crimes uncounted are sometimes called shadow elves, dark
elves, or simply trow. It’s widely assumed that shadow elves fled to new
realms deep below the ground, and indeed, the routes that lead to their
true abodes are mostly subterranean and include many grand underground
keeps. However, the heart of the shadow elf kingdom lies in the
colorless dimension of Shadow itself, where all things exist as a dim
reflection of the real world.

Sometimes shadow elves appear on the surface, spilling from dark tunnels
or, in some cases, from the shadows themselves. They raid for plunder,
fresh slaves, and sacrifices. The sacrifices are made to their godqueen,
a monstrously sized black widow spider that schemes in darkness.

When a shadow elf returns to the world of light, it can choose to appear
as a silhouette only: a slender humanoid outline lurking as if at the
nadir of a well.

Motive:
    Tortures for pleasure, serve the shadow elf godqueen

Environment:
    Almost anywhere dimly lit, singly or in groups of up to
    four

Health:
    15

Damage Inflicted:
    5 points

Armor:
    1

Movement:
    Short

Modifications:
    Stealth and perception as level 6; Speed defense as level
    6 due to shadowy nature

Combat:
    Shadow elves attack with short blades, knives, and crossbow
    quarrels of steel-hard shadow. They can see in dim light and absolute
    darkness as if it were daylight.

    Some shadow elves can cast spells, including the following. Each spell
    requires an action to cast.

    +----+----------------------------------------------------------------+
    | d6 | Shadow Elf Spell                                               |
    +====+================================================================+
    | 1  | Enchant weapon to inflict 3 additional points of damage (8     |
    |    | total)                                                         |
    +----+----------------------------------------------------------------+
    | 2  | Enchant weapon to inflict 1 additional point of Speed damage   |
    |    | (poison, ignores Armor), plus 2 points of Speed damage each    |
    |    | additional round until victim succeeds on a Might defense roll |
    +----+----------------------------------------------------------------+
    | 3  | Fly a long range each round for ten minutes                    |
    +----+----------------------------------------------------------------+
    | 4  | Gain +2 to Armor (total of 3 Armor) for ten minutes            |
    +----+----------------------------------------------------------------+
    | 5  | Long-range spell renders subject blind for ten minutes on      |
    |    | failed Might defense roll                                      |
    +----+----------------------------------------------------------------+
    | 6  | Long-range spell targets up to three creatures next to each    |
    |    | other; holds them motionless in a shadow web for one minute on |
    |    | failed Speed defense rolls                                     |
    +----+----------------------------------------------------------------+

    If subject to full daylight, a shadow elf loses its modifications to
    stealth, perception, and Speed defense, and is likely to retreat.

Interaction:
    Shadow elves may negotiate and even ally with other
    creatures for a time. But they do so only until the best opportunity for
    a betrayal presents itself.

Use:
    Shadow elves have overrun an outlying keep, and even in broad
    daylight, the castle is shrouded in darkness and webs of shadow. The
    treasures said to lie in the keep’s coffers may already be in the hands
    of the dark fey.

Loot:
    A shadow elf carries currency equivalent to an expensive item, in
    addition to weapons, light armor, and a cypher or two. Shadow elf
    leaders may carry an artifact.

GM intrusion:
    The shadow elf casts a spell that charms a character on a
    failed Intellect defense roll. The character fights on the side of the
    shadow elf for up to one minute, though they can make another Intellect
    defense roll each round to try to break the influence.

Skeleton 2 (6)
--------------

Skeletons are animated bones without much sense of self-preservation.
They enjoy a crucial advantage over living creatures in one important
and often exploited area: skeletons are dead shots with ranged weapons.
They have no breath, no heartbeat, and no shaking hands to contend with
as they release a shot, which means that skeletons armed with ranged
weapons are something to be feared.

Motive:
    Defense or offense

Environment:
    Nearly anywhere, in formations of four to ten

Health:
    6

Damage Inflicted:
    3 points (claw) or 5 points (ranged weapon)

Armor:
    1

Movement:
    Short

Modifications:
    Ranged attacks as level 5; Speed defense against most
    ranged attacks as level 5; resist trickery as level 1

Combat:
    Skeletons can attack with a bony claw if they have no other
    weapon, but most attack with a long-range weapon. If a skeleton can see
    any portion of its target, the target loses any benefits of cover it
    might have otherwise enjoyed.

    When in formation, a group of four or more skeletons with ranged weapons
    can focus their attacks on one target and make one attack roll as a
    single level 7 creature, dealing 7 points of damage.

    Skeletons can see in the dark.

    Reanimators:
        Some skeletons were created by a curse, and simply
        battering them into a pile of bones isn’t enough to end their existence.
        Two rounds after reanimator skeletons are “killed,” they regain full
        health in a flash of magical illumination. This regeneration can be
        prevented if the linchpin of the animating curse is separated from the
        skeleton after it falls. Such an item is usually obvious and might take
        the form of a lead spike through the skull, an ebony amulet, a dull
        sword through the ribs, a crown, and so on.

Interaction:
    A skeleton usually interacts only by attacking. Unless
    animated by a sapient spirit able to communicate via magic, skeletons
    lack the mechanisms for speech. However, they can hear and see the world
    around them just fine.

Use:
    Skeletons make ideal units in armies, especially when archery or
    artillery is required. A formation of four or more skeletons with ranged
    weapons atop a tower provides a surprisingly robust defense.

Loot:
    Sometimes the linchpin item required to create a reanimator
    skeleton is valuable.

GM intrusion:
    A skeleton destroyed by a melee attack explodes like a
    grenade. The bone shrapnel inflicts 5 points of damage to every creature
    in immediate range.

Statue, Animate 7 (21)
----------------------

Towering statues carved from stone or cast in metal are sometimes more
than humans rendered in moments of triumph, celebration, or suffering.
Sometimes a statue moves, usually in service to some ancient geas or
command that animated it in the first place.

Most animate statues are vessels imprisoning the mind of a sentient
creature. Such entrapment usually tumbles the spirits into the abyss of
insanity, though most rest in a dormant state, their minds lost in
whatever memories they retain. Disturbing animate statues can cause them
to awaken, usually with disastrous results.

Motive:
    Release from imprisonment; guard an area

Environment:
    In out-of-the-way places, especially ancient ruins

Health:
    33

Damage Inflicted:
    9 points

Armor:
    4

Movement:
    Short

Modifications:
    All tasks involving balance as level 2; Might defense as
    level 8; Speed defense as level 5 due to size

Combat:
    An animate statue towers over most foes, and it can smash or
    stomp a target within short range as a melee attack. The statue’s
    massive size and the material of its body means it can walk through
    nearly any obstacle, smashing through walls of solid rock, buildings,
    and trees. When walking, it pays no attention to what it steps on.
    Anything in its path is likely flattened. A character who is stepped on
    must make a Speed defense roll to dodge or be knocked down and take 9
    points of damage.

    Animate statues are strong and hard to hurt, but they are often
    top-heavy. If one falls or is knocked over, it takes a few rounds to
    rise and resume whatever it was doing.

Interaction:
    Statues spend years immobilized and insensate, their minds
    lost in half-remembered experiences and hallucinations. Rousing a statue
    has unpredictable results. Some might rampage. Others laugh, cry, or
    scream streams of nonsense. Regardless, if one has been commanded to
    guard an area or entrance, it also likely lashes out.

Use:
    An animate statue holds a treasure trove of knowledge. If the
    characters can keep it focused or knocked down long enough, they might
    coax from it the information they seek.

GM intrusion:
    The animate statue strikes a character so hard that the
    victim flies a long distance and lands in a heap, possibly dropping gear
    and weapons along the way.

Tyrannosaurus Rex 7 (21)
------------------------

The short arms of a tyrannosaurus have been much parodied in Earth
social media circles, but the arms aren’t really important when a
hunting tyrannosaurus is after you. It’s more the soul-shivering roar,
designed to freeze prey in place, and a skull and mouth so enormous that
the entire creature is cantilevered by a massive tail that itself can be
used as a powerful weapon.

As vicious as tyrannosauruses likely were 66 million years ago, the
versions still hunting today could be even more dangerous. That’s
because the ones with a taste for humans have learned to adapt to human
defenses and to use their roar to terrorize prey as they hunt.

Motive:
    Hungers for flesh

Environment:
    Tyrannosauruses hunt solo or in pairs; they’re drawn to
    loud, unfamiliar noises (like motor engines).

Health:
    50

Damage Inflicted:
    10 points

Movement:
    Short

Modifications:
    Perception as level 5; Speed defense as level 5 due to
    size

Combat:
    A tyrannosaurus attacks with its massive bite. Not only does it
    deal damage, but the target must also make a Might defense roll to pull
    free or be shaken like a rat in the mouth of a pit bull for 3 additional
    points of Speed damage (ignores Armor). The shaking recurs each
    subsequent round in which the target fails a Might-based task to pull
    free.

    A tyrannosaurus can also make a trampling attack if it can charge from
    just outside of short range. When it does, it moves 50 feet (15 m) in a
    round, and anything that comes within immediate range is attacked. Even
    those who make a successful Speed defense roll take 2 points of damage.

    Finally, a tyrannosaurus can roar. The first time creatures within short
    range hear the roar on any given day, they must succeed on a difficulty
    2 Intellect defense roll or stand frozen in fear for a round. Attacks
    against them are eased by two steps in the attacker’s favor and deal 2
    additional points of damage.

    For all their power, tyrannosauruses are not above self-preservation.
    They never fight to the death if they are outclassed, and they usually
    break off if they take more than 30 points of damage in a conflict.

Interaction:
    Tyrannosauruses are animals, but they’re clever hunters,
    too. When they hunt in pairs, they work to keep prey penned between
    them.

Use:
    Something is killing big game in a forest preserve. Poachers are
    suspected at first, but when they are also found dead, it’s clear that
    something else is to blame.

GM intrusion:
    The tyrannosaurus’s tail swings around and knocks the
    character tumbling out of short range and possibly into dangerous
    terrain.

Vampire 6 (18)
--------------

Vampires are undead creatures, risen from the grave to drink blood.
Their very nature and essence are evil and anti-life, even as they revel
in their own endless existence. Most vampires are vain, arrogant,
sadistic, lustful, and domineering. Their powers allow them to
manipulate others, and they frequently toy with their prey before
feeding. Vampires come out only at night, as the sun’s rays will destroy
them.

The bite of a vampire over three nights (in which it exchanges a bit of
its own blood) ensures that the victim will rise as a vampire under the
thrall of the one that killed it. While vampires are careful not to
create too many of their kind (which amount to competition), each thrall
conveys a bit more supernatural power to a vampire.

Motive:
    Thirsts for blood

Environment:
    Usually solitary, on the edges of civilization

Health:
    24

Damage Inflicted:
    7 points

Movement:
    Long

Modifications:
    Climb, stealth, and perception as level 8; Speed defense
    as level 7 due to fast movement

Combat:
    Vampires are strong and fast. They have impressive fangs, but
    these are usually used in feeding, not in battle. They typically fight
    with their fists or hands (which basically become claws) but sometimes
    use weapons.

    A vampire can change into a bat or a wolf. This transformation does not
    change its stats or abilities except that, as a bat, it can fly.
    Vampires can also transform into shadow or mist, and in these forms they
    can’t be harmed by anything (but also can’t affect the physical world).

    Vampires possess an unholy charisma and can mesmerize victims within
    immediate distance so that they stand motionless for one round. In
    subsequent rounds, the victim will not forcibly resist the vampire, and
    the vampire can suggest actions to the victim (even actions that will
    cause the victim to harm themselves or others they care about). Each
    round, the victim can attempt a new Intellect defense roll to break
    free.

    Vampires are notoriously difficult to hurt. Unless a weapon is very
    special (blessed by a saint, has specific magical enchantments against
    vampires, or the like), no physical attack harms a vampire. They simply
    don’t take the damage. Exceptions include the following:

    Fire:
        Vampires burn, though the damage doesn’t kill them. It only
        causes pain, and a vampire regains all health lost to fire damage within
        a day.

    Running water:
        Complete immersion inflicts 10 points of damage per
        round. If not destroyed, the vampire can use a single action to regain
        all health lost in this way.

    Holy water:
        This inflicts 4 points of damage and affects a vampire
        exactly like fire.

    Sunlight:
        Exposure to sunlight inflicts 10 points of damage per round.
        If not destroyed, the vampire regains all health lost to exposure within
        a day.

    Wooden stake:
        This weapon inflicts 25 points of damage, effectively
        destroying the vampire in one blow. However, if the vampire is aware and
        able to move, this attack is hindered as the vampire does everything it
        can to evade.

    Further, vampires have the following special weaknesses:

    Garlic:
        Significant amounts of garlic within immediate distance hinder
        a vampire’s tasks.

    Cross, holy symbol, or mirror:
        Presenting any of these objects
        forcefully stuns a vampire, causing it to lose its next action. While
        the object is brandished and the vampire is within immediate range, its
        tasks are hindered by two steps.

    Eventually, a vampire with a multitude under its command becomes the new
    vampire lord. The vampire lord is the most powerful vampire in the world
    and is often (but not always) the most ancient of its kind. It has many
    vampires under its control, and even those that it did not create pay it
    respect and homage.

Interaction:
    Most vampires look upon humans as cattle upon which to
    feed. They rarely have respect for anything but other vampires, and they
    often hate other supernatural creatures that they cannot enslave.

Use:
    Strange stories of shadows in the night, people disappearing from
    their beds, and graves missing their former occupants could portend the
    arrival of a vampire in the region.

GM intrusion:
    The character struck by the vampire is caught fast in its
    powerful grip. If the character doesn’t escape immediately, the vampire
    bites them automatically.

Vampire, Transitional 3 (9)
---------------------------

When humans are “visited upon” (bitten) by a vampire, they might be
killed, or they might be left alive to begin a slow transformation into
a creature of the night. If victims are bitten three times, they become
a vampire forever under the control of the one that bit them. From the
time of the first bite until their complete transformation after the
third bite, they are transitional vampires. Ways to return transitional
vampires to normal include using special ancient rituals or destroying
the vampire that bit them in the first place.

Transitional vampires usually serve as guardians, consorts, or spies for
their masters.

Motive:
    Thirsts for blood

Environment:
    Anywhere, usually solitary but sometimes in groups of two
    or three

Health:
    12

Damage Inflicted:
    4 points

Movement:
    Short

Modifications:
    Climb and stealth as level 4

Combat:
    Transitional vampires can maintain a human existence during the
    day without any of a vampire’s powers or weaknesses. However, they have
    a disdain for garlic and the sun. At night they take on all the
    characteristics of a vampire, and if confronted by any of the
    traditional vampiric weaknesses (a wooden stake, a cross, and so on),
    they flee unless their master is present.

Interaction:
    Transitional vampires are utterly devoted to their master.

Use:
    Transitional vampires lie in the intersection of foe and victim. A
    loved one or trusted companion who has been turned into a transitional
    vampire will try to betray, defeat, and kill the PCs, but the characters
    are motivated to save them rather than destroy them.

Vat Reject 3 (9)
----------------

Vat rejects come into being when clone vats meant to produce clone
soldiers or similar
mass-produced entities are corrupted. How the carefully controlled
process becomes compromised varies, but possibilities include yeast
contamination, sunspot activity, nanovirus evolution, or purposeful
meddling with control parameters. Unskilled operators experimenting
with derelict cloning equipment can also produce a vat of rejects.

Vat rejects fear nothing and welcome death, except that their
existential rage requires an outlet other than immediate suicide. Their
warped forms mean that most are in constant pain, and they somehow
understand that this was artificially stamped into them by their
creators. Revenge is their only possible redemption.

Motive:
    Self-destruction through endless aggression

Environment:
    Anywhere in lost and lonely places

Health:
    9

Damage Inflicted:
    3 points

Movement:
    Short

Modifications:
    Speed defense as level 4 due to frenzied alacrity

Combat:
    Vat rejects charge into battle with berserk speed, hindering
    defenses against their initial attack. All vat rejects are able to
    inflict damage directly by cutting, bashing, or biting a victim,
    depending on their particular morphology. Some also have additional
    abilities; roll on the table below for each reject.

    == =======================================================
    d6 Ability
    == =======================================================
    1  Reject deals +3 damage in melee (6 points total)
    2  Reject has short-range acid spit attack that inflicts 2
       points of damage, plus 2 points of damage each
       additional round until victim succeeds on a
       Might defense roll
    3  Reject can fly a long distance as an action
    4  Reject has 2 Armor
    5  Reject has long-range destructive eye ray
       attack that inflicts 6 points of damage
    6  When struck by an attack, reject
       detonates in an immediate radius,
       inflicting 6 points of damage in a
       radioactive explosion (and 1 point even
       on a successful Speed defense roll)
    == =======================================================

Interaction:
    Vat rejects are usually always enraged, making interaction
    nearly impossible. However, some may negotiate if offered a reasonable
    hope of salvation through extreme surgery or other transformation.

Use:
    A long-missing derelict ship, famous for carrying a load of
    planet-buster superweapons, is found. However, salvagers discover it to
    be overrun by vat rejects. No one knows if the rejects plan to use the
    superweapons, if they have been released by someone else as a
    distraction, or if they are part of a mutated ship defense system.

GM intrusion:
    The vat reject also has a radioactive sting. On a failed
    Might defense roll, the character struck by the reject descends one step
    on the damage track.

Wardroid 6 (18)
---------------

When star troopers need heavy support, they sometimes bring in
wardroids. These fearsome robots, standing about 8 feet (2 m) tall, are
ruthless even by trooper standards and are known to kill innocent
bystanders as often as they kill foes. It is said that when wardroids
are unleashed, wise troopers fall back and take cover.

Motive:
    Maintain control, crush, kill, destroy

Environment:
    Anywhere

Health:
    30

Damage Inflicted:
    8 points

Armor:
    3

Movement:
    Short; some models can fly a short distance each round

Modifications:
    Attacks as level 7

Combat:
    A wardroid’s main weapon is a bank of laser blasters that it can
    use to attack up to three foes standing next to each other as one
    action. When damaged, a wardroid regains 1 point of health each round.
    Furthermore, each wardroid has one additional capability:

    +----+----------------------------------------------------------------+
    | d6 | Ability                                                        |
    +====+================================================================+
    | 1  | Emit poison gas that inflicts 5 points of damage on organic    |
    |    | beings in immediate range                                      |
    +----+----------------------------------------------------------------+
    | 2  | Project grenades up to long distance that detonate in an       |
    |    | immediate radius, inflicting 5 points of damage                |
    +----+----------------------------------------------------------------+
    | 3  | Fire a beam that stuns an organic being for one                |
    |    | round, during which it cannot take actions                     |
    +----+----------------------------------------------------------------+
    | 4  | Emit a field that disrupts machines;                           |
    |    | technological devices and machine                              |
    |    | creatures in immediate range cannot                            |
    |    | function for one round                                         |
    +----+----------------------------------------------------------------+
    | 5  | Fire a piercing projectile up to long                          |
    |    | range that inflicts 6 points of damage                         |
    |    | that ignores physical armor (but not                           |
    |    | necessarily other Armor)                                       |
    +----+----------------------------------------------------------------+
    | 6  | Spray a corrosive that inflicts                                |
    |    | 5 points of damage on everything                               |
    |    | in immediate range                                             |
    +----+----------------------------------------------------------------+

Interaction:
    Interaction is difficult for those not authorized to
    communicate with a wardroid.

Use:
    Wardroids are often deployed in groups of two or three to guard a
    vault or the entrance to a spacecraft, or to track down intruders aboard
    a space station.

Loot:
    The remains of a wardroid can yield one or two cyphers to someone
    adept at salvage.

GM intrusion:
    When defeated, the wardroid detonates, inflicting 8 points
    of damage on all creatures within immediate range.

Werewolf 4 (12)
---------------

The curse of lycanthropy begins as nightmares about being chased or,
somehow more terrifying, chasing someone else. As the dreams grow more
fierce and each night’s sleep provides less rest, victims begin to
wonder about the bloodstains on their clothing, the strange claw marks
in their homes, and eventually, the mutilated bodies they find buried in
their backyards.

When not transformed, many who suffer the curse seem like completely
normal people, if emotionally traumatized by the fact that most of their
friends and family have been brutally slaughtered over the preceding
months. Some few, however, realize the truth of their condition, and
depending on their natures, they either kill themselves before their
next transformation or learn to revel in the butchery.

Motive:
    Slaughter when transformed; searching for answers when human

Environment:
    Anywhere dark, usually alone but sometimes as part of a
    small pack of two to five

Health:
    24

Damage Inflicted:
    5 points

Movement:
    Short; long when in wolf form

Modifications:
    Attacks as level 6 when half lupine; Speed defense as
    level 6 when full lupine; perception as level 7 when half or full lupine

Combat:
    In normal human form, a werewolf has no natural attacks, though
    it may use a weapon. It also lacks the abilities described below; its
    only power is to transform into a half-lupine form or full-lupine form,
    which takes 1d6 agonizing rounds. A handful of werewolves can control
    their transformation, but most change at night in response to
    moon-related cues.

    Half Lupine:
        A half-lupine werewolf is part humanoid and part wolf,
        but completely terrifying. It attacks with its claws.

    Full Lupine:
        A full-lupine werewolf is a particularly large and
        vicious-looking wolf. It normally bites foes and deals 2 additional
        points of damage (7 points total) but can also use its claws.

    Half and Full Lupine:
        Half-lupine and full-lupine werewolves both
        enjoy enhanced senses and regain 2 points of health per round. However,
        a werewolf that takes damage from a silver weapon or bullet stops
        regenerating for several minutes.

Interaction:
    In human form, werewolves have the goals and aspirations of
    normal people, and they often don’t recall what they did while
    transformed or even realize that they suffer the curse of lycanthropy.
    In half- or full-lupine form, there’s no negotiating with one.

Use:
    When the moon is full, werewolves hunt

GM intrusion:
    A PC who moves down one step on the damage track due to
    damage inflicted by a werewolf must succeed on a Might defense roll or
    be afflicted with the curse of lycanthropy.

Witch 5 (15)
------------

They studied the old ways at the dark of the moon. They heard the
shuffle of unnamed things through the darkling forest, watched the
convection of the bubbles rise in the cauldron, and attended to the
mumbled instructions of withered crones and crumbling messages traced on
dead leaves. Then one midnight, everything came together. Another witch
was born.

When witches lose sight of their humanity and use their powers for
personal gain without regard for others, they are warped by the power
they channel, both mentally and physically. However, they can hide such
transformations beneath layers of illusion.

Motive:
    Domination of others, knowledge

Environment:
    Almost anywhere, usually alone, but sometimes as part of a
    coven of three to seven witches

Health:
    21

Damage Inflicted:
    5 points

Movement:
    Short; long when flying (on a broomstick)

Modifications:
    Deception and disguise as level 7; Speed defense as level
    6 due to familiar; knowledge of forests and dark secrets as level 6

Combat:
    When attacked, a witch relies on the aid of their familiar to
    improve their Speed defense. The familiar could be a large black cat, an
    owl, a big snake, or some other creature. Killing a witch’s familiar is
    so shocking to a witch that their attacks and Speed defense are hindered
    for a few days. It’s also a way to ensure that the witch never forgives
    their foe or grants mercy.

    (Familiar: level 3; health 9; Armor 1)

    A witch can use their ritual blade to attack a creature in immediate
    range, but would much rather use curses, including the ones described
    below. A witch can’t use the same curse more than once every other
    round.

    Charm:
        Victims within short range who fail an Intellect defense roll
        are enslaved. Victims turn on their allies or take some other action
        described by their new master. The curse lasts for one minute, or until
        the victims succeed on an Intellect defense roll; each time they fail a
        roll, the next roll is hindered by one additional step.

    Hexbolt:
        A victim within long range is attacked with fire, cold, or
        psychic
        bolts, as the witch chooses. Psychic bolts deal 3 points of Intellect
        damage (ignores Armor).

    Shrivel:
        A victim within long range and up to two creatures next to
        the victim must succeed on a Might defense roll or take 3 points of
        Speed damage (ignores Armor). In each subsequent round, a victim who
        failed the previous roll must make another Might defense roll with the
        same outcome on failure.

    Vitality:
        The witch regains 11 points of health and gains +3 to Armor
        for one minute. Multiple uses don’t further improve Armor.

Interaction:
    Most witches are deceptive and conniving,
    though a few work against the stereotype. All witches are willing to
    negotiate, though the devious ones usually do
    so in bad faith.

Use:
    The PCs need an old book to continue their investigation. Word is
    that the old woman who lives on the edge of the woods has the only copy.

Loot:
    A witch usually has an artifact or two on their person, possibly
    including a flying broom (which has a depletion roll of 1 in 1d10).

GM intrusion:
    After a character succeeds on a defense roll against one
    of the witch’s ongoing curse effects, the witch immediately tosses a
    hexbolt at them. If the character is hit, the ongoing curse effect also
    continues.

Xenoparasite 6 (18)
-------------------

This alien creature exists only to eat and reproduce. In doing so, it
also destroys every form of life it encounters. Xenoparasites are not
technological but were likely engineered by a species with advanced
biological super-science. Xenoparasites don’t travel between star
systems on their own; they were presumably spread across an area of
space by their creators to serve as a broad-spectrum bioweapon. What has
become of the original maker species is unknown, but given the fecundity
and ferocity of the xenoparasite, it’s likely they were consumed by
their own creation.

Xenoparasites use ovipositors to lay thousands of microscopic eggs in
victims. The implanted eggs, like tiny biological labs, detect the
particular biology of the new host, adapt accordingly, and use it to
fertilize themselves. Within a day or two, victims who haven’t already
been consumed by adult xenoparasites (which are human sized) give
explosive birth to multiple vicious juveniles (which are the size of
cats). These juvenile xenoparasites have an edge in dealing with the
particular species of creature they hatched from.

Motive:
    Eat and reproduce

Environment:
    Hunts alone or in small groups

Health:
    28

Damage Inflicted:
    6 points

Armor:
    2

Movement:
    Short; long when flying

Modifications:
    All stealth actions as level 8

Combat:
    A xenoparasite bites with its mandibles and stings one victim
    with its ovipositor as a single action. The bite inflicts 6 points of
    damage, and the ovipositor inflicts 3 points of damage and injects
    thousands of microscopic eggs if the victim fails a Might defense roll.

Once every other round, an adult can fly at least a short distance to
build terrifying velocity and then make a flying attack with its
mandibles, dealing 12 points of damage. Defenses against this attack are
hindered.

An egg host requires the attention of someone skilled in medicine (and
a successful difficulty 7
Intellect-based roll) to sterilize all the eggs in the victim’s blood
before they hatch twenty or more hours after being deposited, which
kills the host and releases 1d6 juvenile xenoparasites. Juveniles are
level 2 creatures, but they attack the species of the host they were
hatched from as if level 4. After just a few days of feeding, they
grow to full adult size.

Xenoparasites can survive at crushing ocean and gas giant pressures, as
well as in the vacuum of space. They can encrust abandoned spacecraft
and desolate moons for millennia in extended hibernation, only to become
active again when vibrations alert them to potential new food sources.

Interaction:
    These creatures are built to consume,
    not negotiate.

Use:
    Xenoparasites are tough aliens. A colony of them would be a
    challenge even for PCs normally accustomed to stiff opposition. A single
    xenoparasite introduced into an inhabited area could turn the entire
    place into an infested hive within a week.

GM (group) intrusion:
    An NPC shrieks, bursts, and births 1d6 juvenile
    xenoparasites.

Zombie 3 (9)
------------

Humans transformed into aggressive, hard-to-kill serial killers with no
memory of their former existence are called zombies. Depending on a
zombie’s origin, the reason for its transformation varies. A zombie
might arise from an undead curse, a psychic possession, an AI meatware
overwrite, a viral infection, a drug overdose, or something else.
Regardless of how the transformation happened, the result is much the
same: a creature whose humanity has been burned out and replaced with
unquenchable hunger.

Zombies aren’t intelligent, but enough of them together sometimes
exhibit emergent behavior, just as ants can coordinate activities across
a colony. Thus, zombies alone or in small groups aren’t an overwhelming
threat for someone who has a baseball bat or can get away. But it’s
never wise to laugh off a zombie horde.

Motive:
    Hunger (for flesh, cerebrospinal fluid, certain human hormones,
    and so on)

Environment:
    Almost anywhere, in groups of five to seven, or in hordes
    of tens to hundreds

Health:
    12

Damage Inflicted:
    3 points

Movement:
    Immediate

Modifications:
    Speed defense as level 2

Combat:
    Zombies never turn away from a conflict.
    They fight on, no matter the odds, usually attacking by biting, but
    sometimes by tearing with hands made into claws by the erosion of skin
    over their finger bones.

    When zombies attack in groups of five to seven individuals, they can
    make a single attack roll against one target as one level 5 creature,
    inflicting 5 points of damage.

    Zombies are hard to finish off. If an attack would reduce a zombie’s
    health to 0, it does so only if the number rolled in the attack was an
    even number; otherwise, the zombie is reduced to 1 point of health
    instead. This might result in a dismembered, gruesomely damaged zombie
    that is still moving. Zombies can see in the dark
    at short range.

    “Fresh” zombies are vulnerable to electricity. The first time a zombie
    takes 5 or more points of damage from an electrical attack, it falls
    limp and unmoving. Assuming nothing interferes with the process, the
    zombie arises minutes or hours later without the vulnerability.

    Some zombies are infectious. Their bites spread a
    level 8 disease that moves a victim down one step on the damage track
    each day a Might defense roll is failed. Victims killed by the disease
    later animate as zombies.

Interaction:
    Zombies groan when they see something that looks tasty.
    They do not reason, cannot speak, and never stop pursuing something
    they’ve identified as a potential meal, unless something else edible
    comes closer.

Use:
    The characters are asked to clear out a space that once served as
    an old military depot. The appearance of zombies sealed in the area
    comes as an unpleasant surprise.

GM intrusion:
    When the character fails to kill a zombie by rolling an
    odd number on an attack that otherwise would have been successful, in
    addition to the normal effect, the zombie’s arm comes free and animates
    as a separate level 2 zombie.
