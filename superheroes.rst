Superheroes
###########

Like horror, the superhero genre is really a subset of the modern genre
with extensive special considerations. In many ways, it might appear
that the Cypher System is a strange fit for superheroes. But if you
think about it, with foci like Bears a Halo of Fire and Wears a Sheen of
Ice, the Cypher System makes all genres a little bit “superhero-ish.”
Character sentences might look like the following:

* Firebrand is a :ref:`descriptor:Brash` energy projector (:ref:`type:Adept`) who :ref:`focus:Bears a Halo of Fire`.
* King Brick is a :ref:`descriptor:Tough` :ref:`type:Warrior` who :ref:`focus:Performs Feats of Strength`.
* Dimensionar is a :ref:`descriptor:Mystical` warlock (:ref:`type:Adept`) who :ref:`focus:Exists Partially Out of Phase`.
* Dark Ronin is a :ref:`descriptor:Mysterious` crimefighter (:ref:`type:Explorer`) who :ref:`focus:Solves Mysteries`.
* Speedburst is a :ref:`descriptor:Fast` crimefighter (:ref:`type:Explorer`) who :ref:`focus:Moves Like the Wind`.

And so on.

Suggested Types for a Superhero Game
====================================

==================== =========================================
Role                 Type
==================== =========================================
Strong hero          Warrior
Brawler hero         Warrior with stealth flavor
Gadget hero          Explorer with technology flavor
Pilot                Explorer with technology flavor
Charmer              Speaker
Leader               Speaker with combat flavor
Shadowy vigilante    Explorer with stealth flavor
Scientist hero       Explorer with skills and knowledge flavor
Energy-wielding hero Adept with combat flavor
Wizard               Adept
Mentalist            Adept
Psychic ninja        Warrior with magic flavor
==================== =========================================

Basic Creatures and NPCs for a Superhero Game
=============================================

Dog, guard:
    level 3, attacks and perception as level 4

Genetically enhanced bruiser:
    level 3, attacks as level 4; health 15; 5
    points of melee damage

Ninja:
    level 3, stealth as level 6

Robot minion:
    level 4; Armor 2

Bystander:
    level 2

Scientist:
    level 2, science-related tasks as level 4

Worker:
    level 2; health 8

Supervillains
=============

People with amazing abilities who use them for evil earn the label of
supervillain. This section presents five sample supervillains. These
supervillains use the same format as the :doc:`creatures` chapter.

Anathema 7 (21)
---------------

The supervillain called Anathema is big, bright red, and stronger than
anyone on this planet or any other (or so he claims). Superheroes who go
head to head with him learn that he can withstand almost any hit and
always gives back twice as hard as he receives. He can bring down
buildings with a punch and throw semi trucks across state lines.

Before he was Anathema, he was Sameer Stokes, a bitter and spiteful
coder working for a large software company. Having failed in
relationships, promotions, and retaining friends, Sameer retreated
online and learned that he had power when he bullied people. He
delighted in causing emotional distress in others in forums and social
media. In effect, he was a troll. When the metamorphosis happened, he
was turned into a troll for real. (Sameer doesn’t recall the
metamorphosis or the days before and immediately after his change,
despite using therapy and drugs in an attempt to recover those
memories.)

(Assume that Anathema has three power shifts in strength and two in
resilience. These shifts are already figured into his modifications and
other stats.)

Motive:
    Accumulate wealth, live on the edge

Environment:
    Anywhere vast wealth can be stolen

Health:
    70

Damage Inflicted:
    12 points

Movement:
    Short; a few miles (5 km) per leap

Modifications:
    Strength tasks as level 10; Might defense as level 9;
    Speed defense as level 5 due to size

Combat:
    Anathema hits foes with bone-shocking force. He can throw cars
    and large objects at targets within long range, dealing damage to all
    creatures within immediate range of his target.

    Anathema has a healing factor that makes it hard to hurt him in any
    meaningful sense. He regains 10 points of health per round. In any round
    in which he regains health, his attacks deal 3 additional points of
    damage (15 total), and he seems to visibly swell with muscle.

Interaction:
    When Anathema is riled up during a fight, it’s difficult to
    reason with him. However, he is willing to negotiate if someone offers
    him wealth or convinces him they have valuable secrets for breaking
    mental blocks. Anathema doesn’t know how he became the way he is, and he
    wants to recover his missing memories.

Use:
    The rolling earthquake afflicting the city is actually Anathema
    fighting a group of newbie superheroes who haven’t figured out that
    engaging the red mountain will likely cause more deaths than leaving him
    alone. (The first rule of fighting Anathema is to lead or move him
    somewhere with a low population density.)

Loot:
    Anathema doesn’t normally carry wealth or other valuables. In his
    lair, Anathema typically has three to five expensive items, 1d6 cyphers,
    and possibly an artifact.

GM intrusion:
    *Anathema’s attack sends the character flying a long
    distance and potentially into dangerous terrain.*

Doctor Dread 7 (21)
-------------------

Doctor Dread is larger than life thanks to her brilliant mind, her media
savvy, and the robotic armor she uses to enhance her otherwise normal
abilities. Indeed, Doctor Dread has become the most feared terrorist on
the planet. She uses her abilities to extort money, influence, and
technology from the rich and powerful, whether her victims are
individuals, governments, corporations, or superheroes.

Alicia Coleridge is Doctor Dread’s secret identity. Born into relative
obscurity, she received a full scholarship to the Russell Institute of
Technology, where she studied the effects of radioactive substances on
living tissue. In a freak lab accident, Alicia’s fiancé was slain, and
Alicia was disfigured and driven slightly insane, so much so that she
built the Doctor Dread armor. She plows the vast wealth she accumulates
through terrorism into research into the rejuvenation of dead flesh. She
hopes to one day bring back her dead love, whose body she keeps in
suspended animation.

(Doctor Dread is usually accompanied by a handful of robot minions.)

(Dread’s robot minion: level 3; Armor 1; long-range laser attack
inflicts 4 points of damage)

(Assume that Doctor Dread has three power shifts in intelligence and two
in resilience. These shifts are already figured into her modifications
and other stats.)

Motive:
    Accumulate wealth; reanimate dead flesh

Environment:
    Wherever money can be extorted

Health:
    40

Damage Inflicted:
    7 points

Armor:
    4

Movement:
    Short; long when flying

Modifications:
    Resists mental attacks and deception as level 8;
    understands, repairs, and crafts advanced technology as level 10

Combat:
    Doctor Dread’s armor allows her to exist without outside air (or
    air pressure), food, or water for up to ten days at a time. She can call
    on her robotic armor to accomplish a variety of tasks, including the
    following:

    Barricade:
        Establish an immobile, two-dimensional field
        of transparent force 10 feet by 10 feet (3 m by 3 m) for ten minutes

    Energy Cloak:
        Create an energy field that gives her +5 to Armor
        against heat, cold, or magnetism (one at a time, chosen when she uses
        the power) for ten minutes

    Fade:
        Become invisible for one minute, or until she makes an attack

    Plasma Blast:
        Long-range heat and electricity blast that inflicts 7
        points of damage

Interaction:
    Doctor Dread is slightly mad, but that’s normally disguised
    by her amazing brilliance. She is an egomaniac but will negotiate in
    return for a promise of wealth or biomedical lore she doesn’t already
    know.

Use:
    The PCs are called to handle a hostage situation at a party in
    which many of the city’s wealthy elite are being held captive by Doctor
    Dread. She promises to let them go once sufficient wealth is paid into
    her offshore accounts.

Loot:
    Most of Doctor Dread’s considerable wealth is tied up in online
    accounts, two or three secret fortresses, and cutting-edge biological
    research equipment.

GM intrusion:
    Doctor Dread uses a function built into her robotic armor
    that is the perfect solution for her current predicament:
    healing
    herself, teleporting away, disintegrating a barrier, or whatever is
    needed.

Magnetar 8 (24)
---------------

Not much is known about Magnetar other than its powerful ability to
generate and control magnetic fields. Various research groups theorize
that Magnetar is an alien, a sentient and self-improving robot, or even
some kind of manifestation of a fundamental force. Given Magnetar’s
vaguely humanoid shape, a few people even suggest that the villain is
actually a man with a mutant ability so powerful that it burned out all
memories of his former self.

In truth, Magnetar is the animate, sentient, and self-regulating nucleus
of a neutron star that is able to rein in its immense electromagnetic
signature. One of two such beings an advanced alien species created from
a single magnetar (a type of neutron star with an extremely powerful
magnetic field), Magnetar was sent on a mission of exploration. After
millennia, it crashed on Earth and was damaged. Having lost most of its
memory data, Magnetar knows that something was taken from it (its twin),
but it can’t remember what. It has decided to blame the humans.

(Assume that Magnetar has three power shifts in its magnetic power and
two in resilience. These shifts are already figured into its
modifications and other stats.)

Motive:
    Revenge; regain memory

Environment:
    Almost anywhere, searching for what it has lost

Health:
    50

Damage Inflicted:
    12 points

Armor:
    8

Movement:
    Short; long when magnetically levitating

Modifications:
    Speed defense as level 5 due to mass; tasks related to
    controlling and shaping metal through electromagnetic manipulation as
    level 11

Combat:
    Magnetar’s fist packs a wallop, since it can selectively add
    mass to the punch. However, its most potent ability is its level 11
    control over all metal within very long range, which it uses to create
    anything it can imagine, including walls, attacks, pincers, and more.
    Magnetar can lift bridges, vehicles, and structures infused with rebar
    that it can see within its area of influence. When it throws such a
    large object as part of an attack, the target and everything within
    short range of the target takes 10 points of damage.

    Magnetar’s only weakness is psychic attacks, which is fortunate since
    reducing it to 0 health through an
    old-fashioned beating could release an uncontrolled neutron star chunk
    on the Earth’s surface.

Interaction:
    Morose and gruff, Magnetar would rather be alone, but every
    so often, it goes on a rampage, hoping that a display will draw out
    whoever or whatever made it the way it is. Magnetar constantly feels the
    drag of emotional loss, but it doesn’t know why (it doesn’t realize that
    the feeling comes from the loss of its twin).

Use:
    Doctor Dread has put a bounty on Magnetar’s head because she wants
    to study the advanced technology woven through its body. The bounty
    amount is outrageous, but then again, so is Magnetar.

GM intrusion:
    On a failed Might defense roll, all of the character’s
    loose metallic items (including weapons) are stripped from them and
    become stuck to a nearby metallic buttress.

Mister Genocide 5 (15)
----------------------

Real name Alfred Webster, Mister Genocide has the unfortunate ability to
synthesize deadly poison from his skin. His touch can kill, but if he
wishes it, so can his spittle or even his breath.

Anyone who spends too much time in Mister Genocide’s presence becomes
ill, even if the villain isn’t actively using his power. Thus, his
cronies usually wear gas masks and protective clothing. Mister Genocide
has promoted himself to the head of the mob in the city where he resides
and is always looking to expand his operations, sometimes at the expense
of other criminals.

When victims are killed by Mister Genocide’s poison, their skin and the
whites of their eyes take on a bright green hue, which increases the
terror that normal people feel regarding him. Even superheroes have been
brought down by his toxins.

Mister Genocide sometimes teams up with Anathema, because the red
mountain is the only villain who can withstand the poison that Genocide
constantly emits.

(Assume that Mister Genocide has two power shifts in his poison power,
one in intelligence, and two in resilience. These shifts are already
figured into his modifications and other stats.)

Motive:
    Accumulate power

Environment:
    Anywhere crime lords congregate

Health:
    15

Damage Inflicted:
    5 points; see Combat

Armor:
    1

Movement:
    Short

Modifications:
    Poison breath attack and Might defense as level 7;
    Intellect defense and evil genius as level 6

Combat:
    Targets touched by Mister Genocide must make a difficulty 7
    Might defense roll or take 5 points of Speed damage (ignores Armor) from
    the poison transmitted. Worse, the poison continues to inflict 2 points
    of Speed damage each round until the victim succeeds at a Might defense
    roll.

    Every other round, Mister Genocide can make a level 7 poison attack
    that can affect up to ten victims within short range as a single
    action. Those who fail a Might defense roll take 7 points of Speed
    damage (ignores Armor) and spend a round helpless as they cough and
    gag. The inhalant poison does not continue to inflict damage
    each round.

    Mister Genocide is immune to most venoms, toxins, and poisons.

Interaction:
    Certifiably insane, Mister Genocide likes to kill people.
    He may negotiate for a while, but if there is not enough gain to be had,
    he might kill everyone with a breath just for the fun of watching them
    suffocate and turn green.

Use:
    Gang warfare between two criminal organizations is shooting up
    downtown, and many innocent bystanders caught in the crossfire end up
    bullet-ridden or poisoned (with green skin). Someone needs to put a stop
    to Mister Genocide.

Loot:
    The supervillain carries currency equivalent to 1d6 expensive
    items, a cypher or two, and a variety of poisoned knives, needles, and
    vials.

GM intrusion:
    A character affected by the poison must make a second
    Might defense roll or fall unconscious from shock. Unconsciousness lasts
    for up to a minute, or until the victim is jostled awake.

Wrath 6 (18)
------------

The head of an elite group of assassins, Wrath wants to save the world
by killing everyone who impedes her vision of perfection—which turns out
to be the better part of humanity. In addition to being one of the most
accomplished martial artists to walk the earth (thanks to her connection
with a mystical entity called the Demon), Wrath is also a criminal
mastermind whose assassins are just one layer of the organization she
controls.

Born more than two hundred and fifty years ago in China to a name lost
to history, Wrath was taken in by a monastery and trained in the ways of
fist and sword. Everything changed when raiders attacked and killed
everyone in her monastery, leaving her the sole survivor. Vowing revenge
against the raiders and the world that allowed animals like them to
exist, she acquired a magical amulet that contains the Demon. The Demon
in turn bequeathed her extraordinary speed, strength, and longevity.

Wrath is content to let her assassins (and mobsters, lawyers, and
politicians) accomplish many of her goals, though she relishes being
present when particularly important adversaries are brought down.

(Assassin of Wrath: level 4, stealth as level 7)

(Assume that Wrath has two power shifts in dexterity, two in accuracy,
and one in resilience. These shifts are already figured into her
modifications and other stats.)

Motive:
    Save the world

Environment:
    Anywhere wrongs (to Wrath’s way of thinking) must be righted

Health:
    36

Damage Inflicted:
    8 points

Armor:
    1

Movement:
    Short

Modifications:
    Stealth, attacks, and Speed defense as level 8

Combat:
    Wrath prefers a sword, though she is equally adept with a
    crossbow or, in rare cases, modern weapons. In melee she can attack two
    foes as a single action every round.

    Thanks to the influence of the Demon, Wrath regains 3 points of health
    each round, even if reduced to 0 health. The only way to permanently
    kill her is to reduce her to 0 health and keep her that way long enough
    to burn away the tattoo of the Demon that is engraved across her back.

Interaction:
    Wrath is arrogant and confident, though not so much that
    she is easily fooled by flattery. She is usually amenable to
    negotiating, because she can anticipate the agenda of others and usually
    gain far more for herself in the end. However, she is not one to betray
    her word.

Use:
    Wrath is making a bid to form a group of supervillains—all of whom
    will answer to her, of course—and it seems that initial talks are going
    well. The only holdout is Mister Genocide, who feels threatened by
    Wrath’s larger organization, and this tension has led to ongoing warfare
    in the streets as assassins battle mobsters.

Loot:
    In addition to weapons and armor, Wrath likely possesses the
    equivalent of five exorbitant items, 1d6 cyphers, and possibly one or
    two artifacts.

GM intrusion:
    Just as things seem bleakest for her, Wrath summons a
    group of assassins waiting in the wings to surround the PCs and demand
    their surrender.

Additional Superhero Equipment
==============================

Suggested additional equipment is the same as in a modern setting. Keep
in mind, however, that for many heroes, “equipment” can be superfluous.
Where do you stash the flashlight and rope when all you’re wearing is
spandex tights?

Optional Rule: Power Shifts
===========================

Superheroes can do things that other people cannot. They throw cars,
blast through brick walls, leap onto speeding trains, and cobble
together interdimensional gateways in a few hours. It’s tempting to say
that such characters are stronger, faster, or smarter, so they should
have higher Might, Speed, or Intellect Pools. However, simply bumping up
stat Pools or Edge doesn’t fully represent this dramatic increase in
power. Instead, consider using an optional rule called power shifts.

Under this rule, all superhero characters get five power shifts. Power
shifts are like permanent levels of Effort that are always active. They
don’t count toward a character’s maximum Effort use (nor do they count
as skills or assets). They simply ease tasks that fall into specific
categories, which include (but are not necessarily limited to) the
following.

Accuracy:
    All attack rolls

Dexterity:
    Movement, acrobatics, initiative, and Speed defense

Healing:
    One extra recovery roll per shift (each one action, all coming
    before other normal recovery rolls)

Intelligence:
    Intellect defense rolls and all knowledge, science, and
    crafting tasks

Power:
    Use of a specific power, including damage (3 additional points
    per shift) but not attack rolls

Resilience:
    Might defense rolls and Armor (+1 per shift)

Single Attack:
    Attack rolls and damage (3 additional points per shift)

Strength:
    All tasks involving strength, including jumping and dealing
    damage in melee or thrown attacks (3 additional points of damage per
    shift) but not attack rolls

Each shift eases the task (except for shifts that affect damage or
Armor, as specified in the list above). Applying 2 shifts eases the task
by two steps, and applying 3 shifts eases the task by three steps.

A character assigns their five power shifts as desired, but most
characters should not be allowed to assign more than three to any one
category. Once the shifts are assigned, they should not change.

For example, a superstrong character might put three of their shifts
into strength and the other two into resilience. Whenever they lift
something heavy, smash through a wall, or throw an object, they ease the
task by three steps before applying Effort, skill, or assets. Thus, all
difficulties from 0 to 3 are routine for them. They smash through level
3 doors as if they don’t exist. As another example, a masked vigilante
character with a utility belt full of gadgets and great acrobatic skills
might put two shifts in dexterity, one in accuracy, one in intelligence,
and one in healing. They’re not actually superpowered, just tough and
well trained.

Some GMs will want to allow PCs to increase their power shifts. Having a
character spend 10 XP to do so would probably be appropriate. Other GMs
will want to run superhero games with PCs of greater or lesser power
(cosmic-level heroes or street-level heroes, perhaps). In such cases,
more or fewer power shifts should be granted to the PCs at the game’s
start.

Superpowered NPCs and Power Shifts
----------------------------------

NPC superheroes and villains get power shifts, too. Most of the time,
this adds to their level. For example, Blast Star is a level 5 fiery
villain who has three power shifts. When she blasts through a level 7
iron security door, she does so easily because in this circumstance,
she’s actually level 8.

Sometimes, NPC power shifts make things harder for the PCs. For example,
Fleetfoot the level 4 speedster puts all three of her shifts in
dexterity. When she runs past a character who tries to grab her, the
difficulty to do so is increased by three steps to 7.

Typical NPC supers get three power shifts. Exceptional ones usually have
five.

Really Impossible Tasks
-----------------------

In superhero games, due to conventions of the genre, difficulty caps at
15 instead of 10. Difficulty 10 is labeled “impossible,” but that label
is for regular folks. For superpowered characters, “impossible” means
something different, thanks to power shifts.

Think of each difficulty above 10 as being one more step beyond
impossible. Although a GM in another genre would say there’s no chance
that a character could leap 100 feet (30 m) from one rooftop to another,
in a superhero game, that might just be difficulty 11. Picking up a city
bus isn’t something normal characters could do, but for a strong
superhero, it might be difficulty 12.

In theory, NPCs in such a game can go up to level 15 as well. Levels
above 10 represent opponents that only a superhero would consider taking
on: a robot that’s 1,000 feet (300 m) tall (level 11); Galashal, Empress
of Twelve Dimensions (level 14); or a space monster the size of the moon
(level 15).

Superhero Artifacts
===================

Supervillains build doomsday devices. Ancient artifacts present a threat
to all humanity if in the wrong hands. Weird machines from alien
dimensions offer solutions to unsolvable problems. Artifacts are an
important part of superhero stories. A few examples are below.

Doctor Dread’s Time Portal
--------------------------

Level:
    9

Form:
    Arch of metal big enough to walk through

Effect:
    Anyone who steps through it goes to a predetermined point in the
    past or future (a minimum of fifty years in either direction), which can
    be anywhere on the planet.

Depletion:
    1 in 1d20

Serum X
-------

Level:
    1d6 + 2

Form:
    Vial or syringe of red fluid

Effect:
    Strips someone of all superpowers (including abilities granted
    by magic, psionics,
    mutation, or science) for twenty-four hours. The target retains only
    skills and abilities that are mundane, as agreed by the GM and player.

Depletion:
    Automatic

Stellarex Crystal
-----------------

Level:
    1d6 + 4

Form:
    Multifaceted purple stone the size of a fist

Effect:
    Created in the dawning of the universe, this artifact grants
    the wielder the ability to not only fully restore all their stat
    Pools, but also increase each Pool temporarily by 10 points. These
    extra points fade after
    twenty-four hours if not used.

Depletion:
    1–3 in 1d10
