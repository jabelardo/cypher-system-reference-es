from html.parser import HTMLParser
from xml.etree import ElementTree as ET
from typing import Optional, Union, Iterable
from unittest import TestCase


class HTMLElementParser(HTMLParser):
    def __init__(self, builder: ET.TreeBuilder) -> None:
        super().__init__()
        self.builder = builder

    def handle_data(self, data: str) -> None:
        self.builder.data(data)

    def handle_endtag(self, tag: str) -> None:
        self.builder.end(tag)

    def handle_starttag(
        self, tag: str, attrs: list[tuple[str, Optional[str]]]
    ) -> None:
        # TreeBuilder.start() expects the keys and values of the attrs dict
        # to both be `Union[str, bytes]`. Here we build a dict whose keys and
        # values are always `str`, but that doesn't satisfy MyPy's type checking
        # for reasons I don't quite understand:
        #
        #     https://github.com/python/mypy/issues/8477
        #
        # Instead, we declare the type of `attrs_dict` to be
        # what TreeBuilder.start() expects,
        # and this way MyPy accepts it.
        attrs_dict: dict[Union[str, bytes], Union[str, bytes]] = {
            key: (value if value is not None else key) for (key, value) in attrs
        }
        self.builder.start(tag, attrs_dict)


def simplify_html(elem: ET.Element) -> None:
    for each in elem.findall(".//table"):
        # We don't care to hard-code column widths
        for colgroup in each.findall("./colgroup"):
            each.remove(colgroup)

    # We don't care to hard-code docutils classes.
    for each in elem.findall(".//table[@class]"):
        del each.attrib["class"]
    for each in elem.findall(".//th[@class]"):
        del each.attrib["class"]
    for each in elem.findall(".//tr[@class]"):
        del each.attrib["class"]

    # Don't include the admonition title in an admonition block
    # since that can be added by CSS (or by Sphinx, as it turns out)
    for each in elem.findall(".//div"):
        for title in each.findall("./p[@class='admonition-title']"):
            each.remove(title)


def parse(html: str) -> ET.Element:
    builder = ET.TreeBuilder()
    parser = HTMLElementParser(builder)
    parser.feed(html)
    parser.close()
    res = builder.close()
    simplify_html(res)
    return res


def unparse(elems: Iterable[ET.Element]) -> str:
    return "\n".join(
        ET.tostring(each, encoding="unicode", method="html") for each in elems
    )


class ParsingTests(TestCase):
    def assertElementsEqual(
        self,
        actual: ET.Element,
        expected: ET.Element,
        msg: Optional[str] = None,
    ) -> None:
        actual_text = ET.tostring(actual)
        expected_text = ET.tostring(expected)
        self.assertEqual(actual_text, expected_text, msg)

    def test_one_element(self) -> None:
        actual = parse("<html></html>")
        expected = ET.XML("<html></html>")

        self.assertElementsEqual(actual, expected)

    def test_attributes(self) -> None:
        actual = parse("<a href='http://example.com' download>link</a>")
        expected = ET.XML(
            "<a href='http://example.com' download='download'>link</a>"
        )

        self.assertElementsEqual(actual, expected)
