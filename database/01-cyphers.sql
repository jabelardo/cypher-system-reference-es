-- Defines templates that can be used to create cyphers.
CREATE TABLE cypher_templates
    -- The source where this template was defined
    ( source_name TEXT NOT NULL
        REFERENCES sources(source_name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    -- The name of this template
    , name TEXT NOT NULL
    -- The number of dice to roll when picking a level.
    -- The X in "XdY + Z", it must be a positive number.
    , level_dice_count INTEGER NOT NULL CHECK (level_dice_count >= 0)
    -- The size of the dice to roll when picking a level.
    -- The Y in "XdY + Z"
    , level_dice_size INTEGER NOT NULL
    -- The bonus to add when picking a level.
    -- The Z in "XdY + Z"
    , level_bonus INTEGER NOT NULL
    -- A physical decription of the cypher.
    -- The Cypher SRD doesn't list specific forms for cyphers,
    -- but many settings do.
    , form TEXT NOT NULL DEFAULT ''
    -- The effect of activating this cypher.
    -- This field contains arbitrary HTML.
    , effect TEXT NOT NULL
    -- Any addition comments or suggestions about the cypher.
    -- This field contains arbitrary HTML.
    , notes TEXT NOT NULL
    , PRIMARY KEY (source_name, name)
    , CHECK (
        CASE
            -- If we are rolling zero dice...
            WHEN level_dice_count = 0
            -- ...then it doesn't make sense to store a die size.
            THEN level_dice_size = 0
            -- Otherwise, the size must be at least a d2.
            ELSE level_dice_size >= 2
        END
      )
    ) STRICT;

-- Defines the various tags a cypher can have.
CREATE TABLE cypher_tag_names
    -- The source where this tag was defined
    ( source_name TEXT NOT NULL
        REFERENCES sources(source_name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    -- The name of this tag.
    , name TEXT NOT NULL
    , PRIMARY KEY (source_name, name)
    ) STRICT;

-- Defines which cypher templates have which tags
CREATE TABLE cypher_tags
    -- A reference to a row of the cypher_templates table.
    ( template_source_name TEXT NOT NULL
    , template_name TEXT NOT NULL
    -- A reference to a row of the cypher_tag_names table.
    , tag_source_name TEXT NOT NULL
    , tag_name TEXT NOT NULL
    , FOREIGN KEY (template_source_name, template_name)
        REFERENCES cypher_templates(source_name, name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    , FOREIGN KEY (tag_source_name, tag_name)
        REFERENCES cypher_tag_names(source_name, name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    , UNIQUE
        ( template_source_name
        , template_name
        , tag_source_name
        , tag_name
        )
    ) STRICT;
