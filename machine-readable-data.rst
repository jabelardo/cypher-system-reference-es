Machine Readable Data
#####################

Tabletop RPGs are traditionally played around a table,
taking notes with pencil and paper,
but in the 21st century players expect a certain amount of digital assistance.
That requires the rules of the system be encoded
in a way that computers can understand and process.
Converting human-readable data into machine-readable data
is tedious and error-prone,
so here we present automatically converted data
to save anyone else the trouble.

The complete database can be downloaded in SQLite3 format:
:download:`cypher-system.sqlite <database/cypher-system.sqlite>`
(requires SQLite 3.37.0 or higher),
or individual tables can be downloaded as CSV files
in each section below.

Schema diagram
==============

The machine-readable version of the SRD data
conforms to a standard schema,
depicted below.
In each table,
the columns marked with 🔑 are the primary key fields for that table.

Each group of tables will be described below.

.. sqlviz::

Sources
=======

Although not strictly part of the SRD data,
the schema supports collecting items from different sources
and the dependencies between them.
For example,
a GM might want to run a campaign that includes
items from the core rules,
the "Fantasy" and "Modern" genres,
and items of their own invention.
If they want to look up the details of a particular item,
it helps to know where it came from.

.. literalinclude:: database/00-sources.sql
   :language: sql

You can download the contents of these tables in CSV format:

* :download:`sources <database/sources.csv>`
* :download:`source_dependencies <database/source_dependencies.csv>`

Cypher Templates
================

The information from the :doc:`cyphers` chapter
is stored in these tables.
They're "templates" because they're patterns for generating cyphers,
not individual cyphers.
For example, the :ref:`cyphers:Adhesion` cypher template
has a level of "1d6",
but an individual Adhesion cypher would have a specific, fixed level,
like 2 or 5.

.. literalinclude:: database/01-cyphers.sql
   :language: sql

You can download the contents of these tables in CSV format:

* :download:`cypher_templates <database/cypher_templates.csv>`
* :download:`cypher_tag_names <database/cypher_tag_names.csv>`
* :download:`cypher_tags <database/cypher_tags.csv>`

Artifact Templates
==================

This table stores information from the "Artifacts" sections
in each of the :doc:`genre chapters <genres>`.
Like `Cypher Templates`_ above,
these are "templates" because an actual artifact will have
a specific level, instead of a range of possible levels.

.. literalinclude:: database/02-artifacts.sql
   :language: sql

You can download the contents of this table in CSV format:

* :download:`artifact_templates <database/artifact_templates.csv>`

Lookup Tables
=============

There's a lot of things in the Cypher System
that are put into particular categories
with particular properties.
It would be reasonable for character sheet and virtual tabletop apps
to just assume that anything labelled "Light Armor"
would automatically provide +1 Armor
and impose a +2 cost to Speed Effort levels,
but it's possible that different settings
will want to tweak those values,
so we provide them as data.

.. literalinclude:: database/03-lookup-tables.sql
   :language: sql

You can download the contents of these tables in CSV format:

* :download:`armor_categories <database/armor_categories.csv>`
* :download:`price_categories <database/price_categories.csv>`
* :download:`range_categories <database/range_categories.csv>`
* :download:`weapon_categories <database/weapon_categories.csv>`
* :download:`pools <database/pools.csv>`
* :download:`skill_training <database/skill_training.csv>`
