Fantasy
#######

For our purposes, fantasy is any genre that has magic, or something so
inexplicable it might as well be magic. The sort of core default of this
type is Tolkienesque fantasy, also known as second-world fantasy because
it includes a completely new world not our own. Big fantasy epics like
those penned by J. R. R. Tolkien (hence the name), C. S. Lewis, George
R. R. Martin, Stephen R. Donaldson, David Eddings, Ursula K. Le Guin,
and others are indicative of this genre. It usually involves swords,
sorcery, nonhuman species (such as elves, dwarves, helborn, and
half-giants), and epic struggles.

Of course, fantasy might also involve the modern world, with creatures
of myth and sorcerers dwelling among us. It might involve mythic
traditions of any number of cultures (elves, dwarves, and the like,
usually being decidedly European) or bear little resemblance to anything
on Earth, past or present. It might even involve some of the trappings
of science fiction, with spaceships and laser guns amid the wizardry and
swords (this is often called science fantasy).

Fantasy can also be defined by the amount of fantasy elements within it.
A second-world fantasy filled with wizards, ghosts, dragons, curses, and
gods is referred to as high fantasy. Fantasy with a firmer grounding in
reality as we know it in our world is low fantasy. (In fact, low fantasy
often takes place in our world, or in our world’s distant past, like the
stories of Conan.) No single element indicates concretely that a given
fantasy is high or low. It’s the prevalence of those elements.

The point is, there are many, many types of fantasy.

Suggested Types for a Fantasy Game
==================================

============ ==============================
Role         Character Type
============ ==============================
Warrior      Warrior
Knight       Warrior
Ranger       Explorer
Barbarian    Explorer flavored with combat
Thief        Explorer flavored with stealth
Wizard       Adept
Cleric       Speaker flavored with magic
Druid        Explorer flavored with magic
Warrior mage Warrior flavored with magic
Bard         Speaker
============ ==============================

Basic Creatures and NPCs for a Fantasy Game
===========================================

Bat:
    level 1

Dog:
    level 2, perception as level 3

Dog, guard:
    level 3, attacks and perception as level 4

Hawk:
    level 2; flies a long distance each round

Horse:
    level 3; moves a long distance each round

Rat:
    level 1

Viper:
    level 2; bite inflicts 3 points of Speed damage (ignores Armor)

Warhorse:
    level 4; moves a long distance each round

Wolf:
    level 3, perception as level 4

Blacksmith:
    level 2, metalworking as level 4; health 8

Farmer:
    level 2, animal handling as level 3; health 8

Merchant:
    level 2, haggling and assessment tasks as level 3

Villager:
    level 2

Additional Fantasy Equipment
============================

In the default Medieval Europe-style fantasy setting, the following
items (and anything else appropriate to that time period) are usually
available.

Inexpensive Items
-----------------

.. table::
   :width: 100%
   :widths: 40 60

   ====================== ==============================
   Weapons                Notes
   ====================== ==============================
   Arrows (12)
   Crossbow bolts (12)
   Knife (rusty and worn) Light weapon (won’t last long)
   Wooden club            Light weapon
   ====================== ==============================

.. table::
   :width: 100%
   :widths: 40 60

   ==================== =====
   Other Items          Notes
   ==================== =====
   Burlap sack
   Candle
   Iron rations (1 day)
   Torch (3)
   ==================== =====

Moderately Priced Items
-----------------------

.. table::
   :width: 100%
   :widths: 40 60

   =================== ===============================
   Weapons             Notes
   =================== ===============================
   Blowgun             Light weapon, immediate range
   Dagger              Light weapon
   Handaxe             Light weapon
   Sword (substandard) Medium weapon (won’t last long)
   Throwing knife      Light weapon, short range
   =================== ===============================

.. table::
   :width: 100%
   :widths: 40 60

   ============== ===========
   Armor          Notes
   ============== ===========
   Hides and furs Light armor
   Leather jerkin Light armor
   ============== ===========

.. table::
   :width: 100%
   :widths: 40 60

   ================= =============
   Other Items       Notes
   ================= =============
   Backpack
   Bedroll
   Crowbar
   Hourglass
   Lantern
   Rope              Hemp, 50 feet
   Signal horn
   Spikes and hammer 10 spikes
   Tent
   ================= =============

Expensive Items
---------------

.. table::
   :width: 100%
   :widths: 40 60

   ============== ================================
   Weapons        Notes
   ============== ================================
   Battleaxe      Medium weapon
   Bow            Medium weapon, long range
   Cutlass        Medium weapon
   Light crossbow Medium weapon, long range
   Quarterstaff   Medium weapon (requires 2 hands)
   Sword          Medium weapon
   ============== ================================

.. table::
   :width: 100%
   :widths: 40 60

   =========== ============
   Armor       Notes
   =========== ============
   Breastplate Medium armor
   Brigandine  Medium armor
   Chainmail   Medium armor
   =========== ============

.. table::
   :width: 100%
   :widths: 40 60

   ================== =====
   Other Items        Notes
   ================== =====
   Bag of heavy tools
   Bag of light tools
   ================== =====

Very Expensive Items
--------------------

.. table::
   :width: 100%
   :widths: 40 60

   =============== ========================
   Weapons         Notes
   =============== ========================
   Greatsword      Heavy weapon
   Heavy crossbow  Heavy weapon, long range
   Sword (jeweled) Medium weapon
   =============== ========================

.. table::
   :width: 100%
   :widths: 40 60

   =================== ======================================
   Armor               Notes
   =================== ======================================
   Dwarven breastplate Medium armor, encumbers as light armor
   Full plate armor    Heavy armor
   =================== ======================================

.. table::
   :width: 100%
   :widths: 40 60

   ============ ===================================
   Other Items  Notes
   ============ ===================================
   Disguise kit Asset for disguise tasks
   Healing kit  Asset for healing tasks
   Spyglass     Asset for perception tasks at range
   ============ ===================================

Exorbitant Items
----------------

.. table::
   :width: 100%
   :widths: 40 60

   =============== ===================================
   Armor           Notes
   =============== ===================================
   Elven chainmail Medium armor, encumbers as no armor
   =============== ===================================

.. table::
   :width: 100%
   :widths: 40 60

   ==================== =====
   Other Items          Notes
   ==================== =====
   Sailing ship (small)
   ==================== =====

Fantasy Artifacts
=================

In many ways, fantasy is the genre for artifacts. All magic items—wands
that shoot lightning, magic carpets, singing swords, rings that make the
wearer invisible, and so on—are artifacts. Below are a few sample
artifacts to give a template for GMs to follow. Those running a fantasy
campaign will likely want to create many magic artifacts.

.. sqlquery::
   SELECT DISTINCT
       name,
       CASE
           WHEN level_dice_count <> 0 AND level_bonus <> 0
               THEN level_dice_count || 'd' || level_dice_size ||
                    ' + ' || level_bonus
           WHEN level_dice_count <> 0 AND level_bonus = 0
               THEN level_dice_count || 'd' || level_dice_size
           WHEN level_dice_count = 0 AND level_bonus <> 0
               THEN level_bonus || ''
       END as level,
       form,
       effect,
       CASE
           WHEN depletion_target == 0
               THEN '—'
           WHEN depletion_target == 1
               THEN '1 in 1d' || depletion_dice_size
           ELSE '1-' || depletion_target || ' in 1d' || depletion_dice_size
       END as depletion,
       depletion_notes,
       notes
   FROM artifact_templates
   WHERE source_name = 'CSRD Fantasy'
   ORDER BY name

   {% for row in cursor %}
   {{row.name}}
   {{row.name|length * "-"}}

   Level:
       {{row.level}}

   Form:
       {{row.form|indent}}

   Effect:
       {{row.effect|html2rst|indent}}

   Depletion:
       {{row.depletion}} {{row.depletion_notes}}

   {{row.notes|html2rst}}
   {% endfor %}

Fantasy Species Descriptors
===========================

In a high fantasy setting, some GMs may want dwarves and elves to be
mechanically different from humans. Below are some possibilities for how
this might work.

Dwarf
-----

You’re a stocky, broad-shouldered, bearded native of the mountains and
hills. You’re also as stubborn as the stone in which the dwarves carve
their homes under the mountains. Tradition, honor, pride in smithcraft
and warcraft, and a keen appreciation of the wealth buried under the
roots of the world are all part of your heritage. Those who wish you ill
should be wary of your temper. When dwarves are wronged, they never
forget.

You gain the following characteristics:

Stalwart:
    +2 to your Might Pool.

Skill:
    You are trained in Might defense rolls.

Skill:
    You are trained in tasks related to stone, including sensing
    stonework traps, knowing the history of a particular piece of
    stonecraft, and knowing your distance beneath the surface.

Skill:
    You are practiced in using axes.

Skill:
    You are trained in using the tools required to shape and mine
    stone.

Vulnerability:
    When you fail an Intellect defense roll to avoid damage,
    you take 1 extra point of damage.

Additional Equipment:
    You have an axe.

Initial Link to the Starting Adventure:
    From the following list of
    options, choose how you became involved in the first adventure.

    1. You found the PCs wandering a maze of tunnels and led them to safety.
    2. The PCs hired you to dig out the entrance to a buried ruin.
    3. You tracked down the thieves of your ancestor’s tomb and found they were the PCs. Instead of killing them, you joined them.
    4. Before dwarves settle down, they need to see the world.

Elf
---

You haunt the woodlands and deep, natural realms, as your people have
for millennia. You are the arrow in the night, the shadow in the glade,
and the laughter on the wind. As an elf, you are slender, quick,
graceful, and long lived. You manage the sorrows of living well past
many mortal lifetimes with song, wine, and an appreciation for the deep
beauties of growing things, especially trees, which can live even longer
than you do.

You gain the following characteristics:

Agile:
    +2 to your Speed Pool.

Long-Lived:
    Your natural lifespan (unless tragically cut short) is
    thousands of years.

Skill:
    You are specialized in tasks related to perception.

Skill:
    You are practiced in using one bow variety of your choice.

Skill:
    You are trained in stealth tasks. In areas of natural woodland,
    you are specialized in stealth tasks.

Fragile:
    When you fail a Might defense roll to avoid damage, you take 1
    extra point of damage.

Additional Equipment:
    You have a bow and a quiver of arrows to go with
    it.

Initial Link to the Starting Adventure:
    From the following list of
    options, choose how you became involved in the first adventure.

    1. Before putting an arrow in the forest intruders, you confronted them and met the PCs, who were on an important quest.
    2. Your heart yearned for farther shores, and the PCs offered to take you along to new places.
    3. Your home was burned by strangers from another place, and you gathered the PCs along the way as you tracked down the villains.
    4. An adventure was in the offing, and you didn’t want to be left behind.

Half-Giant
----------

You stand at least 12 feet (4 m) tall and tower over everyone around
you. Whether you are a full-blooded giant or merely have giant heritage
from large ancestors, you’re massive. Always large for your age, it
became an issue only once you reached puberty and topped 7 feet (2 m) in
height, and kept growing from there.

You gain the following characteristics:

Tough:
    +4 to your Might Pool.

Mass and Strength:
    You inflict +1 point of damage with your melee
    attacks and attacks with thrown weapons.

Breaker:
    Tasks related to breaking things by smashing them are eased.

Inability:
    You’re too large to accomplish normal things. Tasks related
    to initiative, stealth, and fine manipulation of any sort (such as
    lockpicking or repair tasks) are hindered.

Additional Equipment:
    You have a heavy weapon of your choice.

Initial Link to the Starting Adventure:
    From the following list of
    options, choose how you became involved in the first adventure.

    1. You fished the PCs out of a deep hole they’d fallen into while exploring. 
    2. You were the PCs’ guide in the land of giants and stayed with them afterward. 
    3. The PCs helped you escape a nether realm where other giants were imprisoned by the gods. 
    4. You kept the PCs from being discovered by hiding them behind your bulk when they were on the run.

Helborn
-------

Demons of the underworld sometimes escape. When they do, they can taint
human bloodlines. Things like you are the result of such unnatural
unions. Part human and part something else, you are an orphan of a
supernatural dalliance. Thanks to your unsettling appearance, you’ve
probably been forced to make your own way in a world that often fears
and resents you. Some of your kin have large horns, tails, and pointed
teeth. Others are more subtle or more obvious in their differences—a
shadow of a knife-edge in their face and a touch that withers normal
plants, a little too much fire in their eyes and a scent of ash in the
air, a forked tongue, goatlike legs, or the inability to cast a shadow.
Work with the GM on your particular helborn appearance.

You gain the following characteristics.

Devious:
    +2 to your Intellect Pool.

Skill:
    You are trained in tasks related to magic lore and lore of the
    underworld.

Fire Adapted:
    +2 to Armor against damage from fire only.

Helborn Magic:
    You are inherently magical. Choose one low-tier ability
    from the :doc:`abilities` chapter. If the GM agrees it is appropriate, you gain
    that ability as part of your helborn heritage, and can use it like any
    other type or focus ability.

Inner Evil:
    You sometimes lose control and risk hurting your allies.
    When you roll a 1, the GM has the option to intrude by indicating that
    you lose control. Once you’ve lost control, you attack any and every
    living creature within short range. You can’t spend Intellect points for
    any reason other than to try to regain control (a difficulty 2 task).
    After you regain control, you suffer a –1 penalty to all rolls for one
    hour.

Inability:
    People distrust you. Tasks to persuade or deceive are
    hindered.

Initial Link to the Starting Adventure:
    From the following list of
    options, choose how you became involved in the first adventure.

    1. You were nearly beaten to death by people who didn’t like your look, but the PCs found and revived you. 
    2. The PCs hired you for your knowledge of magic. 
    3. Every so often you get visions of people trapped in the underworld. You tracked those people down and found the PCs, who’d never visited the underworld. Yet. 
    4. Your situation at home became untenable because of how people reacted to your looks. You joined the PCs to get away.

Optional Rule: Spellcasting
===========================

Fantasy settings prioritize magic as an essential ingredient. But why
restrict that magic to just wizards and similar characters? It’s not
uncommon in fantasy literature for a thief or warrior to learn a few
spells as they steal or brawl through their adventures. Leiber’s Gray
Mouser knew some spells, Moorcock’s Elric knew a lot, pretty much
everyone in Anthony’s Xanth books knew at least one, and so on. Of
course, wizards and sorcerers specialize in spellcasting, which gives
them clear superiority in magic use. But whether a character is a
fireball-flinging wizard or a belligerent barbarian, anyone can learn
some spellcasting under this optional rule.

Under the spellcasting rule, any character, no matter their role or
type, can choose to learn a spell as a long-term benefit. After they
learn one spell, they may learn more later if they wish, or just stick
with the one.

First Spell
-----------

Any character can gain a spell by spending 3 XP and working with the GM
to come up with an in-game story of how the PC learned it. Maybe they
learned it as a child from their parent and practiced it enough to
actually do it; perhaps they spent a month hiding in a wizard’s library
reading; it could be that they found a weird magical amulet that imbues
them with the spell; and so on.

Next, choose one low-tier ability from the :doc:`abilities` chapter. If the GM
agrees it is appropriate, the character gains that ability as their
spell, with a few caveats. The spell can’t be used like a normal ability
gained through a PC’s type or focus. Instead, a character must either
use a recovery roll or spend many minutes or longer evoking their spell,
in addition to paying its Pool cost (if any).

Using a Recovery Roll to Cast a Spell:
    If the character uses a
    one-action, ten-minute, or one-hour recovery roll as part of the same
    action to cast the spell (including paying any Pool costs), they can use
    the ability as an action. This represents a significant mental and
    physical drain on the character, because the normal benefit of
    recovering points in a Pool is not gained.

Spending Time to Cast a Spell:
    If the character takes at least ten
    minutes chanting, mumbling occult phonemes, concentrating deeply, or
    otherwise using all their actions, they can cast a low-tier spell (if
    they also pay any Pool costs). An hour is required to cast mid-tier
    spells. Ten hours are required to cast a high-tier spell.

More Spells
-----------

Once a character has learned at least one spell, they can opt to learn
additional spells later. Each time, they must spend an additional 3 XP
and work with the GM to come up with an in-game story of how the
character’s magical learning has progressed.

Two additional rules for learning additional spells apply:

First, a character must be at least tier 3 and have previously gained
one low-tier spell before they can learn a mid-tier spell.

Second, a character must be at least tier 5 and have previously gained
one mid-tier spell before they can learn a high-tier spell.

Otherwise, gaining and casting additional spells are as described for
the character’s first spell.

Wizards and the Optional Spellcasting Rule
------------------------------------------

Wizards (usually Adepts) and characters with explicit spellcasting foci
like Masters Spells, Channels Divine Blessings, Speaks for the Land, and
possibly others are also considered to be spellcasters, and moreover,
specialized ones. Their spells—abilities provided by their type or
focus—are used simply by paying their Pool costs. Extra time or physical
effort isn’t required to cast them. That’s because, in the parlance of
the fantasy genre, these spells are considered to be “prepared.”

But specialized casters can also use the optional spellcasting rule to
expand their magic further. They can learn additional spells via the
optional spellcasting rule just like other characters, with the same
limitations.

Optionally, specialized casters who record their arcane knowledge in a
spellbook (or something similar) gain one additional benefit. The
spellbook is a compilation of spells, formulas, and notes that grants
the specialized caster more flexibility than those who’ve simply learned
a spell or two. With a spellbook, a PC can replace up to three prepared
spells with three other spells they’ve learned of the same tier. To do
so, they must spend at least one uninterrupted hour studying their
spellbook. Usually, this is something that requires a fresh mind, and
must be done soon after a ten-hour recovery.

For instance, if a wizard exchanges Ward (an ability gained from their
type) with Telekinesis (an ability gained from the optional spellcasting
rule), from now on the character can cast Ward only by spending time or
using a recovery roll (as well as spending Pool points). On the other
hand, they can use Telekinesis normally, because now it’s prepared.
Later, the wizard could spend the time studying to change out their
prepared spells with others they’ve learned using the optional
spellcasting rule.

(A PC might choose the 4 XP character advancement option to select a new
type-based ability from their tier or a lower tier. If so, the ability
gained doesn’t count as a spell, and the spellcasting rule limitations
do not apply to the ability so gained. If the PC is a wizard and uses
the 4 XP character advancement option, treat the ability as one more
prepared spell.)
