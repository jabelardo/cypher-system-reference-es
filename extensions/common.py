from pathlib import Path
import sqlite3
from typing import Optional

from database.build import build_database


_DATABASE: Optional[sqlite3.Connection] = None
_DATABASE_FILES: list[str] = []


def ensure_database() -> tuple[sqlite3.Connection, list[str]]:
    global _DATABASE
    global _DATABASE_FILES

    if _DATABASE is None:
        data_path = Path(__file__).parent.parent.joinpath("database/")
        db_path = data_path.joinpath("cypher-system.sqlite")

        try:
            db_path.unlink()
        except FileNotFoundError:
            # All according to plan!
            pass

        _DATABASE = sqlite3.connect(str(db_path))
        _DATABASE.row_factory = sqlite3.Row
        _DATABASE_FILES = build_database(
            _DATABASE,
            data_path,
        )

    return (_DATABASE, _DATABASE_FILES)
