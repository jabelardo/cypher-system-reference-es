Cyphers
#######

Cyphers are one-use abilities that characters gain over the course of
play. They have cool powers that can heal, make attacks, ease or hinder
task rolls, or (in a more supernatural and extreme example) produce
effects such as nullifying gravity or turning something invisible.

Most cyphers aren’t physical objects—just something useful that happens
right when you need it. They might be a burst of insight that allows a
character to make a perfectly executed attack, a lucky guess when using
a computer terminal, a coincidental distraction that gives you an
advantage against an NPC, or a supernatural entity that makes things
work out in your favor. In some games, cyphers come in the form of
items, like magic potions or bits of alien technology.

Cyphers that don’t have a physical form are called subtle cyphers.

Cyphers that have a physical form are called manifest cyphers.

Regardless of their form, cyphers are single-use effects and are always
consumed when used. Unless a cypher’s description says otherwise, it
works only for the character who activates it. For example, a PC can’t
use an enduring shield cypher on a friend.

Cyphers are a game mechanic designed for frequent discovery and use. PCs
can have only a small number of cyphers at any given time, and since
they’re always finding more, they’re encouraged to use them at a steady
pace.

In theory, the cyphers gained by the PCs are determined randomly.
However, the GM can allow PCs to acquire or find them intentionally as
well. Cyphers are gained with such regularity that the PCs should feel
that they can use them freely. There will always be more, and they’ll
have different benefits. This means that in gameplay, cyphers are less
like gear or treasure and more like character abilities that the players
don’t choose. This leads to fun game moments where a player can say
“Well, I’ve got an X that might help in this situation,” and X is always
different. X might be an intuitive understanding of the local computer
network, a favor from the Faerie Court, an explosive device, a
short-range teleporter, or a force field. It might be a powerful magnet
or a prayer that will cure disease. It could be anything. Cyphers keep
the game fresh and interesting. Over time, characters can learn how to
safely carry more and more cyphers at the same time, so cyphers really
do seem more like abilities and less like gear.

(“Carry” in this sense refers to both subtle cyphers and manifest
cyphers, though a PC may not actually carry anything that physically
represents the cypher. A character thrown into prison without their
equipment might still have subtle cyphers.)

Cyphers don’t have to be used to make room for new ones. For subtle
cyphers, a character can just use an action to “lose” the cypher,
freeing up space to “find” one later (once a subtle cypher is discarded
this way, it is gone and can’t be recovered). For manifest cyphers, it’s
perfectly acceptable for the PCs to stash one elsewhere for later use;
of course, that doesn’t mean it will still be there when they return.

Why Cyphers?
============

Cyphers are (not surprisingly, based on the name) the heart of the
Cypher System. This is because characters in this game have some
abilities that rarely or never change and can always be counted
on—pretty much like in all games—and they have some abilities that are
ever-changing and inject a great deal of variability in play. They are
the major reason why no Cypher System game session should ever be dull
or feel just like the last session. This week your character can solve
the problem by walking through walls, but last time it was because you
could create an explosion that could level a city block.

The Cypher System, then, is one where PC abilities are fluid, with the
GM and the players both having a role in their choice, their assignment,
and their use. Although many things separate the game system from
others, this aspect makes it unique, because cyphers recognize the
importance and value of two things:

1. “Treasure,” because character abilities make the game fun and
   exciting. In fact, in the early days of roleplaying, treasure (usually
   in the form of magic items found in dungeons) was really the only
   customization of characters that existed. One of the drives to go out
   and have adventures is so you can discover cool new things that help you
   when you go on even more adventures. This is true in many RPGs, but in
   the Cypher System, it’s built right into the game’s core.

2. Letting the GM have a hand in determining PC abilities makes the
   game move more smoothly. Some GMs prefer
   to roll cyphers randomly, but some do not. For example, giving the PCs
   a cypher that will allow them to teleport far away might be a secret
   adventure seed placed by a
   forward-thinking GM. Because the GM has an idea of where the story is
   going, they can use cyphers to help guide the path. Alternatively, if
   the GM is open to it, they can give out cyphers that enable the
   characters to take a more proactive role (such as teleporting anywhere
   they want). Perhaps most important, they can do these things without
   worrying about the long-term ramifications of the ability. A device
   that lets you teleport multiple times might really mess up the game
   over the long term. But once? That’s just fun.

Cypher Limits
=============

All characters have a maximum number of cyphers they can have at any one
time, determined by their type. If a character ever attempts to carry
more, random cyphers instantly disappear until the PC has a number of
cyphers equal to their maximum (depending on the genre of the campaign,
subtle cyphers may be more or less likely to vanish this way). These
vanished cyphers are not recoverable.

Subtle Cyphers
==============

Subtle (nonphysical) cyphers are a way to introduce cyphers into a game
without overt “powered stuff”—no potions, alien crystals, or anything of
that nature. They’re most useful, perhaps, in a modern or horror setting
without obvious fantasy elements. Subtle cyphers are more like the
inherent abilities PCs have, adding boosts to Edge, recovering points
from Pools, coming up with ideas, and so on. In general, these are
commonplace, non-supernatural effects—a subtle cypher wouldn’t create a
laser beam or allow a character to walk through a wall. They don’t break
the fragile bubble of believability in genres where flashy powers and
abilities don’t make a lot of sense.

Subtle cyphers are particularly nice in a genre where the PCs are
supposed to be normal people. The cyphers can simply be an expression of
innate capabilities in characters that aren’t always dependable. And in
many ways, that’s probably more realistic than an ability you can count
on with certainty, because in real life, some days you can jump over a
fence, and some days you just can’t.

Concepts for subtle cyphers include the following:

Good fortune:
    Once in a while, things just go your way. You’re in the
    right place at the right time.

Inspirations:
    Sometimes you get inspired to do something you’ve never
    done before and might not be able to do again. Call it adrenaline mixed
    with the right motivation, or just doing the right thing at the right
    place at the right time. Who can really define it? Life’s funny that
    way.

Alien concepts:
    Complex and utterly inhuman memes enter our world and
    worm their way into and out of human consciousness. When this happens,
    it can cause mental distress and disorientation. It can also grant
    impossible abilities and advantages.

Blessings:
    In a fantasy world, there are nine gods. Each morning, all
    intelligent residents of the world pray to one of the gods, and some of
    the faithful gain a divine blessing. Some people believe that praying to
    different gods gives you different blessings.

Earworms:
    You know how some songs pop into your head and just won’t
    leave? There’s a power to those songs, and the right people know how to
    harness it. Make the songs disturbing or reminiscent of evil chants, and
    you’ve got a perfect cypher concept for a horror campaign.

Mysterious transmissions:
    What’s that buzzing? That mechanical
    chittering? Those numbers repeating over and over? And why can only some
    people hear it? A few who are aware of the sounds have learned how to
    make use of them.

Supernatural powers:
    Mental or mystical energies constantly shift and
    change, ebb and flow. But you’ve figured out how to attune your mind to
    them. There are no physical actions or paraphernalia required—just an
    inner conduit to the numinous.

Discovering Subtle Cyphers
--------------------------

Since subtle cyphers aren’t physical objects, GMs will need to figure
out when to give PCs new ones to replace the ones they have used. The
cyphers probably shouldn’t be tied to actions entirely under the
characters’ control—in other words, they shouldn’t come as a result of
meditation or anything of that nature. Instead, the GM should choose
significant points in the story when new cyphers might simply come
unbidden to the PCs. In the broader view, this is no different than
manifest cyphers placed as treasure in a creature’s lair, a secret
cache, or somewhere else for the characters to find. Either way, the GM
is picking good spots to “refill” potentially used cypher-based
abilities.

Subtle cyphers are often found in groups of one to six (the GM can roll
1d6 to determine the number). The GM might randomly assign the cyphers
to each PC who has space for more, or present a selection of cyphers to
the group and allow the players to choose which ones they want for their
characters. Characters should immediately know what their subtle cyphers
do. If a PC activates a healing subtle cypher when they think it’s
something to help pick a lock, that’s a waste of a useful character
ability.

PCs might be able to obtain subtle cyphers from NPCs or in unusual
circumstances as gifts, boons, or blessings, even asking for a
particular kind of subtle cypher, such as healing, protection, or skill.
For example, PCs who make a donation at a temple of a healing goddess
could ask to receive a blessing (subtle cypher) that allows them to
speak a healing prayer that restores points to one of their Pools. An
NPC wizard who owes the PCs a favor might cast a spell on them that
deflects one weapon if they say a magic word. An alien pylon might grant
knowledge of a strange mental code that lets a person see in the dark
for a few hours.

A PC can also acquire a new subtle cypher by spending 1 XP on one of the
following :ref:`player intrusions <how-to-play:player intrusion>`:

General cypher:
    You ask the GM for a general subtle cypher, such as
    “healing,” “movement,” “defense,” or perhaps something as specific as
    “flight.” The GM gives you a cypher that meets that description and
    randomly determines its level. If you don’t have space for this cypher,
    you immediately lose one of your current cyphers (your choice) and the
    new cypher takes its place.

Specific cypher:
    You ask the GM for a specific subtle cypher (such as a
    curative or stim) of a specific level. Make an Intellect roll with a
    difficulty equal to the cypher’s level plus 1. If you have had this
    cypher before, the task is eased. If you fail the roll, you do not gain
    a cypher. If you succeed, the GM gives you that subtle cypher at that
    level. If you don’t have space for this new cypher, you immediately lose
    one of your current cyphers (your choice) and the new cypher takes its
    place. Whether or not you succeed at the roll, the 1 XP is spent.

Manifest Cyphers
================

Because manifest cyphers are physical objects, and people are familiar
with the idea of finding “treasure” as part of playing an RPG, these
kinds of cyphers are easy to get into the hands of the PCs. They are
often found in groups of one to six (the GM can roll 1d6 to determine
the number), usually because the characters are searching for them. They
might be among the possessions of a fallen foe, hidden in a secret room,
or scattered amid the wreckage of a crashed starship. The GM can prepare
a list ahead of time of what successful searchers find. Sometimes this
list is random, and sometimes there is logic behind it. For example, a
warlock’s laboratory might contain four different magic potions that the
PCs can find.

If the characters search for cyphers, the GM sets the difficulty of the
task. It is usually 3 or 4, and scavenging can take fifteen minutes to
an hour.

Scavenging is not the only way to obtain manifest cyphers. They can also
be given as gifts, traded with merchants, or sometimes purchased in a
shop.

Unlike subtle cyphers, characters don’t automatically know what manifest
cyphers do. Once the PCs find a manifest cypher, identifying it is a
separate task, based on Intellect and modified by knowledge of the topic
at hand. In a fantasy setting, that knowledge would probably be magic,
but in a science fiction setting, it might be technology. The GM sets
the difficulty of the task, but it is usually 1 or 2. Thus, even the
smallest amount of knowledge means that cypher identification is
automatic. The process takes one to ten minutes. If the PCs can’t
identify a cypher, they can bring it to an expert for identification and
perhaps trade, if desired.

Manifest Cypher Forms
---------------------

None of the manifest cyphers in this chapter have a stated physical
form. The entries don’t tell you if something is a potion, a pill, or a
device you hold in your hands because that sort of detail varies greatly
from genre to genre. Are they magic? Are they tech? Are they symbiotic
creatures with programmed DNA? That’s up to the GM. It’s flavor, not
mechanics. It’s as important or unimportant as the style of an NPC’s
hair or the color of the car the bad guys are driving. In other words,
it’s the kind of thing that is important in a roleplaying game, but at
the same time doesn’t actually change anything (and RPGs have a lot of
things like that, if you think about it).

A manifest cypher’s physical form can be anything at all, but there are
some obvious choices based on genre. The GM can design a setting that
uses just one type—for example, a magical world where all cyphers are
potions made by faeries. Or they can use many types, perhaps mixing them
from different genres. Some suggestions include the following.

Fantasy/Fairy Tale:
    * Potions
    * Scrolls
    * Runeplates
    * Tattoos
    * Charms
    * Powders
    * Crystals
    * Books with words of power

Modern/Romance:
    * Drugs (injections, pills, inhalants)
    * Viruses
    * Smartphone apps

Science Fiction/Post-Apocalyptic:
    * Drugs (injections, pills, inhalants)
    * Computer programs
    * Crystals
    * Gadgets
    * Viruses
    * Biological implants
    * Mechanical implants
    * Nanotechnological injections

Horror:
    * Burrowing worms or insects
    * Pages from forbidden books
    * Horrific images

Superhero:
    * Forms from all the other genres

Manifest Cyphers Duplicating Subtle Cyphers
===========================================

Lots of overlap exists between what subtle cyphers and manifest cyphers
can do. Nearly anything that can be explained as a subtle cypher can
just as easily be a magic item, scientific device, or other manifest
object. A bit of luck that helps you sneak (a subtle cypher) and a
potion that helps you sneak (a manifest cypher) do the exact same thing
for a character. One advantage of manifest cyphers is that characters
can easily trade them to each other or sell them to NPCs. On the other
hand, manifest cyphers can be dropped or stolen, and subtle cyphers
can’t.

It’s fine if the GM decides to include both kinds of cyphers in the same
game. A horror game could begin with the PCs as normal people with
subtle cyphers, but as time goes on, they find one-use spells in occult
tomes, weird potions, and bone dust that has strange powers.

Using Cyphers
=============

The action to use a cypher is Intellect based unless described otherwise
or logic suggests otherwise. For example, throwing an explosive might be
Speed based because the device is physical and not really technical, but
using a ray emitter is Intellect based.

Because cyphers are single-use items, cyphers used to make attacks can
never be used with the :ref:`Spray <spray>` or :ref:`Arc Spray <arc spray>` abilities that some characters
might have. They are never treated as rapid-fire weapons.

Identified manifest cyphers can be used automatically. Once a manifest
cypher is activated, if it has an ongoing effect, that effect applies
only to the character who activated the cypher. A PC can’t activate a
cypher and then hand it to another character to reap the benefits.

A character can attempt to use a manifest cypher that has not been
identified; this is usually an Intellect task using the cypher’s level.
Failure might mean that the PC can’t figure out how to use the cypher or
that they use it incorrectly (GM’s discretion). Of course, even if the
PC activates the unidentified cypher, they have no idea what its effect
will be.

(Cyphers are meant to be used regularly and often. If PCs are hoarding
or saving their cyphers, feel free to give them a reason to put the
cyphers into play.)

Cypher Levels and Effects
=========================

All cyphers have a level and an effect. The level sometimes determines
an aspect of the cypher’s power (how much damage it inflicts, for
example) but otherwise it only determines the general efficacy, the way
level works with any object. The Level entry for a cypher is usually a
die roll, sometimes with a modifier, such as 1d6 or 1d6 + 4. The GM can
roll to determine the cypher’s level, or can allow the player to roll
when they receive the cypher.

Normal and Fantastic Effects
============================

Cypher effects fall into two categories: normal and fantastic. Normal
effects are things that could reasonably happen or be explained in the
normal physical world we’re familiar with. Fantastic effects are things
that can’t. A normal person could hit a target 240 feet (73 m) away with
a football, quickly get over a cold, run across a tightrope, or multiply
two two-digit numbers in their head. These tasks are difficult, but
possible. A normal person can’t throw an armored car, regrow a severed
arm, create a robot out of thin air, or control gravity with their mind.
These tasks are impossible according to the world as we know it. Cypher
effects are either normal (possible) or fantastic (impossible according
to the world as we know it).

Normal cypher effects should be available to PCs regardless of the genre
of your game. It’s perfectly reasonable for a modern, fantasy, horror,
science fiction, or superhero PC to have a cypher that gives them a
one-use bonus on an attack or skill task, lets them take a quick
breather to recover a few points in a Pool, or helps them focus their
will to avoid distractions or fatigue.

Fantastic cypher effects should be limited to games where magic,
technology, or other factors stretch the definition of “impossible.” A
cypher that turns a corpse into a zombie is out of place in a
non-fantastic modern game, but is perfectly reasonable for a fantasy,
science fiction, or superhero game, or even a horror game where zombies
exist, as long as the GM decides there is an appropriate story
explanation for it. The zombie cypher might be a necromantic spell in a
fantasy or superhero game, a code that activates a swarm of nanobots in
a science fiction game, or a virus in a horror game. The rules
categorize some cypher effects as fantastic to help the GM decide
whether to exclude cyphers that don’t fit the game they’re running. For
example, it is appropriate for a GM running a zombie horror survival
game set in 1990s Georgia to allow the zombie-creating cypher but not a
teleportation cypher, because creating a zombie is a fantastic effect
that fits the setting and teleportation isn’t.

Fantastic cyphers can be subtle or manifest.

Optional Rule: Normal Cyphers Duplicating Fantastic Effects
-----------------------------------------------------------

If the GM and players are willing to stretch their imaginations a bit,
it’s possible to include some fantastic cypher effects in a game where
only normal cypher effects should exist, even if the PCs are only using
subtle cyphers. The player using the cypher just needs to come up with a
practical, realistic explanation for how the fantastic result occurred
(perhaps with a much shorter or reduced effect than what’s described in
the cypher text).

For example, a PC with a phase changer who is trapped in a prison cell
could say that instead of physically phasing through the wall, using the
cypher means they find a long-forgotten secret door connected to a
narrow hallway leading to safety. A PC with a fire detonation could say
they notice a can of paint thinner in the room, kick it over, and throw
a table lamp into the spill, creating a spark and a momentary burst of
harmful flames. A PC with a monoblade could say they spot structural
flaws in an opponent’s armor, allowing them to attack for the rest of
that combat in such a way that the foe’s Armor doesn’t count.

These interpretations of fantastic cyphers in a non-fantastic setting
require player ingenuity and GM willingness to embrace creative
solutions (similar to players using player intrusions to make a change
in the game world). The GM always has the right to veto the explanation
for the fantastic effect, allowing the player to choose a different
action instead of using the fantastic cypher.

List of Cyphers by Category
===========================

If you want to pick a cypher of a particular category,
you can roll on one of these tables:

- :ref:`cyphers:Manifest Cypher Table`
- :ref:`cyphers:Fantastic Cypher Table`
- :ref:`cyphers:Subtle Cypher Table`

The classifications of manifest, fantastic, and subtle cyphers
are just suggestions for a typical campaign setting.

Manifest Cypher Table
---------------------

===== ============================
d100  Cypher
===== ============================
01–03 `Adhesion`_
04–05 `Antivenom`_
06–09 `Armor Reinforcer`_
10–11 `Attractor`_
12–13 `Blackout`_
14–15 `Catholicon`_
16–17 `Curse Bringer`_
18–19 `Death Bringer`_
20–22 `Density`_
23–26 `Detonation`_
27–29 `Detonation (Flash)`_
30–31 `Detonation (Massive)`_
32–34 `Detonation (Pressure)`_
35–36 `Detonation (Sonic)`_
37–38 `Detonation (Spawn)`_
39–41 `Detonation (Web)`_
42–44 `Equipment Cache`_
45–46 `Fireproofing`_
47–49 `Friction Reducer`_
50–52 `Gas Bomb`_
53–55 `Hunter/Seeker`_
56–57 `Infiltrator`_
58–60 `Information Sensor`_
61–63 `Metal Death`_
64–65 `Nullification Ray`_
66–68 `Poison (Emotion)`_
69–70 `Poison (Mind Disrupting)`_
71–73 `Radiation Spike`_
74–76 `Remote Viewer`_
77–79 `Shocker`_
80–82 `Sleep Inducer`_
83–85 `Sniper Module`_
86–88 `Solvent`_
89–90 `Spy`_
91–92 `Tracer`_
93–94 `Uninterruptible Power Source`_
95–96 `Warmth`_
97–98 `Water Adapter`_
99–00 `X-Ray Viewer`_
===== ============================

Fantastic Cypher Table
----------------------

===== ==============================
d100  Cypher
===== ==============================
01    `Age Taker`_
02    `Banishing`_
03–04 `Blinking`_
05    `Chemical Factory`_
06    `Comprehension`_
07–08 `Condition Remover`_
09    `Controlled Blinking`_
10    `Detonation (Creature)`_
11    `Detonation (Desiccating)`_
12    `Detonation (Gravity)`_
13    `Detonation (Gravity Inversion)`_
14    `Detonation (Matter Disruption)`_
15    `Detonation (Singularity)`_
16    `Disguise Module`_
17    `Disrupting`_
18    `Farsight`_
19    `Flame-Retardant Wall`_
20    `Force Cube`_
21–22 `Force Field`_
23    `Force Screen Projector`_
24    `Force Shield Projector`_
25    `Frigid Wall`_
26–27 `Gravity Nullifier`_
28    `Gravity-Nullifying Application`_
29–30 `Heat Attack`_
31    `Image Projector`_
32    `Inferno Wall`_
33–34 `Instant Servant`_
35    `Instant Shelter`_
36    `Lightning Wall`_
37–38 `Machine Control`_
39    `Magnetic Attack Drill`_
40    `Magnetic Master`_
41    `Magnetic Shield`_
42    `Manipulation Beam`_
43    `Matter Transference Ray`_
44    `Memory Switch`_
45    `Mental Scrambler`_
46    `Mind Meld`_
47    `Mind-Restricting Wall`_
48–49 `Monoblade`_
50    `Monohorn`_
51    `Null Field`_
52–53 `Personal Environment Field`_
54–55 `Phase Changer`_
56    `Phase Disruptor`_
57    `Poison (Explosive)`_
58    `Poison (Mind Controlling)`_
59    `Psychic Communique`_
60    `Ray Emitter`_
61    `Ray Emitter (Command)`_
62    `Ray Emitter (Fear)`_
63    `Ray Emitter (Friend Slaying)`_
64    `Ray Emitter (Mind Disrupting)`_
65    `Ray Emitter (Numbing)`_
66    `Ray Emitter (Paralysis)`_
67    `Reality Spike`_
68    `Repair Unit`_
69    `Repeater`_
70–71 `Retaliation`_
72    `Sheen`_
73–74 `Shock Attack`_
75    `Slave Maker`_
76    `Sonic Hole`_
77–78 `Sound Dampener`_
79    `Spatial Warp`_
80    `Stasis Keeper`_
81    `Subdual Field`_
82–83 `Telepathy`_
84    `Teleporter (Bounder)`_
85    `Teleporter (Interstellar)`_
86    `Teleporter (Planetary)`_
87    `Teleporter (Traveler)`_
88    `Temporal Viewer`_
89    `Time Dilation (Defensive)`_
90    `Time Dilation (Offensive)`_
91    `Trick Embedder`_
92    `Vanisher`_
93–94 `Visage Changer`_
95    `Visual Displacement Device`_
96    `Vocal Translator`_
97–98 `Weapon Enhancement`_
99    `Wings`_
00    `Zero Point Field`_
===== ==============================

Subtle Cypher Table
-------------------

===== ===========================
d100  Cypher
===== ===========================
01–04 `Analeptic`_
05–07 `Best Tool`_
08–10 `Burst of Speed`_
11–13 `Contingent Activator`_
14–17 `Curative`_
18–20 `Darksight`_
21–23 `Disarm`_
24–26 `Eagleseye`_
27–29 `Effect Resistance`_
30–32 `Effort Enhancer (Combat)`_
33–35 `Effort Enhancer (Noncombat)`_
36–39 `Enduring Shield`_
40–42 `Intellect Booster`_
43–45 `Intelligence Enhancement`_
46–48 `Knowledge Enhancement`_
49–51 `Meditation Aid`_
52–54 `Mind Stabilizer`_
55–57 `Motion Sensor`_
58–60 `Nutrition and Hydration`_
61–63 `Perfect Memory`_
64–66 `Perfection`_
67–69 `Reflex Enhancer`_
70–73 `Rejuvenator`_
74–76 `Remembering`_
77–79 `Repel`_
80–82 `Secret`_
83–85 `Skill Boost`_
86–88 `Speed Boost`_
89–91 `Stim`_
92–94 `Strength Boost`_
95–97 `Strength Enhancer`_
98–00 `Tissue Regeneration`_
===== ===========================

List of Cyphers by Name
=======================

All cyphers in this section may be manifest cyphers. It is the GM’s
discretion whether a particular cypher can be a subtle cypher, and that
decision usually depends on the setting.

.. sqlquery::
   SELECT DISTINCT name
   FROM cypher_templates
       LEFT JOIN cypher_tags ON (
           cypher_templates.source_name = cypher_tags.template_source_name
           AND cypher_templates.name = cypher_tags.template_name
       )
   WHERE source_name = 'Cypher System Reference Document'
       AND tag_name <> 'Power Boost'
   ORDER BY name

   This section includes the following cyphers:
   {% for row in cursor %}
   :ref:`cyphers:{{row.name}}`{% if loop.last %}.{% else %},{% endif +%}
   {% endfor %}

.. sqlquery::
   SELECT DISTINCT
       name,
       CASE
           WHEN level_dice_count <> 0 AND level_bonus <> 0
               THEN level_dice_count || 'd' || level_dice_size ||
                    ' + ' || level_bonus
           WHEN level_dice_count <> 0 AND level_bonus = 0
               THEN level_dice_count || 'd' || level_dice_size
           WHEN level_dice_count = 0 AND level_bonus <> 0
               THEN level_bonus || ''
       END as level,
       effect,
       notes
   FROM cypher_templates
       LEFT JOIN cypher_tags ON (
           cypher_templates.source_name = cypher_tags.template_source_name
           AND cypher_templates.name = cypher_tags.template_name
       )
   WHERE source_name = 'Cypher System Reference Document'
       AND tag_name <> 'Power Boost'
   ORDER BY name

   {% for row in cursor %}
   {{row.name}}
   {{row.name|length * "-"}}

   Level:
       {{row.level}}

   Effect:
       {{row.effect|html2rst|indent}}

   {{row.notes|html2rst}}
   {% endfor %}

Power Boost Cyphers
===================

These cyphers increase, modify, or improve a character’s existing
powers. A burst boost cypher, for example, allows someone with the Bears
a Halo of Fire focus to create a blast of fire in all directions, one
time. Imagine this as being a fire-using superhero’s ability to “go
nova.”

Power boost cyphers affect one use of a character’s abilities but do not
require an action. Their use is part of the action that they affect.

Power boost cyphers are a special type of cypher. In some Cypher System
games, they may be inappropriate, and in others, they may be the main
(or only) type of cypher available, as determined by the GM. They can be
either subtle or manifest.

.. sqlquery::
   SELECT DISTINCT
       name,
       CASE
           WHEN level_dice_count <> 0 AND level_bonus <> 0
               THEN level_dice_count || 'd' || level_dice_size ||
                    ' + ' || level_bonus
           WHEN level_dice_count <> 0 AND level_bonus = 0
               THEN level_dice_count || 'd' || level_dice_size
           WHEN level_dice_count = 0 AND level_bonus <> 0
               THEN level_bonus || ''
       END as level,
       effect,
       notes
   FROM cypher_templates
       JOIN cypher_tags ON (
           cypher_templates.source_name = cypher_tags.template_source_name
           AND cypher_templates.name = cypher_tags.template_name
       )
   WHERE source_name = 'Cypher System Reference Document'
       AND tag_name = 'Power Boost'
   ORDER BY name

   {% for row in cursor %}
   {{row.name}}
   {{row.name|length * "-"}}

   Level:
       {{row.level|indent}}

   Effect:
       {{row.effect|html2rst|indent}}

   {{row.notes|html2rst}}
   {% endfor %}
