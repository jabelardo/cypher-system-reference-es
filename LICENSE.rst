Legal Information
#################

This website is based on information from
the *Cypher System Reference Document*,
under the *Cypher System Open Licence*,
which requires the following disclaimer:

    This product is an independent production and is not affiliated with Monte
    Cook Games, LLC. It is published under the Cypher System Open License, found
    at http://csol.montecookgames.com.

    CYPHER SYSTEM and its logo are trademarks of Monte Cook Games, LLC in the
    U.S.A. and other countries. All Monte Cook Games characters and character
    names, and the distinctive likenesses thereof, are trademarks of Monte Cook
    Games, LLC.

All modified SRD content and non-SRD content on this site is released under
the *CC0 1.0 Universal* licence.

The full text of these licenses are included below:

.. toctree::
   :maxdepth: 1

   open-licence
   CC0-1.0
